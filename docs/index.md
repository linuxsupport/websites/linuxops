# Linux operations handbook

## Am I on rota?

Maybe, check [the schedule](rota/schedule.md) to find out!

## I *am* on rota! What now?

Lucky you, now you get to have [all the fun](rota/tasks.md)!
