The ~lxsoft-alerts channel is where automated system messages are recorded.
These massages are being defined in [Packages Alerts](https://gitlab.cern.ch/linuxsupport/cronjobs/package_alerts) and [Grafana alerts](https://monit-grafana.cern.ch/alerting/list).

### Packages Alerts
It parses Red Hat API  content from https://access.redhat.com/management/api/rhsm and based on the rules defined in `prod.packages.yml`, it will inform about, for instance, dangerous packages (new releases).

### Grafana Alerts
Important to understand if Koji, Lxsoft and Nomad services are in good health.