// Javascript for the Linux Distribution Roadmap page

function generateDateMap(timeline) {
    const [start, end] = timeline.map(date => date.split('.').map(Number));
    const dateMap = [];

    for (let year = start[0]; year <= end[0]; year++) {
        for (let half = 1; half <= 2; half++) {
            if (year === start[0] && half < start[1]) continue;
            if (year === end[0] && half > end[1]) break;
            dateMap.push(`${year}.${half}`);
        }
    }

    return dateMap;
}

function createTable() {
    const table = document.getElementById('distribution');
    table.innerHTML = '';

    const dateMap = generateDateMap(timeline);

    const headerRow = document.createElement('tr');
    const today = new Date();
    const today_half = (today.getMonth()+1 <= 6) ? 1 : 2;
    dateMap.forEach(header => {
        const th = document.createElement('th');
        th.className = `th_${header.replace('.', '_h')}`;
        th.innerText = header.replace('.', ' H');

        const [year, half] = header.split('.');
        if (today.getFullYear() == year && today_half == half) {
            th.classList.add("current_date");
        }
        headerRow.appendChild(th);
    });
    table.appendChild(headerRow);

    data.forEach(rowData => {
        const row = document.createElement('tr');
        row.id = rowData.row_id;
        let lastColIndex = 0;

        rowData.items.forEach(item => {
            let colStart = dateMap.indexOf(item.start);
            let colEnd = dateMap.indexOf(item.end);

            // Adjust colStart and colEnd if the item partially overlaps with the timeline
            if (colStart === -1 && colEnd === -1) return; // Skip if completely out of bounds

            colStart = colStart === -1 ? 0 : colStart;
            colEnd = colEnd === -1 ? dateMap.length - 1 : colEnd;

            const colSpan = colEnd - colStart + 1;

            if (colStart > lastColIndex) {
                const blankColSpan = colStart - lastColIndex;
                const blankTd = document.createElement('td');
                blankTd.colSpan = blankColSpan;
                blankTd.className = "blank";
                blankTd.innerHTML = "&nbsp;";
                row.appendChild(blankTd);
            }

            const td = document.createElement('td');
            td.colSpan = colSpan;
            td.className = item.style;
            td.innerText = item.name;
            row.appendChild(td);

            lastColIndex = colEnd + 1;
        });

        if (lastColIndex < dateMap.length) {
            const blankColSpan = dateMap.length - lastColIndex;
            const blankTd = document.createElement('td');
            blankTd.colSpan = blankColSpan;
            blankTd.className = "blank";
            blankTd.innerHTML = "&nbsp;";
            row.appendChild(blankTd);
        }

        table.appendChild(row);
    });
}

function update(e) {
    var lst = document.getElementById(e.target.id.split('-')[1]);
    lst.style.display = e.target.checked ? '' : 'none';
}

function getBoundingBox(ctx, left, top, width, height) {
    var ret = {};

    // Get the pixel data from the canvas
    var data = ctx.getImageData(left, top, width, height).data;
    var first = false;
    var last = false;
    var right = false;
    var left = false;
    var r = height;
    var w = 0;
    var c = 0;
    var d = 0;

    // 1. get bottom
    while(!last && r) {
        r--;
        for(c = 0; c < width; c++) {
            if(data[r * width * 4 + c * 4 + 3]) {
                last = r+1;
                ret.bottom = r+1;
                break;
            }
        }
    }

    // 2. get top
    r = 0;
    var checks = [];
    while(!first && r < last) {

        for(c = 0; c < width; c++) {
            if(data[r * width * 4 + c * 4 + 3]) {
                first = r-1;
                ret.top = r-1;
                ret.height = last - first - 1;
                break;
            }
        }
        r++;
    }

    // 3. get right
    c = width;
    while(!right && c) {
        c--;
        for(r = 0; r < height; r++) {
            if(data[r * width * 4 + c * 4 + 3]) {
                right = c+1;
                ret.right = c+1;
                break;
            }
        }
    }

    // 4. get left
    c = 0;
    while(!left && c < right) {

        for(r = 0; r < height; r++) {
            if(data[r * width * 4 + c * 4 + 3]) {
                left = c;
                ret.left = c;
                ret.width = right - left - 1;
                break;
            }
        }
        c++;

        // If we've got it then return the height
        if(left) {
            return ret;
        }
    }

    // We screwed something up...  What do you expect from free code?
    return false;
}

function exportSVG(tableid) {
    var canvas = document.createElement("canvas");
    canvas.width = CANVAS_WIDTH;
    canvas.height = CANVAS_HEIGHT;

    var html = document.getElementsByTagName("style")[0].outerHTML;
    html += document.getElementById(tableid).outerHTML;
    rasterizeHTML.drawHTML(html, canvas)
        .then(function success(renderResult) {
            // Figure out how big the actual contents are
            var bbox = getBoundingBox(canvas.getContext("2d"), 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

            // Create a new cropped canvas
            var destCanvas = document.createElement('canvas');
            destCanvas.width = bbox.width+10;
            destCanvas.height = bbox.height+10;
            destCanvas.getContext("2d").drawImage(
                canvas,
                bbox.left, bbox.top, bbox.width, bbox.height,
                0, 0, bbox.width, bbox.height);

            // Download the new canvas
            var link = document.createElement('a');
            link.href = destCanvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
            link.download = tableid+'.png';
            link.click();
        }, function error(e) {
            console.error(e);
        });
}

if (document.getElementById('distribution')) {
    createTable();

    form = document.querySelector('form.controls');
    /* Change the event listener of all 'input' elements in the form */
    form.querySelectorAll('input').forEach(input => {
        input.addEventListener('change', update);
        // Run it already once to hide the lists that are not checked
        update({target: input});
    });
}
