// Javascript to deal with the testing pipeline configurator in docs/distributions/openstack.md

function validate() {
    let valid = 1;

    // First, let's count how many options are selected in each category
    const OSes = [
        document.image_ci.TEST_OS8al.checked,
        document.image_ci.TEST_OS9al.checked,
        document.image_ci.TEST_OSRH8.checked,
        document.image_ci.TEST_OSRH9.checked,
    ].filter(value => value === true).length;

    const Instances = [
        document.image_ci.TEST_VIRTUAL.checked,
        document.image_ci.TEST_PHYSICAL.checked,
    ].filter(value => value === true).length;

    const Configs = [
        document.image_ci.TEST_UNMANAGED.checked,
        document.image_ci.TEST_PUPPET.checked,
    ].filter(value => value === true).length;

    const Architectures = [
        document.image_ci.TEST_X86_64.checked,
        document.image_ci.TEST_AARCH64.checked,
    ].filter(value => value === true).length;

    // If we were given an image, we can only have one OS selected
    if (document.image_ci.IMAGE.value !== '' && OSes !== 1) {
        document.image_ci.IMAGE.setCustomValidity('If you specify an image, you must select <em>exactly</em> one OS to test.');
        valid = 0;
    } else if (document.image_ci.IMAGE.validity.patternMismatch) {
        document.image_ci.IMAGE.setCustomValidity('The image must be specified as a UUID.');
        valid = 0;
    } else {
        document.image_ci.IMAGE.setCustomValidity('');
    }
    document.image_ci.IMAGE.reportValidity();

    // If we were given a flavor, TEST_PHYSICAL has to be selected
    if (document.image_ci.FLAVOR.value !== '' && !document.image_ci.TEST_PHYSICAL.checked) {
        document.image_ci.FLAVOR.setCustomValidity('If you specify a flavor, you must also enable tests of the Physical instance type.');
        valid = 0;
    } else {
        document.image_ci.FLAVOR.setCustomValidity('');
    }

    // Update the total number of tests to run and enable or disable the button
    const total = valid * OSes * Instances * Configs * Architectures;
    document.image_ci.run.textContent = `Run ${total} test` + (total === 1 ? "" : "s");
    if (total > 0) {
        document.image_ci.run.disabled = false;
    } else {
        document.image_ci.run.disabled = true;
    }

    showErrors(document.image_ci);
}

// Show validation errors next to the form elements
function showErrors(form) {
    var invalidFields = form.querySelectorAll( ":invalid" ),
        errorMessages = form.querySelectorAll( ".error-message" ),
        parent;

    // Remove any existing messages
    for ( var i = 0; i < errorMessages.length; i++ ) {
        errorMessages[ i ].parentNode.removeChild( errorMessages[ i ] );
    }

    for ( var i = 0; i < invalidFields.length; i++ ) {
        parent = invalidFields[ i ].parentNode;
        parent.insertAdjacentHTML( "beforeend", "<div class='error-message'>" +
            invalidFields[ i ].validationMessage +
            "</div>" );
    }

    // If there are errors, give focus to the first invalid field
    if ( invalidFields.length > 0 ) {
        invalidFields[ 0 ].focus();
    }
}

// Assemble the Gitlab URL and open it in a new tab
function runImageTests() {
    // .../pipelines/new?ref=<branch>&var[<variable_key>]=<value>&file_var[<file_key>]=<value>

    // Assemble the list of variables to pass to the pipeline
    let variables = [];
    document.image_ci.querySelectorAll('input').forEach(item => {
        if (item.type === 'button') return;

        if (item.type === 'checkbox') {
            value = item.checked ? 'True' : 'False';
        } else {
            value = item.value;
        }

        if (value !== '') variables.push(`var[${item.name}]=${value}`);
    });

    const url = `https://gitlab.cern.ch/linuxsupport/testing/image-ci/-/pipelines/new?ref=master&${variables.join('&')}`;
    return window.open(url, "_blank");
}

// Let's make sure all input fields validate their input on changes
document.image_ci.querySelectorAll('input').forEach(item => {
    item.addEventListener('change',  (event) => validate(), false);
    item.addEventListener('invalid', (event) => event.preventDefault(), true);
});
