# Onboarding of new members of the team

First of all, a very warm welcome to the Linux Team.

This can be considered a good starting point for any new member of the team, thus feel free to add any information that might be missing as well as updating any inaccurate/outdated resources.

!!! tip ""
    May Linus Torvalds be with you along this journey that is about to begin.

## Mattermost

Most internal discussions take place in the ~lxsoft-admins mattermost channel.

We also have a channel ~lxsoft-alerts which is where automated system messages are recorded.

Both channels are private, thus you will need to be invited by an existing member.

## Rota

Rotas are scheduled through [Op-Webtools Planning](https://op-webtools.web.cern.ch/planning/#/system/it:cd:cli:linux/group/4651), more information [here](/rota/schedule/).

You won't be assigned rota weeks right away, but please go to the tool to [edit your preferences](https://op-webtools.web.cern.ch/preferences), and in the "Planning" section, enable "Allow displaying EDH Holidays in Planning". You can also configure email alerts and use the subscription link to add the rota calendar to your favorite calendar application.

## Basics

### Adding yourself to `cern-linuxsupport-access`

[`cern-linuxsupport-access`](https://gitlab.cern.ch/linuxsupport/rpms/cern-linuxsupport-access) is a tool that can be used to enable/disable root access to the machine by CERN Linux Support personnel.

Hence, create a merge request with your public SSH key and your Kerberos principal. An example of what to modify exactly can be seen in this [commit](https://gitlab.cern.ch/linuxsupport/rpms/cern-linuxsupport-access/-/commit/61fdb57f1723ea5a2d38fb751dc50b988cdda445). The merge request will have to be accepted by a current member of the team, and the new package will have to be promoted to production.

### Update `cern-linuxsupport-access` on non puppet managed hosts

After the new user's key/principle has been added to `cern-linuxsupport-access` (and the RPM has been promoted to production), the package should be updated on the build test nodes (see [Architecture](../distributions/architecture) for the list)

### e-groups

Subscribe to the following [e-groups](https://e-groups.cern.ch/e-groups/EgroupsSearchForm.do):

* `lxsoft-admins`
* `lsb-experts`
* `linux-experts`
* `linux-team`

For Staff, also:

* `linux-team-sms`
* `lxsoft-admins-admins`

!!! warning
    Ask any previous member of the team for access to those e-groups where self-subscription is not a possibility

### Becoming a koji admin

Ask any already existing koji admin to run:

```
$> koji grant-permission admin $USERNAME
$> koji -p kojitest grant-permission admin $USERNAME
```

Here's the config for kojitest:
```
[kojitest]
server = https://kojitesthub.cern.ch/kojihub
weburl = https://kojitest.cern.ch/
topurl = https://kojitest.cern.ch/kojifiles
krb_canon_host = no
krb_rdns = false
no_ssl_verify = true
```

### Add user to the `linux:adm` AFS group

Get a Kerberos ticket with the `linux@CERN.CH` principal doing `kinit linux`

Get the password from:

```
tbag show linux_password --hg lxsoft/adm
```

Check the membership (before and/or after), and add the user to the group:

```
pts membership -expandgroups linux:adm
pts adduser -user $USERNAME -group linux:adm
```

### Add user principle to non puppet managed locations

1. lxsoftadm01:/mnt/data2/home/build/.k5login

### External accounts

Ask a current member of the Linux team to add you to [access.redhat.com](https://access.redhat.com).

## Community Engagement

We try to participate actively in the Enterprise Linux upstream community.

This involves being in touch with them, participating in the conversations that establish the direction of the project, and contributing when possible.

In addition, we have also organised two [CentOS Dojo](https://wiki.centos.org/Events/Dojo) events at CERN, where the community gathered:

* 2017: [CentOS Dojo at CERN](https://indico.cern.ch/event/649159/overview)
* 2018: [CentOS Dojo / RDO day at CERN](https://indico.cern.ch/event/727150/)

### Mailing Lists

In order to keep up to date with the community, subscribe to the [CentOS hyperkitty](https://lists.centos.org/hyperkitty/):

At least join:

* `devel`
* `mirror`

Of course do not hesitate to join any other list that might be of your interest.

### IRC

Even though most of the conversation happens in the mailing list, sometimes IRC might come in handy to ask direct questions or to attend meetings.

Join any [CentOS channel](https://wiki.centos.org/irc) if you consider it appropriate given the description.

### AlmaLinux

* Chat: https://chat.almalinux.org
* Forums: https://forums.almalinux.org/

## Useful bookmarks

* Pending tasks are tracked via the [LOS](https://its.cern.ch/jira/browse/LOS) Jira project
* [Availability History of Linuxsoft](https://monit-grafana.cern.ch/d/000000855/overview-service-availability?orgId=1&var-service=linuxsoft)
* [Historical operational documentation](https://twiki.cern.ch/twiki/bin/view/LinuxSupport/LinuxSupportInternals) - Note - this is provided as an old reference, though should not ever be needed
