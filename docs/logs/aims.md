# Monitoring aims.cern.ch

The aims.cern.ch service is configured via the [aims hostgroup](https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims) in Puppet.

The aims logs can be found under `monit_private_aims*` index in [Opensearch](https://os-linux.cern.ch/).

The fluentbit configuration for `aims` can be found [here](https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/-/blob/qa/data/hostgroup/aims.yaml?ref_type=heads#L29)

Using systemd fluentbit input to get the logs from the following systemd units:

- httpd.service that includes reboot.cgi and server.cgi
- dnsmasq.service
- xinetd.service that includes in.tftp

We are applying `modify` and `lua` filters to get the following info:

- facility
- logsource
- message
- pid
- priority
- program
