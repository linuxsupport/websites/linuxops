# Monitoring linuxsoft.cern.ch

The linuxsoft.cern.ch service is configured via [lxsoft hostgroup](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxsoft) in Puppet.
It is composed by 4 subhostgroups: `web`, `rsync`, `legacy` and `nomad`.

### lxsoft/web

The web logs can be found under `monit_private_lxsoft_logs_web*` index in [Opensearch](https://os-linux.cern.ch/).

We want to have access to the all httpd accesses made to [linuxsoft.cern.ch](linuxsoft.cern.ch).

The logs are arriving into `/var/log/httpd/*ssl_access*.log` of each `web` node, and we are using the `cci_apache2` parser to parse them with fluentbit.

The fluentbit configuration for `web` can be found [here](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxsoft/-/blob/qa/data/hostgroup/lxsoft/web.yaml?ref_type=heads#L99)

Using the `tail` fluentbit input and applying `modify`, `lua`, `geoip2` and `type_converter` filters to get the following info:

- acctype - `mirror`, `snap`, `upstream`, `cern`
- action - `GET`
- apache_response
- host
- logsource
- path
- os_arch
- os_flavor
- os_tag
- os_version
- os_dist
- os_source
- repository
- puppet_managed
- response_time
- return_data_size
- kibibytes_per_second
- megabytes_per_second
- client_ip
- city_name
- continent_code
- country_code
- country_name
- latitude
- longitude
- region_name
- timezone
- protocol
- agent

Added [lua unit tests](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxsoft/-/tree/qa/code/files/fluentbit/tests?ref_type=heads) to check the OS information for different kinds of urls. Please keep it updated.

The geoip2 filter is using a **static** MaxMind GeoLite2-City.mmdb database.

> **_NOTE:_** The geoip2 database needs to be updated - to be discussed with the monit team.

### lxsoft/rsync

The rsync logs can be found under `monit_private_lxsoft_logs_rsync*` index in [Opensearch](https://os-linux.cern.ch/).

The fluentbit configuration for `rsync` can be found [here](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxsoft/-/blob/qa/data/hostgroup/lxsoft/rsync.yaml?ref_type=heads#L6)

Using the `systemd` fluentbit input and applying `modify` and `lua` filters to get the following info:

- facility
- logsource
- message
- pid
- priority
- program


### lxsoft/nomad

The nomad logs can be found under `monit_private_lxsoft_logs_nomad*` index in [Opensearch](https://os-linux.cern.ch/).

The fluentbit configuration for `nomad` can be found [here](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxsoft/-/blob/qa/data/hostgroup/lxsoft/adm/nomad.yaml#L118)

Using fluentd docker log driver and use `forward` fluent-bit input.

We are applying `modify` and `parser` filters to get the following info:

- NOMAD_ALLOC_ID
- NOMAD_ALLOC_INDEX
- NOMAD_ALLOC_NAME
- NOMAD_GROUP_NAME
- NOMAD_JOB_NAME
- NOMAD_TASK_NAME
- container_name
- message
- source
- tag

> **_NOTE:_** The NOMAD fields, tag, container_name, message, host, and timestamp are being sent via fluent-bit opensearch output to `linux_private-nomad` index. This is done to avoid to lose our dashboards. Tt can be deprecated after we change our dashboards.