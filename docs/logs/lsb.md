# Monitoring koji.cern.ch

The koji.cern.ch service is configured via the [lsb hostgroup](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsb) in Puppet.
It is composed by `web`, `hub`, `builder` subhostgroups.

The koji logs can be found under `monit_private_lsb_logs*` index in [Opensearch](https://os-lsb.cern.ch/).

There are 4 different logtype: httpd_access, httpd_error, kojira, kojid.

The `kojid` and `kojira` are using the same parser - koji.

### lsb/web

The fluentbit configuration for `web` nodes can be found [here](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsb/-/blob/qa/data/hostgroup/lsb/web.yaml?ref_type=heads#L17)

Using the `tail` fluentbit input to get the access and error apache logs and applying `modify`, `lua`, `geoip2` and `type_converter` filters to get the following info:

- httpd_access:
    - cluster
    - referer
    - agent
    - method
    - latitude
    - continent_code
    - time_zone
    - roger_appstate
    - apache_response
    - country_code
    - path
    - logtype
    - size
    - clientip
    - country_name
    - user
    - longitude
- http_error:
    - cluster
    - level
    - latitude
    - continent_code
    - pid
    - time_zone
    - message
    - roger_appstate
    - country_code
    - logtype
    - clientip
    - country_name
    - longitude

### lsb/hub

The fluentbit configuration for `hub` nodes can be found [here](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsb/-/blob/qa/data/hostgroup/lsb/hub.yaml?ref_type=heads#L10)

Like in web nodes, it is using the `tail` fluentbit input to get the access and error apache logs. 
But we are also processing the `kojira` logs from `/var/log/kojira.log` using the `tail` input and `modify` filter to get the following info:
    
- cluster
- level
- logtype
- message
- roger_appstate

### lsb/builder

The fluentbit configuration for `builder` nodes can be found [here](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsb/-/blob/qa/data/hostgroup/lsb/builder.yaml?ref_type=heads#L19)

Processing the `kojid` logs from `/var/log/kojid.log` using the `tail` input, and `modify` and `lua` filters to get the same info above.