# Log monitoring

The Linux team is responsible for 3 main services:

- The main distribution server: linuxsoft.cern.ch

- The installation infrastructure servers: aims.cern.ch

- The RPM build service: koji.cern.ch

Each service is composed by test and prod virtual machines configured via Puppet.

These machines are running different services, for instance httpd, kojira, dnsmasq, etc., that we need to monitor to ensure the healthiness of the machines.

For this purpose, we use [Fluentbit](https://fluentbit.io/).

> Fluent Bit enables you to collect logs and metrics from multiple sources, enrich them with filters, and distribute them to any defined destination.

In our case the destination of the logs is [OpenSearch](https://opensearch.org/)

> OpenSearch is a community-driven, Apache 2.0-licensed open source search and analytics suite that makes it easy to ingest, search, visualize, and analyze data.

I recommend you read the [Fluentbit documentation](https://docs.fluentbit.io/manual), but in summary, we define which logs are we looking for, using the input configuration. Then we can modify them using the filters and send them to Opensearch using the output configuration.

We are using the `http` output plugin to send the logs to monit-logs.cern.ch. The MONIT team generated an http_password that can be found here: `tbag show --hg lsb monit_logs_tenant_lsb_password`.

The [fluentbit module](https://gitlab.cern.ch/ai/it-puppet-module-fluentbit) is maintained by MONIT and contains the fluentbit installation and all the different inputs, parsers, filters and outputs that can be used.

The [linux-monitoring module](https://gitlab.cern.ch/ai/it-puppet-module-linux_monitoring) is common structure to:

- Define the fluent-bit configuration constants.

- Instantiate a fluent-bit service with the hostgroup name.

- Create the required configuration files and the service instance.

- Enable the debug in `/var/log/messages` using [stdout filter](https://docs.fluentbit.io/manual/pipeline/filters/standard-output).

- Define a way to receive more than one input, filters and outputs and handle its properties.

This module uses the fluentbit module.

All the fluentbit configuration needed in each hostgroup will be made by calling `linux-monitoring` module.

One of mandatory fields is the `agent_name` that is used to create the fluentbit service `"fluent-bit@${agent_name}.service"`.

The other mandatory fields are `input_plugins`, `output_plugins` and `filters`.

So, let's take a look at each main service and understand which logs are we interested in and how this is being processed by Fluentbit.

- [LXSOFT](./lxsoft.md)

- [AIMS](./aims.md)

- [KOJI](./lsb.md)