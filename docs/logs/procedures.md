# Steps to get fluentbit to work

The goal is to use fluentbit to send our logs to monit-agent.cern.ch. 
These logs can be accessed through Opensearch.

### 1) Created the internal user in Opensearch for MONIT with all_access roles: `linux_private_monit` (os-linux.cern.ch) and `lsb_private_monit` (os-lsb.cern.ch)
     - Security -> Internal users
     - Security -> Roles -> all_access -> Mapped users


### 2) Created the PWN service to share the Opensearch user/password between MONIT and Linux team

```
$ ai-pwn set service fluentbit-lsb --owners monit-support linux-team --hg lsb

$ ai-pwn show service fluentbit-lsb

$ ai-pwn set service fluentbit-lxsoft --owners monit-support linux-team --hg lsb

$ ai-pwn show service fluentbit-lxsoft
```

### 3) Add the user and password to tbag hg and service (to share with MONIT)

```
$ tbag show --hg lsb ospassword_monit

$ tbag show --hg lsb osuser_monit

$ tbag set --service fluentbit-lsb osuser_monit

$ tbag set --service fluentbit-lsb ospassword_monit

$ tbag showkeys --service fluentbit-lsb
```

The same applies to `fluentbit-lxsoft`.

### 4) Created a SNOW ticket requests

[Request registration of a new producer in MONIT: lsb](https://cern.service-now.com/nav_to.do?uri=%2Fu_request_fulfillment.do%3Fsys_id%3Dd12bc0674786d210ff618f1c736d4384) where:

- the producer is the hostgroup
- the data type is logs
- the data visibility is private
- the data access is opensearch
- document schema is json

the cluster is os-lsb.cern.ch or os-linux.cern.ch

More information in [MONIT documentation](https://monit-docs.web.cern.ch/logs/http/)


### 5) Work on the fluentbit inputs, filters and output in each hostgroup/subhostgroup

#### Inputs

The input name defines what kind of logs are you interested in. For instance, to get the httpd logs, we need to use the `tail` input.

```
hg_lsb::input_plugins:
  - name: tail
    properties:
      path: '/var/log/httpd/*error*.log'
      storage_type: 'filesystem'
      tag: 'httpd_error'
      parsers: ["apache_error"]
```

Some properties need to be defined depending on the input that you chose.

But, for instance, the `tag` property is transversal to all input types.
The `tag` is used to route messages. In case that you have more than one input defined, and you need to apply different filters to which one.
You can use the `match` property of filters.

#### Filters

There are some mandatory fields requested by MONIT, like `producer` and `type`.

More information in [MONIT documentation](https://monit-docs.web.cern.ch/logs/http/)

To accomplish that we created a modify filter, for instance:

```
hg_lxsoft::filters:
  - name: modify
    properties:
      alias: 1
      set:
        producer: 'lxsoft'
        type: 'rsync'
        hostname: "%{facts.networking.fqdn}"
        hostgroup: "%{facts.hostgroup}"
        environment: "%{::environment}"
```

Setting the `debug_log_files` to true will enable the `stdout` filter and send all the fluentbit log files to `/var/log/messages`.

#### Output

Logs can be sent to the monit-logs HTTP endpoint.

We need to send the logs to https://monit-logs.cern.ch:10013/\<producer\>.

More information in [MONIT documentation](https://monit-docs.web.cern.ch/logs/http/#sending-data).

```
hg_lsb::output_plugins:
  - name: 'http'
    properties:
      host: 'monit-logs.cern.ch'
      port: 10013
      uri: '/lsb'
      http_passwd: 'monit_logs_tenant_lsb_password'
      http_user: 'lsb'
      format: 'json'
      match: '*'
      log_level: 'debug'
      json_date_key: 'timestamp'
      json_date_format: 'epoch'
      tls:
        enabled: true
```

The http_password that can be found here: `tbag show --hg lsb monit_logs_tenant_lsb_password`.

### 6) Create a Index template and a Index patterns in Opensearch

Once we have defined the fields that will search in Opensearch, two things needs to be created:

- Index Management -> Templates -> Create Template

- Dashboards Management -> Index patterns -> Create a Index pattern

> **_NOTE:_** Every time that you change the fields, you need to recreate the Index Template and delete the index in question from `Index Management -> Templates` (it will appear again when new data arrives). Also, please refresh the Index pattern.
