# Red Hat support

## Red Hat contacts

As of September 2023, our local contact for Red Hat related matters is **Laura Otieno <lotieno@redhat.com>** ("Key Account Manager"). The previous one was Daniel Perret <dperret@redhat.com>.

## Licenses

As of March 2022 and until 2029-05-31, CERN has an academic site-license which permits unlimited access for the RHEL product. Whilst unlimited, there is no support included (Red Hat calls this 'self support')
We also have a limited amount of fully licensed RHEL, RHEV and RHEUS hosts.
As we have full licenses, this allows us to raise support cases at Red Hat

## RedHat Customer accounts

From time to time you may get requests from people asking for RedHat Customer accounts, which you can create by going to <https://www.redhat.com/wapps/ugc/protected/usermgt/inviteUsers.html>. You will need your own Company/Organisation admin account, so if you do not, ask one from the team members.

To invite the user, put her/his CERN email and **do not** select any of the permissions.

## Access control

!!! note ""
    This is no longer needed (due to our site license), but the documentation is kept here for historical reasons.

As we have a site license, RHEL content on linuxsoft.cern.ch is available for all cern.ch IP addresses

Installation/updates repositories for licensed Virtualization / Extended Lifetime Support are however only available on a per host basis. Access is controlled by CERN LanDB Sets.

Each system requiring access must be inserted into corresponding network set at: [https://landb.cern.ch/landb/portal/sets/displaySets](https://landb.cern.ch/landb/portal/sets/displaySets)

* For Red Hat Enterprise Virtualization sets are named: `LINUXSOFT RHEV LICENSED XXX`
* For Red Hat Enterprise Extended Lifetime Support sets are named: `LINUXSOFT RHEUS LICENSED XXX`

where XXX corresponds to network domain (GPN/TN/LCG/ALICE, etc.)

**If you have a ticket reference, please add it as comments to the addition**.

Please note that RHEV/RHEUS cases are special - a very limited number of licensed is available: for these please contact Linux service managers before inserting new systems.

For `RHEL` systems: please add new system to `LINUXSOFT RHEL LICENSED XXX` (for most cases XXX will be `GPN` / `TN` - if not sure on which network the system is - please try inserting it in `GPN` first, then try `TN`)

Please note that for some network, the aforementioned sets might have not been created yet. Thus, if you need to add a system belonging to a different network domain: please contact Linux service managers for set creation.

After a system is inserted into a LanDB Set, a .htaccess file is updated automatically on Linuxsoft distribution servers within 10 minutes (see [rhel-access](https://gitlab.cern.ch/linuxsupport/cronjobs/rhel-access)).

!!! note ""
    Machines need to exist before being able to add them accordingly. If you can't find the requested machine, please check `https://network.cern.ch/sc/fcgi/sc.fcgi?Action=SearchForDisplay&DeviceName=*<name>*` in order to make sure it exists.

!!! note ""
    If a machine is deleted, it will be automatically removed from the corresponding LANDB set.

## RHEL images in OpenStack

!!! note ""
    This only applies for RHEL 7, RHEL 8+ images are handled by the [snapshot process](/distributions/snapshots/).

If a new RHEL image (architecture: x86_64) is detected from Red Hat, they are automatically managed via [rhel_manage_images](https://gitlab.cern.ch/linuxsupport/cronjobs/rhel_manage_images/). They are downloaded, adjusted slightly for the CERN cloud and are then uploaded to openstack.cern.ch
Users of the [linux-announce-rhel](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10430752) egroup will automatically be notified via email of the image UUID

## RHEL images in AIMS2

!!! note ""
    This only applies for RHEL 7, RHEL 8+ images are handled by the [snapshot process](/distributions/snapshots/).

If a new RHEL image (architectures: x86_64, aarch64 and ppc64le) is detected from Red Hat they are automatically managed via [rhel_manage_images](https://gitlab.cern.ch/linuxsupport/cronjobs/rhel_manage_images/). They are downloaded, their content is extracted, added to AIMS2 (only x86_64 and aarch64) and are made available [here](https://linuxsoft.cern.ch/enterprise/rhel/server/).
Users of the [linux-announce-rhel](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10430752) egroup will automatically be notified via email.

* Update our AIMS2 menus as in https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/-/blob/qa/data/hostgroup/aims.yaml#L213

* Ask kindly our Config team to add this new target on Foreman

## Support cases

In order to open a ticket, ask if a colleague added your account for RedHat support (as part of the onboarding steps), you can report cases on <https://access.redhat.com/support/cases/>

## Extended Research Network (ERN)

* As part of the CERN's academic site license, we negogiated special conditions for HEP sites as part of a "Extended Research Network" (ERN) whereby sites affiliated with CERN for "research" can get up to 1000 RHEL licenses for zero cost
* Interested sites need a commercial agreement with Red Hat (although one can be created at the time of interest) and an "introduction" through CERN (See [Red Hat Contacts](#Red Hat Contacts) above)
* Larger HEP sites can also apply directly to Red Hat for a Site License agreement

!!! note ""
    Research: servers exclusively doing computations, analysing and storing research data. HPC, Big data
    Non-research: computing capacity required to run organisation’s backend systems such as email servers, file servers storing various documents, HR, ERP, backup servers etc

This information has been shared with the HEP community several times, the most recent being the [Update on the Linux Strategy for CERN (and WLCG)](https://indico.cern.ch/event/1222948/contributions/5321042/attachments/2618936/4527498/20230328-hepix-linux-update.pdf) presentation at HEPiX Spring 2023

If a HEP site contacts us asking to profit from this offer, all we need to do is forward the request email to our [Red Hat Contacts](#Red Hat Contacts) - indicating that the institution is affiliated with CERN for the purposes of research. If you do not know who/what the institution is, it's best to check one of these resources:

* listed as a [WLCG site](https://wlcg-cric.cern.ch/wlcg/fedrcsite/list/)
* they have [signed a MoU](https://wlcg.web.cern.ch/mou/signed)
* part of [CMS](https://cms.cern/collaboration/cms-institutes)
* part of [ATLAS](https://atlas.cern/Discover/Collaboration)
* part of [ALICE](https://alice-collaboration.web.cern.ch/collaboration/alice_institute)
* part of [LHCb](https://lbfence.cern.ch/membership/report/members-list)
* other experiments?

Once you put them in contact with Red Hat and the onboarding process starts, add them to the list below.

Here is a list of the sites that we assume have so far profited from this agreement.

!!! note ""
    CERN is only involved in the initial onboarding, we don't know any specifics about how the contract is implemented

| Date site contacted CERN | Institution                                       | Institution contact name     |
| ------------------------ | ------------------------------------------------- | ---------------------------- |
| 2025.01.31               | ALICE-FIT laboratory in Poland                    | krystian.roslon@pw.edu.pl     |
| 2023.09.19               | Kirchhoff-Institut für Physik (KIP)               | tigran.mkrtchyan@cern.ch     |
| 2023.06.08               | Istituto Nazionale di Fisica Nucleare (INFN)      | andrea.chierici@cnaf.infn.it |
| 2023.05.30               | Institut de Physique des 2 Infinis de Lyon (IP2I) | d.pugnere@ipnl.in2p3.fr      |
| 2023.01.11               | Helsinki Institute of Physics (HIP)               | tomas.linden@Helsinki.fi     |
| 2022.06.27               | TRIUMF                                            | di.qing@cern.ch              |

