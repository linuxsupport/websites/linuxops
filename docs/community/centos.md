# Contributing to the CentOS community

CERN is a contributor to a few SIGs:

 * [Cloud](https://wiki.centos.org/SpecialInterestGroup/Cloud) (openstack)
 * [Software Collections](https://wiki.centos.org/SpecialInterestGroup/SCLo)
 * [Virtualization](https://wiki.centos.org/SpecialInterestGroup/Virtualization) (kubernetes)
 * [Storage](https://wiki.centos.org/SpecialInterestGroup/Storage) (ceph mimic)

Alternative architectures (aarch64, ppc64le, i686) support is there.

* Alt arches are released at the same time as x86_64.
* armv7hl available too, but less resources. Thanks @pgreco !

CI infrastructure (jenkins) for a lot of upstream projects: RDO, openshift, gluster, etc...

Community build system (<https://cbs.centos.org>) based on Koji, maintained by CentOS and CERN
