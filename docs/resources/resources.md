# List of accounts/services owned by the Linux team in other CERN services

## OpenStack projects

| Name | Purpose | Owner| Access |
| --- | --- | --- | --- |
| AIMS service | hosting aims02, aimstest01, aimstest02 | airibarr | `aims-admins` |
| AIMS service - Critical | hosting aims01, CP specific tenant | airibarr| `aims-admins` |
| IT Linux Support  | Catch-all | manuel | `lxsoft-admins` |
| IT Linux Support - Critical   | Catch-all Critical AVZ | manuel | `lxsoft-admins` |
| IT Linux Support - CI VMs | Automated tests with VMs. **NOT FOR USE BY HUMANS** | manuel | `lxsoft-admins`, `imageci`, `it-dep-cm-lcs-support` |
| IT Linux Support - CI Physical | Automated tests with physical instances. **NOT FOR USE BY HUMANS** | manuel | `lxsoft-admins` |
| IT Linux Support - Test Physical | Manual tests with physical instances  | manuel | `lxsoft-admins` |
| IT Linux Support - Test VMs | Manual tests with VMs | manuel | `lxsoft-admins` |
| Koji service  | Koji LSB instances | manuel | `koji-admins` |
| Koji service - physical | Koji LSB physical instance for image building | airibarr | `koji-admins` |
| Linuxsoft service | https://linuxsoft.cern.ch | manuel | `lxsoft-admins` |
| Linuxsoft service - Critical | https://linuxsoft.cern.ch | airibarr | `lxsoft-admins` |
| IT Distcc service | https://twiki.cern.ch/twiki/bin/view/LinuxSupport/DistccAsAService | manuel | `lxsoft-admins` |
| Linux training | https://linux-training.web.cern.ch/ | airibarr | `lxsoft-admins` |


!!! note ""
    If you ever want to check authz for the previous projects, do the following from `aiadm`:
    ```bash
    [djuarezg@aiadm00 ~]$ eval $(ai-rc "AIMS service")
    OpenStack environment variables set for Project = 'AIMS service'
    [djuarezg@aiadm00 ~]$ openstack role assignment list --project 'AIMS service' --names
    ```

## Openshift projects

| Name | Purpose | Access |
| --- | --- | --- |
| linuxops | Documentation autopublication | lxsoft-admins |
| linux | Main linux.cern.ch website | lxsoft-admins |
| linux-qa | linux-qa.cern.ch website | lxsoft-admins |
| linux-archive | Old linux.cern.ch content | lxsoft-admins |
| abrt-analytics | https://abrtanalytics.web.cern.ch/faf/ | lxsoft-admins |

## Opensearch internal users

* User: `linux_private` (`tbag show esuser --hg lxsoft`)
    * Retrieve credentials with: `tbag show espassword --hg lxsoft`
    * Purpose: Sending logs to our OS instance.
    * If need to change this password: 
        * Go to Opensearch -> Security -> Internal Users 
        * Do no forget to do the tbag change as well

* User: `linux_private_ro`
    * Retrieve credentials with: `tbag show linux_private_ro --hg lxsoft`
    * Purpose: Give Grafana read-only access to the Opensearch indices (<os-linux.cern.ch>) through the defined data sources.
    * If need to change this password: 
        * Go to Opensearch -> Security -> Internal Users 
        * Do no forget to do the tbag change as well
        * Update the password in Grafana -> Data sources

* User: `linux_private_monit`
    * Retrieve credentials with: `tbag show ospassword_monit --hg lxsoft`
    * Purpose: Give Monit team write access to <os-linux.cern.ch> Opensearch indices to populate log data.
    * If need to change this password: 
        * Go to Opensearch -> Security -> Internal Users 
        * Do no forget to do the tbag change as well
        * Inform the Monit team about the new password. Share it through:
            ```
            tbag show --service fluentbit-lxsoft esuser
            tbag show --service fluentbit-lxsoft espassword
            ```

* User: `slsmon`
    * Retrieve credentials with: `tbag show slsmon --hg lxsoft`
    * Purpose: To access the cron jobs in lxplus `slsmon@lxplus8$ acrontab -l | grep linux | grep -v`
    * If need to change this password: 
        * Go to Opensearch -> Security -> Internal Users 
        * Do no forget to do the tbag change as well

* User: `lsb_ro`
    * Retrieve credentials with: `tbag show opensearch_lsb_ro --hg lsb`
    * Purpose: Give Grafana read-only access to the Opensearch indices (<os-lsb.cern.ch>) through the defined data sources.
    * If need to change this password: 
        * Go to Opensearch -> Security -> Internal Users 
        * Do no forget to do the tbag change as well
        * Update the password in Grafana -> Data sources (OpenSearch LSB)

* User: `lsb_private_monit` (`tbag show osuser_monit --hg lsb`)
    * Retrieve credentials with: `tbag show ospassword_monit --hg lsb`
    * Purpose: Give Monit team write access to <os-lsb.cern.ch> Opensearch indices to populate log data.
    * If need to change this password:
        * Go to Opensearch -> Security -> Internal Users
        * Do no forget to do the tbag change as well
        * Inform the Monit team about the new password. Share it through:
            ```
            tbag show --service fluentbit-lsb osuser_monit
            tbag show --service fluentbit-lsb ospassword_monit
            ```

## Databases

### Oracle databases service (left for testing, no longer in use)

* Databases: `dbod-aims.cern.ch:6601`
* User: `aims`
* Retrieve credentials with: `tbag show --hg "aims" aims_psqldb_pwd`
* Owner: `aims-admins`
* Purpose: **old** AIMS2 production database. Now left for Oracle DB rpm testing

### Mimir Prometheus

* User: `linux`
* Retrieve credentials with: `tbag show --hg lxsoft linux_mimir_prometheus_password`
* Purpose: Used for monitoring

## CephFS shares

* Linuxsoft [/mnt/data1](https://filer-carbon.cern.ch/grafana/d/000000111/cephfs-detail?from=now-90d&orgId=1&refresh=1m&to=now&var-cluster=levinson&var-share=3de599f2-85bf-4775-a60d-8f0c5573b875&viewPanel=15)
* Linuxsoft [/mnt/data2](https://filer-carbon.cern.ch/grafana/d/000000111/cephfs-detail?orgId=1&refresh=1m&fullscreen&panelId=15&from=now-90d&to=now&var-cluster=flax&var-share=0d0757df-6220-4a27-81e7-51a38cd7ecdc)
* Linuxsoft [/mnt/data3](https://filer-carbon.cern.ch/grafana/d/000000111/cephfs-detail?orgId=1&refresh=1m&fullscreen&panelId=15&from=now-90d&to=now&var-cluster=flax&var-share=303fadd2-188b-4d27-984b-cc91bdfbbb73)
* Koji [/koji](https://filer-carbon.cern.ch/grafana/d/000000111/cephfs-detail?orgId=1&refresh=1m&var-cluster=flax&var-share=f51e1a94-b1ff-4f2c-9686-c7e9835e0f6c&fullscreen&panelId=15&from=now-90d&to=now)
* AIMS prod [/aims2](https://filer-carbon.cern.ch/grafana/d/000000111/cephfs-detail?from=now-90d&orgId=1&refresh=1m&to=now&viewPanel=15&var-cluster=flax&var-share=cebaadff-7d3d-4a1f-88d0-6eb773325901)
* AIMS test [/aims2](https://filer-carbon.cern.ch/grafana/d/000000111/cephfs-detail?from=now-90d&orgId=1&refresh=1m&to=now&viewPanel=15&var-cluster=dwight&var-share=4272339e-0efb-4d0f-b17d-43d4d6ec600b)

### CephFS shares full

    If we get Share free space alerts, we need to increase the size of shares of that particular project.

    CephFS are managed by Openstack manila:
    
    * Go to project -> share -> shares in the X project of openstack.cern.ch, where X can be one of the projects described above.
    * Click on "resize share"
    * Check that there is enough quota in “File share size” of the X project

## External resources

* Fedora mirror:
    * Fedora uses the self service [mirror manager](https://mirrormanager.fedoraproject.org) where mirror admins can update their sites/hosts/distributions. The upstream documentation on this page can be found [here](https://fedoraproject.org/wiki/Infrastructure/Mirroring#MirrorManager:_the_Fedora_Mirror_Management_system)
    * A Fedora account is required to access the mirror manager. We have a generic 'cern' account and the credentials can be seen through:
```
$ tbag show --hg lxsoft fedora_username
$ tbag show --hg lxsoft fedora_site_password
```
If required, though should not be:
```
$ tbag show --hg lxsoft fedora_security_question_answer
``` 

## DR Cold recovery tests

| service | date performed | log links |
| --- | --- | ---  |
| linuxsoft | 16.05.2023 | [DR link](https://disaster-recovery.web.cern.ch/dr-log/linuxsoft-cold-recovery-2024) / [codimd link](https://codimd.web.cern.ch/sprdaezkSaS941lKp2Z2UA?view) |
| aims | 29.07.2024 | [DR link](https://disaster-recovery.web.cern.ch/dr-log/aims-cold-recovery-2024) / [codimd link](https://codimd.web.cern.ch/yEOKAWCiT9qQg6QYeTomSw?view) |
| koji | 19.08.2024 | [DR link](https://disaster-recovery.web.cern.ch/dr-log/koji-cold-recovery-2024) / [codimd link](https://codimd.web.cern.ch/FBtptJAFRseFA263biibhA?view) |
