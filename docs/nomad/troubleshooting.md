# Troubleshooting

## Placement failures

If jobs are not starting due to placement failures (no resources on any node),
the cluster might have gotten itself into a broken state. I've also seen problems
with `docker pull` after S3 failures. Here are some things you can do to try to recover:

!!! danger "Important Note"
    Do this only to one server at a time, keeping the others running. Otherwise
    you risk losing the entire cluster, which would be a real PITA to recover.

1. Restart Nomad: `systemctl restart nomad`
2. Stop Nomad, restart the docker daemon, restart nomad
3. Stop Nomad, delete the client DB, restart Nomad:
  `systemctl stop nomad && rm -f /var/lib/nomad/client/state.db && systemctl restart nomad`
  (Do this especially if nomad crashes on startup. There have been some bugs in
  the past that prevented nomad from restarting cleanly)
4. Reboot the node

## Lost leader

Without a leader, nomad is unable to schedule nor execute any jobs. The negotiation of a leader should be automatic through the raft protocol, however it can happen that nomad gets confused and leader election is stuck or flaps. If this happens, you will see the following:

```
# . setNomad.sh
# /usr/local/bin/nomad server members
Name                        Address                    Port  Status  Leader  Protocol  Build  Datacenter  Region
lxsoftadm27.cern.ch.global  2001:1458:d00:39::100:469  4648  alive   false   2         1.1.4  meyrin      global
lxsoftadm28.cern.ch.global  2001:1458:d00:16::3c       4648  alive   false   2         1.1.4  meyrin      global
lxsoftadm29.cern.ch.global  2001:1458:d00:2d::100:4a   4648  alive   false   2         1.1.4  meyrin      global
lxsoftadm30.cern.ch.global  2001:1458:d00:32::100:1c   4648  alive   false   2         1.1.4  meyrin      global
lxsoftadm31.cern.ch.global  2001:1458:d00:12::3d       4648  alive   false   2         1.1.4  meyrin      global

Error determining leaders: 1 error occurred:
    * Region "global": Unexpected response code: 500 (No cluster leader)
```

To fix this issue, the [upstream instructions](https://learn.hashicorp.com/tutorials/nomad/outage-recovery?in=nomad/manage-clusters#manual-recovery-using-peers-json) can be followed

This process is essentially:

1. Stop Nomad `systemctl stop nomad` on all servers
2. Populate `/var/lib/nomad/server/raft/peers.json` on all servers
3. Start Nomad `systemctl start nomad` on all servers

Notes:

  * `/var/lib/nomad/server/raft/peers.json` is removed after Nomad startup
  * The upstream instructions use ipv4 for the `address` in `peers.json`. As we utilise ipv6, we should also utilise ipv6 in the creation of `peers.json`
  * A script to assist with the creation of peers.json can be found [here](https://gitlab.cern.ch/-/snippets/1964)

The `peers.json` file should look similar to the one below (which is the valid one as of 2023-09-14):

```json
[
  {
    "id": "0a60673d-a2ff-252a-01fc-f997241ad3c5",
    "address": "[2001:1458:d00:39::100:469]:4647",
    "non_voter": false
  },
  {
    "id": "bf046772-d3ce-eb79-b0ad-4d279deac507",
    "address": "[2001:1458:d00:16::3c]:4647",
    "non_voter": false
  },
  {
    "id": "a564bbbd-16a2-244c-51ab-3e7d593ce42b",
    "address": "[2001:1458:d00:2d::100:4a]:4647",
    "non_voter": false
  },
  {
    "id": "e40a4c11-c1fd-591f-cbaf-c6de74d96c8b",
    "address": "[2001:1458:d00:32::100:1c]:4647",
    "non_voter": false
  },
  {
    "id": "a76f3d09-4ecf-8950-05f6-b0de522cb7a5",
    "address": "[2001:1458:d00:12::3d]:4647",
    "non_voter": false
  }
]
```
