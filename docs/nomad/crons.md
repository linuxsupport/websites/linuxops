# Cron scripts



## Koji-distrepo

You can use the following little script to trigger koji-distrepo jobs for testing:

```bash
TAG="${1:-mytag9al-testing}"
PAYLOAD="{ \"TAG\": \"$TAG\", \"ARCHES\": \"x86_64 aarch64\" }"
TOKEN='xxx'

ENC_PAYLOAD=`echo -n "$PAYLOAD" | base64 -w 0`
DATA="{ \"Meta\": {\"PARENT_JOB\": \"test\"}, \"Payload\": \"$ENC_PAYLOAD\"}"

curl -v  \
    -H "X-Nomad-Token: $TOKEN" \
    -X POST \
    --data "$DATA" \
    https://lxsoftadm.cern.ch:4646/v1/job/prod_koji-distrepo/dispatch

```

To get the token, please use the following command in an `aiadm` node: `tbag show --hg lsb/test2/hub nomad_token`

## Koji regenrepo

Similarly, you can run the `koji-regenrepo` job like this:

```bash
#!/bin/bash

REGEX=''
REGEN_JOB="prod_koji-regenrepo"

PAYLOAD="{ \"REPO_REGEX\": \"${REGEX}\", \"FORCE\": \"false\" }"
DATA="{ \"Meta\": {\"PARENT_JOB\": \"centos8_snapshots\"}, \"Payload\": \"`echo -n "$PAYLOAD" | base64 -w 0`\"}"

echo "Triggering Nomad job with payload: ${PAYLOAD}"
curl -H "X-Nomad-Token: `cat /secrets/token`" \
    -X POST \
    --data "$DATA" \
    ${NOMAD_ADDR}/v1/job/${REGEN_JOB}/dispatch
```
