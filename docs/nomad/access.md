# Nomad

Nomad is a workload orchestrator that we use for automating tasks.

## Access

### UI

To access Nomad's UI, point your browser to:

<https://lxsoftadm.cern.ch>

Once logged in, go to <https://lxsoftadm.cern.ch/ui/settings/tokens> and click the "Sign in with cernsso" button.

Alternatively, you can also sign in with a token, which you can get from there:

```
tbag show nomad_submit_token --hg lxsoft/adm
```

### CLI

You can use nomad CLI in lxsoftadm (ssh as root):
```
[root@lxsoftadm28 ~]# source /root/setNomad.sh
[root@lxsoftadm28 ~]# nomad job status | grep running | wc -l
634
[root@lxsoftadm28 ~]#
```
For more info about the nomad CLI:
```
[root@lxsoftadm28 ~]# nomad help
```

Stop pending jobs:
```
[root@lxsoftadm28 ~]# nomad status | grep prod_koji-distrepo | grep -v dead | awk '{print $1}' | xargs -n 1 nomad stop --detach
```

### Monitoring

There's some basic monitoring available here:

<https://monit-grafana.cern.ch/d/_ffC7H-ik/job-status?orgId=18>

### Debugging

From the UI you can find out which node the docker container is running - to ease testing you may want to add something like `sleep 10000` in your scripts to eventually debug your code via a `docker exec -it <container> /bin/bash`

You can also check job logs on <https://es-linux.cern.ch/kibana/goto/35aa8869cc6ea88d1a384d6af96703ad?security_tenant=internal> (filter `NOMAD_TASK_NAME=task_name` as in `NOMAD_TASK_NAME=prod_yumsnapshot`).

## Cronjobs

All cronjobs' definitions can be found here:
<https://gitlab.cern.ch/linuxsupport/cronjobs>
