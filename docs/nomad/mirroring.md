# Mirroring a new repo

A recurring use case is some users asking us to mirror an external repo:

E.g. [RQF1444976](https://cern.service-now.com/service-portal?id=ticket&table=u_request_fulfillment&n=RQF1444976)

In the [reposync](https://gitlab.cern.ch/linuxsupport/cronjobs/reposync/) Gitlab repo, we can add new mirrors.

Ideally, in a first step, we can add the repo definition to the `dev.repos.d` folder, e.g. [tpc.repo](https://gitlab.cern.ch/linuxsupport/cronjobs/reposync/blob/57ed4f6f1f1727ecfcfdf94bc1c02989503b1014/prod.repos.d/tpc.repo)

If we want to modify the path (e.g. we only have an IP from upstream), we can edit the `dev.repos.yaml` file to define what we want to remove from the upstream url (pathcut) and how do we want the path to be created in our mirror (pathroot). E.g. [this commit](https://gitlab.cern.ch/linuxsupport/cronjobs/reposync/commit/78659add3b5f851cf3d6fe523318961a0e93cb91#46921bca268c7e0fcd91e1636e7426a2e311b009).

Once we have tested that the repository gets mirrored as we wish, we can move the introduced changes:

`dev.repos.yaml` -> `prod.repos.yaml`
`dev.repos.d/my-new-mirror.repo` -> `prod.repos.d/my-new-mirror.repo`
