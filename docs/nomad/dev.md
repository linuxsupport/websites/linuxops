# Development and Troubleshooting

One way to test and develop Nomad jobs is to run the docker container that the
job is supposed to start, taking care of passing all required environment variables
and mount points.

As most jobs will need to have access to `/mnt/data1` of the Ceph share, the most
convenient place to run this is directly on a lxsoftadm node. This *Should Be Safe<sup>TM</sup>*
as long as you're **not** using the production volumes and environment variables.

### Example

Here's how you could debug the [centos8_snapshots](https://gitlab.cern.ch/linuxsupport/cronjobs/centos8_snapshots):

1. ssh as root to a `lxsoft/adm` node:
    ```
    ai-foreman --hg lxsoft/adm/nomad showhost
    ```

2. Pull the latest docker image, just in case:

    ```bash
    ~ > docker pull gitlab-registry.cern.ch/linuxsupport/cronjobs/centos8_snapshots/centos8_snapshots:master
    ```

3. Take a look at the [nomad job definition](https://gitlab.cern.ch/linuxsupport/cronjobs/centos8_snapshots/blob/master/centos8_snapshots.nomad)
to see what volumes and environment variables you need to define.

4. Look for the appropriate values for these in the appropriate [dev environment configuration](https://gitlab.cern.ch/linuxsupport/cronjobs/centos8_snapshots/blob/master/dev.variables.sh).

    !!! danger "DANGER AHEAD"
        Make **really really extra sure** that you're **not** using the production environment variables.

5. Run your docker container putting everything together: (fill in your email address below and add volumes if needed)

    ```bash
    ~ > docker run -it --rm --entrypoint bash \
    -v /mnt/data1/dist:/data \
    -v /etc/nomad/ssh-id_ed25519:/root/.ssh/id_ed25519 \
    -v /etc/nomad/submit_token:/secrets/token \
    -v /etc/nomad/linuxci_api_token:/secrets/linuxci_api_token \
    -e PATH_MIRROR="centos/8" \
    -e PATH_SRC_RPMS="centos-vault/centos/8" \
    -e PATH_DBG_RPMS="centos-debuginfo/8" \
    -e PATH_KOJI_RPMS="internal/repos" \
    -e PATH_DESTINATION="tmp" \
    -e PRODDATE="last monday" \
    -e OLDESTDATE="10 days ago" \
    -e EMAIL_FROM="linux.support@cern.ch" \
    -e EMAIL_ADMIN="<you>@cern.ch" \
    -e EMAIL_USERS="<you>@cern.ch" \
    -e EMAIL_USERS_TEST="<you>@cern.ch" \
    -e NOMAD_ADDR="https://lxsoftadm.cern.ch:4646" \
    gitlab-registry.cern.ch/linuxsupport/cronjobs/centos8_snapshots/centos8_snapshots:<branch>
    ```

You will now be in a bash shell and you'll be able to run your cronjob interactively.
