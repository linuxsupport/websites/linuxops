# Useful references

This is a list of useful links that were used at some point for the Koji service. Specially for <https://gitlab.cern.ch/linuxsupport/koji-image-build/>, <https://gitlab.cern.ch/linuxsupport/rpms/koji/>, <https://gitlab.cern.ch/linuxsupport/rpms/oz/>, <https://gitlab.cern.ch/ai/it-puppet-module-koji> and <https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsb>. Kickstart references are specially relevant here for our current image building mechanism.

* <https://lukas.zapletalovi.com/2017/10/efi-with-libvirt-in-rhel7.html>: Libvirt + OVMF configuration. Not using Kraxel's RPM anymore [in favour of CS8 RPM instead](https://its.cern.ch/jira/browse/LOS-921)
* <https://andreaskaris.github.io/blog/linux/libvirt-uefi-without-secureboot/>: RHEL: Booting a virtual machine with UEFI but without secure boot. Relevant for [LOS-921](https://its.cern.ch/jira/browse/LOS-921)
* <https://forums.centos.org/viewtopic.php?t=71030>: background for how we do dual-boot cloud images.
* <https://techblog.web.cern.ch/techblog/post/bios_uefi_cloud_image/>: more background on how we built our dual-boot cloud images
* <https://github.com/openlogic/AzureBuildCentOS/blob/8f46a34e6fceed41030ef4611ca85a272f0abbe4/ks/azure/centos77.ks>: Where our inspiration for dual-boot cloud images come from
* <https://github.com/CentOS/sig-cloud-instance-build/blob/98aa8c6f0290feeb94d86b52c561d70eabc7d942/cloudimg/CentOS-8-x86_64-Azure.ks>: Where our inspiration for dual-boot cloud images come from
* <https://askubuntu.com/questions/500359/efi-boot-partition-and-biosgrub-partition>: more refs used for the dual-boot images
* <https://gist.github.com/vkanevska/fd624f708cde7d7c172a576b10bc6966>: Custom CentOS 7 bootable ISO / Support UEFI & Secure boot
* <https://blog.heckel.io/2017/05/28/creating-a-bios-gpt-and-uefi-gpt-grub-bootable-linux-system/>: Creating a BIOS/GPT and UEFI/GPT Grub-bootable Linux system
* <https://docs.openvz.org/openvz_installation_using_pxe_guide.webhelp/_kickstart_file_example.html>: OpenVZ Installation Using PXE Chapter 4. Creating a Kickstart File
* <https://docs.openvz.org/openvz_installation_using_pxe_guide.webhelp/_kickstart_file_example_for_installing_on_efi_based_systems.html>: OpenVZ Installation Using PXE 4.3. Kickstart File Example
* <https://listman.redhat.com/archives/kickstart-list/2012-August/msg00005.html>: how to tell when biosboot partition is needed? Relevant for the dual-boot story
* <https://outflux.net/blog/archives/2018/04/19/uefi-booting-and-raid1/>: relevant for the background info about these dual-boot images, UEFI and RAID. Ref. [RQF1571304](https://cern.service-now.com/service-portal?id=ticket&table=u_request_fulfillment&n=RQF1571304)
* <https://www.thomas-krenn.com/en/wiki/%EF%BB%BFInstalling_Operating_Systems_on_UEFI_Systems>: `efibootmgr` refs. Relevant for [KB0006881](https://cern.service-now.com/service-portal?id=kb_article&n=KB0006881).
* <https://community.theforeman.org/t/efi-boot-order-with-centos-7-network-boot-vs-local-boot/10529/6>: `efibootmgr`, UEFI boot order changes. Same as the previous link.
* <https://vessokolev.blogspot.com/2015/03/how-to-install-centos-7-on-hp-proliant.html?m=1>: Driver Disk info. Relevant for <https://pagure.io/koji/pull-request/3217>
* <https://groupbcl.ca/blog/posts/2018/creating-a-device-driver-disc-for-red-hatcentos-installs/>: Driver Disk info. Relevant for <https://pagure.io/koji/pull-request/3217>
