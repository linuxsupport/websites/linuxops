# Create a tag in koji

Eventually, SNOW tickets will come our way regarding the need of creating a new tag in koji (e.g. [RQF1446382](https://cern.service-now.com/service-portal?id=ticket&table=u_request_fulfillment&n=RQF1446382)).

To accomplish that mission, start by cloning the [lxdist-build](https://gitlab.cern.ch/linuxsupport/lxdist-build) repo.

Remember that **all changes to Koji tags should be done via these scripts**, not directly in Koji. This helps keep our tags reproducible.

## Alma and RHEL 8/9 tags
### 1. **Adding the tag to koji**

In the `bin/tags` directory, copy the template file `createtagX.template` to create a script called `<mynewtagX>.sh` (adapt for your tag name where `X` can be `8el`, `9el`, `8al`, `9al`).

Change the `TAG` variable and the other default variables as necessary.

Add it to git, commit, push, (wait for the MR to be reviewed if it's your first time).

Then, login in `lxsoftadm01` as `build`, change directory into `bin/tags`, do a `git pull` and run the script(s) from the master branch et voila!

### 2. **Adding the admin egroup to ACL**

In [koji::hub::acls](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsb/-/blob/qa/data/hostgroup/lsb.yaml?ref_type=heads#L158) add the egroup admin responsible for the tag, and the tag under it. For instance, the admin egroup is `admin-tagx` and the tag is `tagx` and the request was to build the tag to RHEL8 and AlmaLinux9:

    'admin-tagx':
        - 'tagx8el'
        - 'tagx9al'

Add it to git, commit, push, (wait for the MR to be reviewed if it's your first time).

**Note:** Please make sure to maintain the alphabetical sorting of the e-groups *and* tags. This helps us manage the long list of ACLs effectively. There's a extra test in the CI pipeline (`check_hiera_sorting`) that will verify this, make sure it passes before merging your changes.

**Note:** Do not forget to merge your changes to master as well once the MR has been accepted by another team member.

## Public tags

By default, tags will create repos under <https://linuxsoft.cern.ch/internal/repos/>. If the user requested a public tag/repo, it should be added to <https://gitlab.cern.ch/linuxsupport/cronjobs/koji-distrepo/-/blob/master/prod.config.yaml> as well, so it can appear under <https://linuxsoft.cern.ch/repos/>.

## Special Openstack build dependencies

The Cloud team sometimes needs build dependencies that are created by RDO but not shipped in their normal repos.
For example, the CS8 build dependencies for Wallaby are in the following Koji tag in CBS: [cloud8s-openstack-wallaby-el8-build](https://cbs.centos.org/koji/builds?latest=1&tagID=2230&order=nvr&inherited=0).
The contents of these tags will need to be manually synced via a special script called [sync-cbs](https://gitlab.cern.ch/linuxsupport/lxdist-build/-/blob/master/bin/sync-cbs).
This script should be run from `lxsoftadm01` as the `build` user and is generally a one-time operation. The
resulting repos (in this example, at <http://linuxsoft.cern.ch/mirror/cbs.centos.org/repos/cloud8s-openstack-wallaby-el8-build/>)
will then have to be added as external repos to Koji and to the required Koji tag (as always, via it's
configuration script).

**Why not just add this to the reposync cronjob?** Because that job downloads everything listed in a remote repo's
repodata, and the repodata of Koji buildtags includes *all* repositories added to the tag, such as BaseOS, AppStream,
etc. With `sync-cbs` we download only the packages in that tag and then generate new repodata. As these repos don't
generally change after a new RDO release, it should be sufficient to run this once.
