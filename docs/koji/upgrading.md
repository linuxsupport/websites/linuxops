# Upgrading koji

## Overview
The standard process to upgrade koji is:

1. Build the new RPM
2. Create a tag and tag it to qa
3. Upgrading Koji in the test environment:
    - Stopping all Koji services
    - Running the DB migration script (or anything else listed at <https://docs.pagure.org/koji/migrations/migrations/>
    - Upgrade the Koji RPMs
    - (Optional) Upgrade the ` edk2.git-ovmf-x64-0` RPM
    - Restarting Koji services
    - Re-enable all Koji services
    - Test it
4. Scheduling an OTG
5. On the intervetion day:
    - Tag it to production
    - Perform the same steps described in 3. for the production environment
6. Bravo!

Detailed information in the next items.

## Build new rpm and tag it appropriately

Our Koji rpm: <https://gitlab.cern.ch/linuxsupport/rpms/koji>

1. Get the new Koji upstream version

    a. go to <https://pagure.io/koji/releases> **OR** <https://koji.fedoraproject.org/koji/packageinfo?packageID=1181> to get the latest release

    b. in aiadm download the src.rpm `https://pagure.io/koji/archive/koji-1.33.1/koji-koji-1.33.1.tar.gz` **OR** `wget https://kojipkgs.fedoraproject.org//packages/koji/1.33.0/1.el8/src/koji-1.33.0-1.el8.src.rpm`

    c. install the src.rpm `rpm -ivh koji-1.33.0-1.el8.src.rpm` **OR**
    ```
    tar xvzf koji-koji-1.33.1.tar
    mv koji-koji-1.33.1 koji-1.33.1
    ```

    d. go to `cd ~/rpmbuild/SOURCES` **OR** create the `tar -cvjSf koji-1.33.1.tar.bz2 koji-1.33.1`

    e. copy the `koji-<>.tar.bz2` to `/koji/src` of <https://gitlab.cern.ch/linuxsupport/rpms/koji> and change the spec file.

    f. remove the previous `.tar.bz2` from the git repo and add the new one. Also add the modified spec file. Commit your changes!
    ```
    git rm src/koji-1.33.0.tar.bz2
    git add src/koji-1.33.1.tar.bz2
    git add koji.spec
    ```

2. Create a MR and check if the pipeline run without errors.

3. If there is a error on the pipeline related with the `prometheus-metrics-index.patch`, it means that it needs to be recreated.

    a. Create a copy of the last release of koji `koji-<last_version>_orig`

    b. Change the index.py in `/usr/share/koji-web/scripts` to add a metrics function that will call kojiexporter script

    ```
    +from kojiweb.kojiexporter import prometheus_metrics

    +
    +
    +def metrics(environ):
    +    _getServer(environ)
    +    _initValues(environ, 'PrometheusMetrics', 'metrics')
    +    environ['koji.headers'].append(['Content-Type', 'text/plain'])
    +    metrics = prometheus_metrics(environ)
    +    return metrics
    +
    ```

    c. Create the patch: `diff -urN koji-1.33.1_orig/www koji-1.33.1/www`

    d. Add it to `/koji/src`

4. When the pipeline succeeds, and it is reviewed by our colleagues, it is ready to merge to master!

5. Create a new tag

**Before you tag the rpm to `qa` make sure all koji.cern.ch nodes are on the production environment and all kojitest.cern.ch nodes are on the qa environment.**

If you do not do so, you may have prod nodes with qa repos which will make versions inconsistent across the LSB nodes.

**Note**: Don't forget to tag the build as needed **before** shutting down Koji!

## Upgrading Steps:

The following process applies for both test and prod nodes, so please first do it on kojitest.cern.ch involved nodes, adapting the commands when necessary.

**Note**: Don't worry if mco command doesn't work in the next steps, use wassh commands instead.

### 1) Disable alerts

**Note**: These wassh commands may not work for the case of 2FA protected nodes, so you may need to run it yourself from within those.

Make sure Roger knows something is going on:

```
 The roger status changes must be ran with sudo even when ssh'ing as root

# test
wassh -l root -c lsb/test2 'sudo roger update --all_alarms false --message OTGXXXXXX --duration 2h'

# prod (limit it to prod nodes, not the whole hostgroup)
wassh -l root -c lsb/hub 'sudo roger update --all_alarms false --message OTGXXXXXX --duration 2h'
wassh -l root -c lsb/web 'sudo roger update --all_alarms false --message OTGXXXXXX --duration 2h'
wassh -l root -c lsb/builder 'sudo roger update --all_alarms false --message OTGXXXXXX --duration 2h'
wassh -l root -c lsb/adm 'sudo roger update --all_alarms false --message OTGXXXXXX --duration 2h'
```

(If you change the appstate, the machines would be removed from the LB alias and the intervention will take longer)

### 2) Shutdown Koji

*puppet (disable)*

```
# test
mco puppet disable "koji upgrade OTGXXXXXX" --dm puppetdb -T lsb -F 'hostgroup_1=test2'

# prod (limit it to prod nodes, not the whole hostgroup)
mco puppet disable "koji upgrade OTGXXXXXX" --dm puppetdb -T lsb -F 'hostgroup_1=hub'
mco puppet disable "koji upgrade OTGXXXXXX" --dm puppetdb -T lsb -F 'hostgroup_1=web'
mco puppet disable "koji upgrade OTGXXXXXX" --dm puppetdb -T lsb -F 'hostgroup_1=builder'
mco puppet disable "koji upgrade OTGXXXXXX" --dm puppetdb -T lsb -F 'hostgroup_1=adm'



# If mco does not work for you:
# test
wassh -l root -c lsb/test2 'puppet agent --disable'

# prod
wassh -l root -c lsb/hub 'puppet agent --disable'
wassh -l root -c lsb/web 'puppet agent --disable'
wassh -l root -c lsb/builder 'puppet agent --disable'
wassh -l root -c lsb/adm 'puppet agent --disable'
```

Run the following command just after the tag the build to qa (test) /master (prod):

**Note:** After run the pipeline to `deploy_qa` / `deploy_master` wait a few minutes until koji_distrepo finishes.

*builders*

```
# test
mco service stop kojid --dm puppetdb -T lsb -F 'hostgroup_1=test2' -F 'hostgroup_2=builder'

# prod
mco service stop kojid --dm puppetdb -T lsb -F 'hostgroup_1=builder'


# If mco does not work for you:
# test
wassh -l root -c lsb/test2/builder 'service kojid stop'

# prod
wassh -l root -c lsb/builder 'service kojid stop'
```

*hub*

```
# test
mco service stop kojira --dm puppetdb -T lsb -F 'hostgroup_1=test2' -F 'hostgroup_2=hub'
mco service stop httpd --dm puppetdb -T lsb -F 'hostgroup_1=test2' -F 'hostgroup_2=hub'

# prod
mco service stop kojira --dm puppetdb -T lsb -F 'hostgroup_1=hub'
mco service stop httpd --dm puppetdb -T lsb -F 'hostgroup_1=hub'


# If mco does not work for you:
# test
wassh -l root -c lsb/test2/hub 'service kojira stop'
wassh -l root -c lsb/test2/hub 'service httpd stop'

# prod
wassh -l root -c lsb/hub 'service kojira stop'
wassh -l root -c lsb/hub 'service httpd stop'
```

*web*

```
# test
mco service stop httpd --dm puppetdb -T lsb -F 'hostgroup_1=test2' -F 'hostgroup_2=web'

# prod
mco service stop httpd --dm puppetdb -T lsb -F 'hostgroup_1=web'


# If mco does not work for you:
# test
wassh -l root -c lsb/test2/web 'service httpd stop'

# prod
wassh -l root -c lsb/web 'service httpd stop'
```

### 3) Backup and run migration script

*Extract migration script **if there is one***

On aiadm or lxplus:

```
$ wget https://pagure.io/koji/blob/master/f/docs/schema-upgrade-1.32-1.33.sql
```

Check if the file was downloaded correctly:

```
$ cat schema-upgrade-1.32-1.33.sql
```

Dump the existing db can be too heavy, for th eproduction case you can just do a backup in <https://dbod.web.cern.ch/pages/instance/koji> (Backup and Restore -> Create a Backup), and skip the next command.

```
pg_dump -h $dbod.cern.ch -p $port -d $database -U $username > kojitest_1.32-`date +%Y%m%d%H%M`.sql
```

Run migration script

```
psql -h $dbod.cern.ch -p $port -d $database -U $username < schema-upgrade-1.32-1.33.sql
```

**Note**: You can retrieve the credentials from `/etc/koji-hub/hub.conf` in a koji hub node or with `tbag`:

```
# test
tbag show koji_db_password --hg lsb/test2
# prod
tbag show koji_db_password --hg lsb
```

### 4) Upgrade Koji RPMs

```
# test
mco shell run '/usr/bin/yum clean all && /usr/local/sbin/distro_sync.sh' --dm puppetdb -T lsb -F 'hostgroup_1=test2'

# prod
mco shell run '/usr/bin/yum clean all && /usr/local/sbin/distro_sync.sh' --dm puppetdb -T lsb



# If mco does not work for you:
# test
wassh -l root -c lsb/test2 '/usr/bin/yum clean all && /usr/local/sbin/distro_sync.sh'

# prod
wassh -l root -c lsb/hub '/usr/bin/yum clean all && /usr/local/sbin/distro_sync.sh'
wassh -l root -c lsb/web '/usr/bin/yum clean all && /usr/local/sbin/distro_sync.sh'
wassh -l root -c lsb/builder '/usr/bin/yum clean all && /usr/local/sbin/distro_sync.sh'
wassh -l root -c lsb/adm '/usr/bin/yum clean all && /usr/local/sbin/distro_sync.sh'
```

### 5) (Optional) Upgrade ` edk2.git-ovmf-x64-0` RPM

This step is optional, as it is not strictly required to update the RPM. The reason behind the yumlock pinning is that since we are using nightly builds we have seen that certain updates make UEFI VM booting impossible, so we should better control the version we use at any given point to make sure it keeps on working.

Pinned at <https://gitlab.cern.ch/ai/it-puppet-module-koji/-/blob/qa/code/manifests/builder.pp#L48-57>.

To test it you would need to follow the usual Puppet environments workflow as per <https://configdocs.web.cern.ch/changes/environment.html>.

### 6) Restart Koji

```
mco puppet enable --dm puppetdb -T lsb -F 'hostgroup_1=test2'
mco puppet runonce --dm puppetdb -T lsb -F 'hostgroup_1=test2'
```

(You can also reboot the nodes to get the latest kernel, etc.)

### 7) Re-enable puppet and all_alarms

```
mco puppet enable --dm puppetdb -T lsb -F 'hostgroup_1=test2'

# test
wassh -l root -c lsb/test2 'puppet agent --enable'
wassh -l root -c lsb/test2 'sudo roger update --all_alarms true'

# prod
wassh -l root -c lsb/hub 'puppet agent --enable && sudo roger update --all_alarms true'
wassh -l root -c lsb/web 'puppet agent --enable && sudo roger update --all_alarms true'
wassh -l root -c lsb/builder 'puppet agent --enable && sudo roger update --all_alarms true'
wassh -l root -c lsb/adm 'puppet agent --enable &&  sudo roger update --all_alarms true'
```

### 8) Test

Once you have tested a few builds, tags, image-builds or else, you will have validated that the built rpm works as it should and you can announce the OTG.

You may want to install the test rpm from <linuxsoft.cern.ch/internal/repos/linuxsupport8al-qa/> (or the corresponding one for other distros) locally to do this. Please be aware of your `/etc/koji.conf` content, take it from `aiadm.cern.ch` if you need to adjust it.

Trigger the pipeline: <https://gitlab.cern.ch/linuxsupport/testing/koji-tester> - Change the koji variable if you are doing it to prod.


## Scheduling an OTG

After the Koji upgrade in the test environment, it is time to schedule the OTG to upgrade Koji in production.
Take [OTG0078591](https://cern.service-now.com/service-portal?id=outage&n=OTG0078591) as an example.
Try to schedule them with at least one week in advance.