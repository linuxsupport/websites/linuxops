# Related resources

## Databases

**Production**

* Databases: `dbod-koji.cern.ch:6600`
* User: `admin`
* Retrieve credentials with: `tbag show --hg "lsb" koji_db_password`
* Owner: `koji-admins`

**Test**

* Databases: `dbod-koji-dev.cern.ch:6600`
* User: `admin`
* Retrieve credentials with: `tbag show --hg "lsb/test2" koji_db_password`
* Owner: `koji-admins`

**Kojimon Grafana**

* Databases: `dbod-kojimon.cern.ch:6600`
* User: `grafana`
* Retrieve credentials with: `tbag show --hg "lsb" kojimon_psqldb_grafana`
* User: `admin`
* Retrieve credentials with: `tbag show --hg "lsb" kojimon_psqldb_admin`
* Owner: `koji-admins`
