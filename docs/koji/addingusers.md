# Adding users to koji

* Adding users to koji and granting them access to repositories requires admin privileges
* This can only be done by other admins

Since the automatic synchronization of koji users is in place with [koji_synchronize_users](https://gitlab.cern.ch/linuxsupport/cronjobs/koji_synchronize_users/):

* adding a CERN user in koji as a **new koji user** is feasible by adding their CERN account in ['koji-users' e-group](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10067587&AI_USERNAME=MANUEL&searchField=0&searchMethod=0&searchValue=koji-users&pageSize=30&hideSearchFields=false&searchMemberOnly=false&searchAdminOnly=false&AI_SESSION=8C164A0AA239178C6471F60D33303419). Additionally, they will need to be a member of one of the e-groups listed in [`koji::hub::acls`](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsb/-/blob/qa/data/hostgroup/lsb.yaml?ref_type=heads#L139).

* adding a CERN user in koji as a **new koji admin** (koji user with admin permissions) is feasible by adding their CERN account in ['koji-admins' e-group](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10067586&AI_USERNAME=GEARGYRI&searchField=0&searchMethod=0&searchValue=koji-admins&pageSize=30&hideSearchFields=false&searchMemberOnly=false&searchAdminOnly=false&AI_SESSION=2E951C254EE00E930595980C6C9F74B9) or ['lsb-experts' e-group](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10285648&AI_USERNAME=GEARGYRI&searchField=0&searchMethod=0&searchValue=lsb-experts&pageSize=30&hideSearchFields=false&searchMemberOnly=false&searchAdminOnly=false&AI_SESSION=2E951C254EE00E930595980C6C9F74B9) (since 'lsb-experts' e-group is a member of 'koji-admins' e-group)

## How the changes are applied
* [koji_synchronize_users](https://gitlab.cern.ch/linuxsupport/cronjobs/koji_synchronize_users/) runs every hour. Thus, you need to wait up to 1 hour for any change to take place.
* All new koji users or admins are added with  `--principal=<username>@CERN.CH`
* **koji users** that belong to an e-group listed in [`koji::hub::acls`](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsb/-/blob/qa/data/hostgroup/lsb.yaml?ref_type=heads#L139) will be given the appropriate `build-*` permissions.
* **koji admins** are users in koji with admin permissions.

## Deleting users from koji
* No user can be deleted from koji, but they can be deactivated and thus lose access to it!
* A user that is only in `koji-users` e-group and then deleted from it, will be auto-deactivated (within 1 hour).
* A user that is only in `koji-admins` e-group and then deleted from it, will be auto-deactivated (within 1 hour).
* A user that is in both e-groups (`koji-users` and `koji-admins`)
	* If deleted only from `koji-users`, no change will take place.
	* If deleted only from `koji-admins`, only the admin permissions will be revoked (downgraded to a **koji user**)
	* If deleted from both e-groups, will be auto-deactivated (within 1 hour).

## kojitest
* kojitest is a koji installation for testing purposes that is usually used from the members of the Linux team.
* All koji users in kojitest, are also koji admins.
* Only `koji-admins` e-group is taken into consideration by `koji_synchronize_users` for any action.


## Manual management of koji users
**This section is kept as a reference in case the automation described above doesn't work, or we need to manually intervene because of any other reason.**

* You can add a user with: `koji add-user --principal=<username>@CERN.CH <username>`

!!! danger "Important!"

	* Do NOT forget the @CERN.CH from the user principal
	* If you do, the user won't be able to log in and manual database modification is required to fix the mistake

* Note: If you get `GenericError: user already exists: user` you can safely ignore it
* Also add the new user to the [koji-user e-group](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10067587&AI_USERNAME=MANUEL&searchField=0&searchMethod=0&searchValue=koji-users&pageSize=30&hideSearchFields=false&searchMemberOnly=false&searchAdminOnly=false&AI_SESSION=8C164A0AA239178C6471F60D33303419), which provides the user with access to the Web Interface ([koji.cern.ch](http://koji.cern.ch))

### Granting build-* permissions

* First add the user as documented above. Then do `koji grant-permission build-* <username>`
* 'build-*' is required to do basically anything useful in koji

### Granting admin permissions

!!! danger "Important!"
	Admin accounts should only be granted to service managers of the koji service

* First add the user as documented above. Then do `koji grant-permission admin <username>`
