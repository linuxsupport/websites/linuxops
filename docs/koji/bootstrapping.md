# Bootstrapping a new distro

This document aims to describe how to create the Koji tag structure for a new
distro, following the schema we use for Alma 9. It is based on Koji tag inheritance
in order and the final structure looks like this:

* mytag9al-build
    * alma9-cern
        * cern9al-stable
            * buildsys9al-stable
                * alma9-defaults

([see example here](https://koji.cern.ch/taginfo?tagID=1981))


The following commands were executed from aiadm.

## Define external repos

The repo structure described here is based on the one created by [alma_snapshots](https://gitlab.cern.ch/linuxsupport/cronjobs/alma_snapshots).

```bash
koji add-external-repo alma9-baseos 'https://linuxsoft.cern.ch/cern/alma/9/BaseOS/$arch/os/'
koji add-external-repo alma9-appstream 'https://linuxsoft.cern.ch/cern/alma/9/AppStream/$arch/os/'
koji add-external-repo alma9-crb 'http://linuxsoft.cern.ch/cern/alma/9/CRB/$arch/os/'

koji add-external-repo alma9-testing-baseos 'https://linuxsoft.cern.ch/cern/alma/9-testing/BaseOS/$arch/os/'
koji add-external-repo alma9-testing-appstream 'https://linuxsoft.cern.ch/cern/alma/9-testing/AppStream/$arch/os/'
koji add-external-repo alma9-testing-crb 'http://linuxsoft.cern.ch/cern/alma/9-testing/CRB/$arch/os/'

koji add-external-repo alma9-latest-baseos 'https://linuxsoft.cern.ch/cern/alma/.9-latest/BaseOS/$arch/os/'
koji add-external-repo alma9-latest-appstream 'https://linuxsoft.cern.ch/cern/alma/.9-latest/AppStream/$arch/os/'
koji add-external-repo alma9-latest-crb 'http://linuxsoft.cern.ch/cern/alma/.9-latest/CRB/$arch/os/'
```

## Create the base tag

This is the tag that all others from this distro will inherit from.

```bash
koji add-tag alma9-defaults --arches="x86_64 aarch64"

koji add-external-repo -t alma9-defaults alma9-baseos -p 30 --mode bare
koji add-external-repo -t alma9-defaults alma9-appstream -p 40 --mode bare
koji add-external-repo -t alma9-defaults alma9-crb -p 50 --mode bare

koji add-group alma9-defaults build
koji add-group alma9-defaults srpm-build
koji add-group-pkg alma9-defaults build bash bzip2 coreutils cpio diffutils findutils gawk gcc gcc-c++ grep gzip info kernel-rpm-macros krb5-libs make patch python-rpm-macros python3-rpm-macros redhat-rpm-config rpm-build sed shadow-utils system-release tar unzip util-linux which xz
koji add-group-pkg alma9-defaults srpm-build bash bzip2 coreutils cpio diffutils findutils gawk gcc gcc-c++ grep gzip info kernel-rpm-macros krb5-libs make patch python-rpm-macros python3-rpm-macros redhat-rpm-config rpm-build sed shadow-utils system-release tar unzip util-linux which xz
```


## Create the buildsys tag

This is the tag where we'll build the `buildsys-macros-*` RPMs, which create the
dist tags our RPMs will be using.

Notice that the buildsys tag inherits from the base tag we just created.

```bash
$ cd bin/tags/
$ cat buildsys9al.sh 
#!/bin/bash

TAG="buildsys9al"
ARCHES="x86_64 aarch64"
PERMS="admin"


### You probably don't need to change anything below this
PARENT="alma9-defaults"
PROFILE="koji" # change it for the test instance
KOJI="/usr/bin/koji -p $PROFILE"

$KOJI remove-tag $TAG-build

$KOJI add-tag --arches "$ARCHES" --parent="$PARENT" $TAG-build
$KOJI add-tag --arches "$ARCHES" --parent="$PARENT" $TAG-testing
$KOJI add-tag --arches "$ARCHES" --parent="$PARENT" $TAG-qa
$KOJI add-tag --arches "$ARCHES" --parent="$PARENT" $TAG-stable

$KOJI edit-tag $TAG-testing --perm="$PERMS"
$KOJI edit-tag $TAG-qa      --perm="$PERMS"
$KOJI edit-tag $TAG-stable  --perm="$PERMS"

$KOJI add-target $TAG $TAG-build $TAG-testing

$ ./buildsys9al.sh 
No such tag: buildsys9al-build
```

## Create tag for EPEL

User tags get the EPEL repo via inheritance, so we need to create the external
repo and the tag that exposes it. This is done via inheritance (and not by simply
adding the external repo to the tag) because we want to make sure the base repos
have higher priority than EPEL. In Koji, external repos added to a tag have higher
priority than inherited ones, regardless of the actual priority you set.

This may be tricky because you may be doing this too early and there's no EPEL
version for the distro you're adding. You can probably use the previous version
for now. Good luck!

```bash
koji add-external-repo epel9 'https://linuxsoft.cern.ch/epel/9/Everything/$arch/'

$ cat epel9.sh 
#!/bin/bash

TAG="epel9"

koji add-tag --arches "x86_64 aarch64" $TAG
koji add-external-repo --tag=$TAG epel9 -p 60 --mode=bare

$ ./epel9.sh 
```

## Create the cern tag

This is the tag where we'll build all our core RPMs, such as `centos-release`, etc.

This tag inherits from the buildsys tag we just created.

```bash
$ cat cern9al.sh 
#!/bin/bash

TAG="cern9al"
ARCHES="x86_64 aarch64"
PERMS="admin"
EPEL=1
TESTING=1
DISTTAG="al9.cern"

### You probably don't need to change anything below this
PARENT="buildsys9al-stable"
PROFILE="koji" # change it for the test instance
KOJI="/usr/bin/koji -p $PROFILE"

$KOJI remove-tag $TAG-build

$KOJI add-tag --arches "$ARCHES" --parent="$PARENT" $TAG-build
$KOJI add-tag --arches "$ARCHES" --parent="$PARENT" $TAG-testing
$KOJI add-tag --arches "$ARCHES" --parent="$PARENT" $TAG-qa
$KOJI add-tag --arches "$ARCHES" --parent="$PARENT" $TAG-stable

if [[ $DISTTAG != "" ]]; then
  $KOJI add-group-pkg $TAG-build build buildsys-macros-$DISTTAG
  $KOJI add-group-pkg $TAG-build srpm-build buildsys-macros-$DISTTAG
fi

if [[ $EPEL -eq 1 ]]; then
  $KOJI add-tag-inheritance --priority 10 ${TAG}-build epel9
fi

if [[ $TESTING -eq 1 ]]; then
  $KOJI add-external-repo -t $TAG-build alma9-testing-baseos -p 25 --mode=bare
  $KOJI add-external-repo -t $TAG-build alma9-testing-appstream -p 35 --mode=bare
  $KOJI add-external-repo -t $TAG-build alma9-testing-crb -p 45 --mode=bare
fi

$KOJI edit-tag $TAG-testing --perm="$PERMS"
$KOJI edit-tag $TAG-qa      --perm="$PERMS"
$KOJI edit-tag $TAG-stable  --perm="$PERMS"

$KOJI add-target $TAG $TAG-build $TAG-testing
```

Now run the script to create the tag:

```bash
$ ./cern9al.sh 
No such tag: cern9al-build
Added external repo alma9-testing-baseos to tag cern9al-build (priority 25)
Added external repo alma9-testing-appstream to tag cern9al-build (priority 35)
Added external repo alma9-testing-crb to tag cern9al-build (priority 45)
```

The `cern` tag is special because it includes a [comps.xml](https://gitlab.cern.ch/linuxsupport/comps.xml)
file, which sets up certain groups of packages. You'll have to add an xml file
in that repo, creating the named groups. Without those groups, repo validations
done in the snapshotting scripts will fail.

Now you need to add the CERN tag as an external repo (as it's used by the
[image builds tag](#create-tag-for-image-builds) that you'll create in a little while.
However, the repos don't exist yet because no package has been tagged, so
[Koji dist-repo](https://gitlab.cern.ch/linuxsupport/cronjobs/koji-distrepo) hasn't run yet, and
also the [snapshotting](https://gitlab.cern.ch/linuxsupport/cronjobs/alma_snapshots) script hasn't picked it up yet.

You can wait a day for the Koji dist-repo Scheduler job to run, or you can run it manually.
Once that's happened, you can wait until the next morning for the snapshotting script
to do it's magic, or you can once again run it manually. You might have to force
production to point to the new snapshot, otherwise the prod repo will still be missing/broken.

Once all that is done, you can finally add the CERN tag as an external repo:

```bash
koji add-external-repo alma9-cern 'http://linuxsoft.cern.ch/cern/alma/9/CERN/$arch/'
koji add-external-repo alma9-testing-cern 'http://linuxsoft.cern.ch/cern/alma/9-testing/CERN/$arch/'
koji add-external-repo alma9-latest-cern 'http://linuxsoft.cern.ch/cern/alma/.9-latest/CERN/$arch/'
```

## Create base for user tags

Now we start to bring it all together with a tag that user tags will inherit from:

```bash
koji add-tag --arches "x86_64 aarch64" --parent cern9al-stable alma9-cern
koji add-group-pkg alma9-cern srpm-build cern-koji-addons
koji add-group-pkg alma9-cern build cern-koji-addons
```

## Create script to create user tags

Make a copy of [createtag9al.sh](https://gitlab.cern.ch/linuxsupport/lxdist-build/-/blob/master/bin/tags/createtag9al.sh)
(or whatever the latest version is) for your new distro and adapt as needed:

```bash
$ cat createtag9al.sh 
# This script is to be executed from within the other *9al.sh scripts
if [[ "${TAG: -3}" != "9al" ]]; then
  echo "Error: The tag name must end with a 9al (ie. 'mytag9al')"
  exit 1
fi

ARCHES="${ARCHES:-x86_64 aarch64}"
PERMS="${PERMS:-}"
TESTING="${TESTING:-0}"
EPEL="${EPEL:-0}"
CLOUD="${CLOUD:-}"
DISTTAG="${DISTTAG:-al9.cern}"

PARENT="${PARENT:-alma9-cern}"
PROFILE="${PROFILE:-koji}" # change it for the test instance
KOJI="${KOJI:-/usr/bin/koji -p $PROFILE}"

$KOJI remove-tag ${TAG}-build

$KOJI add-tag --arches "$ARCHES" --parent="$PARENT" ${TAG}-build
$KOJI add-tag --arches "$ARCHES" ${TAG}-testing
$KOJI add-tag --arches "$ARCHES" ${TAG}-qa
$KOJI add-tag --arches "$ARCHES" ${TAG}-stable

if [[ $DISTTAG != "" ]]; then
  $KOJI add-group-pkg ${TAG}-build build buildsys-macros-${DISTTAG}
  $KOJI add-group-pkg ${TAG}-build srpm-build buildsys-macros-${DISTTAG}
fi

if [[ $EPEL -eq 1 ]]; then
  $KOJI add-tag-inheritance --priority 10 ${TAG}-build epel9
fi

if [[ $TESTING -eq 1 ]]; then
  $KOJI add-external-repo -t ${TAG}-build alma9-testing-baseos -p 25 --mode=bare
  $KOJI add-external-repo -t ${TAG}-build alma9-testing-appstream -p 35 --mode=bare
  $KOJI add-external-repo -t ${TAG}-build alma9-testing-crb -p 45 --mode=bare
fi

if [[ $CLOUD != "" ]]; then
  $KOJI add-external-repo -t ${TAG}-build alma9-openstack-${CLOUD} -p 15 --mode=bare
  $KOJI add-external-repo -t ${TAG}-build alma9-openstack-${CLOUD}-builddep -p 20 --mode=bare
  $KOJI add-external-repo -t ${TAG}-build alma9-openstack-${CLOUD}-deps -p 25 --mode=bare
fi

$KOJI edit-tag ${TAG}-testing --perm="$PERMS"
$KOJI edit-tag ${TAG}-qa      --perm="$PERMS"
$KOJI edit-tag ${TAG}-stable  --perm="$PERMS"

$KOJI add-tag-inheritance --priority 5 ${TAG}-build ${TAG}-testing
$KOJI add-target ${TAG} ${TAG}-build ${TAG}-testing

```



## Create tag for openafs

We need a tag for the openafs kernel modules, we can create it using the
script we created [previously](#create-script-to-create-user-tags).

```bash
$ cat openafs9al.sh 
#!/bin/bash

TAG="openafs9al"
#ARCHES="x86_64 aarch64"
#PERMS=""
TESTING=1
#EPEL=0
DISTTAG="al9.cern"

### You probably don't need to change anything below this
source createtag9al.sh

$KOJI add-external-repo -t ${TAG}-build alma9-baseos -p 20 --mode=bare
$KOJI add-external-repo -t ${TAG}-build alma9-appstream -p 30 --mode=bare
# As we may need dependencies apart from the 'latest' build,
# we add -stable to ensure that we have everything
$KOJI add-tag-inheritance --priority 3 ${TAG}-build ${TAG}-stable
# However, as kmod-openafs is built from the 'openafs' package, it's often the
# case that koji only sees the latest kmod package. We we may expect to see
# other packages that are also built from 'openafs' (such as openafs-devel)
# We can override the default koji behaviour to show all packages, which avoids
# this unfortunate confusion
$KOJI edit-tag ${TAG}-build -x repo_include_all=True

$ ./openafs9al.sh 
No such tag: openafs9al-build
Added external repo alma9-testing-baseos to tag openafs9al-build (priority 25)
Added external repo alma9-testing-appstream to tag openafs9al-build (priority 35)
Added external repo alma9-testing-crb to tag openafs9al-build (priority 45)
Added external repo alma9-baseos to tag openafs9al-build (priority 20)
Added external repo alma9-appstream to tag openafs9al-build (priority 30)
```

Now we can go ahead and add the openafs package to that tag, or we can do it later:

```bash
$ for i in testing qa stable; do koji add-pkg --owner=kojici openafs9al-$i openafs; done
Adding 1 packages to tag openafs9al-testing
Adding 1 packages to tag openafs9al-qa
Adding 1 packages to tag openafs9al-stable
```

## Create tag for image builds

Now we need to create the tag we'll use to build Docker and cloud images.
Note that this tag is completely independent from all the others, as it
should only use external repositories.

```bash
[aiadm10] ~/lxdist-build/bin/tags (master) > cp image8.sh image8s.sh
[aiadm10] ~/lxdist-build/bin/tags (master) > vim image8s.sh
[aiadm10] ~/lxdist-build/bin/tags (master) > cat image8s.sh
#!/bin/bash

DIST="cs8-image-8x"

koji add-tag --arches "x86_64 aarch64" $DIST-build

koji add-group $DIST-build build
koji add-group $DIST-build srpm-build
koji add-group-pkg $DIST-build build bash bzip2 coreutils cpio diffutils findutils gawk gcc gcc-c++ grep gzip info kernel-rpm-macros make patch python-rpm-macros python3-rpm-macros redhat-rpm-config rpm-build sed shadow-utils system-release tar unzip util-linux which xz
koji add-group-pkg $DIST-build srpm-build bash bzip2 coreutils cpio diffutils findutils gawk gcc gcc-c++ grep gzip info kernel-rpm-macros make patch python-rpm-macros python3-rpm-macros redhat-rpm-config rpm-build sed shadow-utils system-release tar unzip util-linux which xz

koji add-external-repo --tag=$DIST-build cs8-cern -p 20 --mode=bare
koji add-external-repo --tag=$DIST-build cs8-baseos -p 30 --mode=bare
koji add-external-repo --tag=$DIST-build cs8-appstream -p 40 --mode=bare
koji add-external-repo --tag=$DIST-build cs8-powertools -p 50 --mode=bare

koji add-tag $DIST

koji add-target $DIST $DIST-build $DIST
[aiadm10] ~/lxdist-build/bin/tags (master) > bash image8s.sh
Added external repo cs8-cern to tag cs8-image-8x-build (priority 20)
Added external repo cs8-baseos to tag cs8-image-8x-build (priority 30)
Added external repo cs8-appstream to tag cs8-image-8x-build (priority 40)
Added external repo cs8-powertools to tag cs8-image-8x-build (priority 50)
```

We can now go ahead and add the only two packages we'll ever build in this tag:

```bash
[aiadm10] ~ > for i in cs8-cloud cs8-docker-base; do koji add-pkg --owner=kojici cs8-image-8x $i; done
Adding 1 packages to tag cs8-image-8x
Adding 1 packages to tag cs8-image-8x
```

## Create tag for mytag (rpmci testing)

We need a tag for testing `rpmci`, we can create it using the
script we created [previously](#create-script-to-create-user-tags).

```bash
$ cat mytag9al.sh 
#!/bin/bash

TAG="mytag9al"
#ARCHES="x86_64 aarch64"
#PERMS=""
#TESTING=0
#EPEL=0
#DISTTAG=""

### You probably don't need to change anything below this
source createtag9al.sh
```

Now we can go ahead and add the myrpm package to that tag, or we can do it later:

```bash
$ for i in testing qa stable; do koji add-pkg --owner=kojici mytag9al-$i myrpm; done
Adding 1 packages to tag mytag9al-testing
Adding 1 packages to tag mytag9al-qa
Adding 1 packages to tag mytag9al-stable
```

## Commit your changes
Don't forget to commit the files you've created so far:

```bash
[aiadm10] ~/lxdist-build/bin/tags (master) > git add createtag9al.sh buildsys9al.sh cern9al.sh epel9al.sh openafs9al.sh image9al.sh mytag9al.sh
[aiadm10] ~/lxdist-build/bin/tags (master) > git commit createtag9al.sh buildsys9al.sh cern9al.sh epel9al.sh openafs9al.sh image9al.sh mytag9al.sh -m "Added Alma9 scripts"
[aiadm10] ~/lxdist-build/bin/tags (master) > git push origin master
```

## Build a Docker image

You'll need a Docker image for many things, starting with `rpmci`. You will have to
frankenstein it a bit because you haven't built all the usual CERN packages yet.
Your frankenimage may not pass the tests, but that's ok right now. You may also
use an upstream image at this point.

You can start with [c8-base](https://gitlab.cern.ch/linuxsupport/c8-base) as a model and
adapt it as needed.

## Add new distro to rpmci

Most of our RPMs are now created using [rpmci](https://gitlab.cern.ch/linuxsupport/rpmci),
so it needs to be adapted to know about the new distro for the next few steps to work.
You can test the new distro support with [myrpm](https://gitlab.cern.ch/linuxsupport/myrpm).

You'll have to build an rpmci worker image based off of your docker image and
[integrate it](https://gitlab.cern.ch/linuxsupport/rpmci/-/compare/8e9d5203...db470a66) into rpmci.

You may need to temporarily disable the tests of some of the RPMs you'll build later on,
as they may not work on your Frankenstein'd image.

## Start building basic packages

Now you have to start building some packages. In most cases, you should be able
to simply update the `.gitlab-ci.yml` file to add the new [rpmci distro](#add-new-distro-to-rpmci)
and tag your your newest commit (provided nothing else
changed in the meantime, of course). The builds for other distros will fail because
a build with the same version already exists, but you'll still be able to tag the
build for the new distro.

Don't forget that you'll have to add these packages to your newly-created tags.

As you tag packages, new repos should start appearing for your tags in [linuxsoft](http://linuxsoft.cern.ch/internal/repos/).
If they do not, check that the [koji-distrepo](https://gitlab.cern.ch/linuxsupport/cronjobs/koji-distrepo)
job is running correctly, that it's being called from the [koji plugin](https://gitlab.cern.ch/linuxsupport/rpms/koji-hub-plugins-cern).

### `buildsys-macros-*`

You'll have to build some new [`buildsys-macros-*` RPMs](https://gitlab.cern.ch/linuxsupport/rpms/buildsys)
into your new `buildsys*` tag first. `buildsys-macros-el8s.cern` (or equivalent)
will be needed first, as that's the dist tag used by packages in the cern tag
you created [previously](#create-a-script-to-create-the-cern-tag).

### `cern-koji-addons`

Then you'll have to build a new [`cern-koji-addons`](https://gitlab.cern.ch/linuxsupport/rpms/cern-koji-addons)
into your new `cern*` tag. This package is added to the build and srpm-build koji
package groups in the `centos*-cern` you created [previously](#create-base-for-user-tags).

### `CERN-CA-certs`

[This package](https://gitlab.cern.ch/linuxsupport/rpms/cern-ca-certs) is also
required by other stuff, so it's best to get it in early to avoid surprises.

### `cern-gpg-keys` and `*-release`

Where appropriate, you probably also want to build a `*-release` RPM to
configure a machine to point to your new mirrors. You will need to do this before
you can build cloud and Docker images. You'll have to build `cern-gpg-keys` first, of course.

### Build other "basic" packages

[`cern-wrappers`](https://gitlab.cern.ch/linuxsupport/rpms/cern-wrappers), [`cern-dracut-conf`](https://gitlab.cern.ch/linuxsupport/rpms/cern-dracut-conf) and
[`cern-krb5-conf`](https://gitlab.cern.ch/linuxsupport/rpms/cern-krb5-conf) are installed
in the Docker and cloud images, so they need to be built now.

Additionally, [`cern-linuxsupport-access`](https://gitlab.cern.ch/linuxsupport/rpms/cern-linuxsupport-access),
[`phonebook`](https://gitlab.cern.ch/linuxsupport/rpms/phonebook) and [`useraddcern`](https://gitlab.cern.ch/linuxsupport/rpms/useraddcern) are installed
as part of the [CERN functional tests](https://gitlab.cern.ch/linuxsupport/testing/cern_centos_functional_tests/-/blob/master/tests/cern_basic/1_yum_installs.sh)
(which will run against the Docker image to validate it), so they have to be built now.

[`epel-release`](https://gitlab.cern.ch/linuxsupport/rpms/releases/epel-release) may
be needed by the rpmci Docker images, if it exists and makes sense for your new distro.

Other packages will be needed for the cloud image:

* [`msktutil`](https://gitlab.cern.ch/linuxsupport/rpms/msktutil)
* [`perl-WWW-Curl`](https://gitlab.cern.ch/linuxsupport/rpms/perl-www-curl)
* [`perl-Authen-Krb5`](https://gitlab.cern.ch/linuxsupport/rpms/perl-authen-krb5)
* [`cern-get-keytab`](https://gitlab.cern.ch/linuxsupport/rpms/cern-get-keytab)

These packages will be needed for openafs:

* [`cern-yum-tool`](https://gitlab.cern.ch/linuxsupport/rpms/cern-yum-tool)
* [`openafs-release`](https://gitlab.cern.ch/linuxsupport/rpms/releases/openafs-release)

The `cern` tag's [`comps.xml`](https://gitlab.cern.ch/linuxsupport/comps.xml)
file will probably list other RPMs you need to build, such as:

* [`locmap-firstboot`](https://gitlab.cern.ch/linuxsupport/rpms/locmap/locmap-firstboot)
* [`cern-anaconda-addon`](https://gitlab.cern.ch/linuxsupport/rpms/cern-anaconda-addon)
* [`hepix`](https://gitlab.cern.ch/linuxsupport/rpms/hepix)
* [`cern-get-sso-cookie`](https://gitlab.cern.ch/linuxsupport/rpms/cern-get-sso-cookie) (replaced by [`auth-get-sso-cookie`](https://gitlab.cern.ch/authzsvc/tools/auth-get-sso-cookie), managed by the Auth Team)
* [`yum-autoupdate`](https://gitlab.cern.ch/linuxsupport/rpms/yum-autoupdate)
* [`sicgsfilter`](https://gitlab.cern.ch/linuxsupport/rpms/sicgsfilter)
* [`lpadmincern`](https://gitlab.cern.ch/linuxsupport/rpms/lpadmincern)
* [`cern-config-users`](https://gitlab.cern.ch/linuxsupport/rpms/cern-config-users) (deprecated by [locmap](https://gitlab.cern.ch/linuxsupport/rpms/locmap))
* [`cern-sssd-conf`](https://gitlab.cern.ch/linuxsupport/rpms/cern-sssd-conf)


## Start creating new tags

Now you can start creating other tags as necessary. It's probably a good idea
to start with `myrpm` (for testing `rpmci`), `linuxsupport` (for our packages)
and probably `config` (for Configuration Management stuff).

The `config` tag is a bit special because it is used very early on in the puppetinit.
In order to avoid hardcoding the URLs for the repos, we provide a .repo file
in a well-known location: `http://linuxsoft.cern.ch/internal/repos/configX.repo`.
Take a look at [this one](http://linuxsoft.cern.ch/internal/repos/config8s.repo) and
modify it as necessary.

## Rebuild the Docker image

Now that we have the basic RPMs, it's time to remove the hacks you did to get
an initial docker image and build a proper one. The tests have to pass this time!

Once you have a working Docker image, you can create a scheduled pipeline to
[rebuild it every month](https://gitlab.cern.ch/linuxsupport/c8-base/-/pipeline_schedules).

## Build a new worker image for rpmci

Now that you have a real Docker image for your new distro, you should rebuild the
rpmci worker to use it.

Once that is done, you can go back through all the previous RPMs you created
where you had to disable the tests and enable them again. Now that your image is
good, they should succeed.

## Build a cloud image

### Prepare Imageci

Before you can actually build the cloud image, you should prepare the
[imageci](https://gitlab.cern.ch/linuxsupport/testing/image-ci) to test it. You'll
have to do [something like this](https://gitlab.cern.ch/linuxsupport/testing/image-ci/-/commit/69de3bf3d46d729f709a34ffff76609511e5d086).

### Build the image

Now that you have the testing infrastructure set up and all the necessary
RPMs built, you can prepare a kickstart template for your new distro. You'll
need to do [something like this](https://gitlab.cern.ch/linuxsupport/koji-image-build/-/commit/622b3632d9f1e6e89705e77694246cbfc5b2c068).

## Upload the AIMS images

You could have done this earlier, but now's as good a time as any. You have to
upload an `vmlinuz` and `initrd.img` of your new distro to AIMS2:

```bash
[aiadm10] ~ > wget http://linuxsoft.cern.ch/cern/centos/s8-testing/BaseOS/x86_64/os/images/pxeboot/{vmlinuz,initrd.img}
--2021-02-24 18:49:15--  http://linuxsoft.cern.ch/cern/centos/s8-testing/BaseOS/x86_64/os/images/pxeboot/vmlinuz
Resolving linuxsoft.cern.ch (linuxsoft.cern.ch)... 2001:1458:d00:2e::100:3e3, 2001:1458:d00:3a::100:1ec, 2001:1458:d00:13::451, ...
Connecting to linuxsoft.cern.ch (linuxsoft.cern.ch)|2001:1458:d00:2e::100:3e3|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 9936008 (9.5M)
Saving to: ‘vmlinuz’

vmlinuz                            100%[================================================================>]   9.48M  32.3MB/s    in 0.3s

2021-02-24 18:49:16 (32.3 MB/s) - ‘vmlinuz’ saved [9936008/9936008]

--2021-02-24 18:49:16--  http://linuxsoft.cern.ch/cern/centos/s8-testing/BaseOS/x86_64/os/images/pxeboot/initrd.img
Reusing existing connection to [linuxsoft.cern.ch]:80.
HTTP request sent, awaiting response... 200 OK
Length: 74289512 (71M) [application/octet-stream]
Saving to: ‘initrd.img’

initrd.img                         100%[================================================================>]  70.85M   101MB/s    in 0.7s

2021-02-24 18:49:21 (101 MB/s) - ‘initrd.img’ saved [74289512/74289512]

FINISHED --2021-02-24 18:49:21--
Total wall clock time: 6.4s
Downloaded: 2 files, 80M in 1.0s (80.5 MB/s)
[aiadm10] ~ > aims2 addimg --name CS8_X86_64_TEST --arch x86_64 --vmlinuz vmlinuz --initrd initrd.img --description "CENTOS STREAM 8 (20210224) X86_64 TEST" --uefi
Uploading to server:
  vmlinuz
    file: vmlinuz
    md5:  cc5678f48665e361f2bba7df10530716
    size: 9936008
  initrd
    file: initrd.img
    md5:  35c0aaa6f80c34a95a24d86f56195367
    size: 74289512
(this will take 20-30 seconds for typical linux vmlinuz/initrd sizes)
100.0% wait ...
vmlinuz upload completed
100.0%
initrd upload completed
Image CS8_X86_64_TEST uploaded to aims.
```

For more details, consult the [AIMS2 documentation](/aims2/addinganewimage/).

## Start getting Puppet ready

You'll need to create a [new tag](https://gitlab.cern.ch/linuxsupport/lxdist-build/-/commit/128424d237e8121864d9e1cf826ed5911a43d4a9)
for the Config team, and possibly a [new disttag](https://gitlab.cern.ch/linuxsupport/rpms/buildsys/buildsys-macros-configX).

Now give a heads up to your Config team colleagues so they start [getting things ready](https://its.cern.ch/jira/browse/AI-6077).
They will need to build a new puppet-agent, add the OS to Foreman and probably make some changes to Puppet modules, such as osrepos.

There will be other basic Koji tags that they'll probably need at this point, such
as `collectd*`, `monit*` and `hw*`, so now is a good time to create them.

## Build OpenAFS Kernel Modules

The [openafs](https://gitlab.cern.ch/linuxsupport/rpms/openafs) repo includes
pipelines to build kernel modules which are [triggered](https://gitlab.cern.ch/linuxsupport/cronjobs/stream8_snapshots/-/blob/8f7d9320/stream8_snapshots/triggers.sh)
from the snapshotting scripts. Those pipelines will need [some changes](https://gitlab.cern.ch/linuxsupport/rpms/openafs/-/commit/8f061a82907d765cfe9e9948a31fc2ac57704051)
to deal with the new OS.


## Specifics for a new RHEL distro

The following won't necessarily be needed for new distros, but is added here in case it helps in the future.

### Creating base RHEL 9 tag

```bash
[aiadm43] ~ > PROFILE=koji
[aiadm43] ~ > OS=9
[aiadm43] ~ > koji -p $PROFILE add-external-repo rhel${OS}-baseos              'http://linuxsoft.cern.ch/cdn.redhat.com/content/dist/rhel9/9/$arch/baseos/os/'
Created external repo 461
[aiadm43] ~ > koji -p $PROFILE add-external-repo rhel${OS}-appstream           'http://linuxsoft.cern.ch/cdn.redhat.com/content/dist/rhel9/9/$arch/appstream/os/'
Created external repo 462
[aiadm43] ~ > koji -p $PROFILE add-external-repo rhel${OS}-codeready-builder   'http://linuxsoft.cern.ch/cdn.redhat.com/content/dist/rhel9/9/$arch/codeready-builder/os/'
Created external repo 463
[aiadm43] ~ > koji -p $PROFILE add-external-repo buildsys${OS}s                'http://linuxsoft.cern.ch/internal/repos/buildsys9-stable/$arch/os/'
Created external repo 464
[aiadm43] ~ > koji -p $PROFILE add-tag rhel${OS}-defaults --arches="x86_64 aarch64"
[aiadm43] ~ > koji -p $PROFILE add-external-repo -t rhel${OS}-defaults buildsys${OS}s -p 20 --mode bare
[aiadm43] ~ > koji -p $PROFILE add-external-repo -t rhel${OS}-defaults rhel${OS}-baseos -p 30 --mode bare
Added external repo rhel9-baseos to tag rhel9-defaults (priority 30)
[aiadm43] ~ > koji -p $PROFILE add-external-repo -t rhel${OS}-defaults rhel${OS}-appstream -p 40 --mode bare
Added external repo rhel9-appstream to tag rhel9-defaults (priority 40)
[aiadm43] ~ > koji -p $PROFILE add-external-repo -t rhel${OS}-defaults rhel${OS}-codeready-builder -p 50 --mode bare
Added external repo rhel9-codeready-builder to tag rhel9-defaults (priority 50)
[aiadm43] ~ > koji -p $PROFILE add-group rhel${OS}-defaults build
[aiadm43] ~ > koji -p $PROFILE add-group rhel${OS}-defaults srpm-build
[aiadm43] ~ > koji -p $PROFILE add-group-pkg rhel${OS}-defaults build bash bzip2 coreutils cpio diffutils findutils gawk gcc gcc-c++ grep gzip info kernel-rpm-macros krb5-libs make patch python-rpm-macros python3-rpm-macros redhat-rpm-config rpm-build sed shadow-utils system-release tar unzip util-linux which xz
[aiadm43] ~ > koji -p $PROFILE add-group-pkg rhel${OS}-defaults srpm-build bash bzip2 coreutils cpio diffutils findutils gawk gcc gcc-c++ grep gzip info kernel-rpm-macros krb5-libs make patch python-rpm-macros python3-rpm-macros redhat-rpm-config rpm-build sed shadow-utils system-release tar unzip util-linux which xz
```

### Create base EPEL 9 tag

Remember, EPEL has two repos now: epel9 and epel-next9. CS systems need both, RHEL systems have to use only the first one.

```bash
[aiadm43] ~/lxdist-build/bin/tags (master) > cat epel9.sh
#!/bin/bash

TAG="epel9"

koji add-tag --arches "x86_64 aarch64" $TAG
koji add-external-repo --tag=$TAG epel9 -p 60 --mode=bare

[aiadm43] ~/lxdist-build/bin/tags (master) > bash epel9.sh
Added external repo epel9 to tag epel9 (priority 60)
```

### Create script to create user tags for RHEL 9

```bash
[aiadm43] ~/lxdist-build/bin/tags (master) > cat createtag9el.sh
# This script is to be executed from within the other *9el.sh scripts
if [[ "${TAG: -3}" != "9el" ]]; then
  echo "Error: The tag name must end with a 9el (ie. 'mytag9el')"
  exit 1
fi

ARCHES="${ARCHES:-x86_64 aarch64}"
PERMS="${PERMS:-}"
EPEL="${EPEL:-0}"
DISTTAG="${DISTTAG:-el9}"

PARENT="${PARENT:-rhel9-defaults}"
PROFILE="${PROFILE:-koji}" # change it for the test instance
KOJI="${KOJI:-/usr/bin/koji -p $PROFILE}"

$KOJI remove-tag ${TAG}-build

$KOJI add-tag --arches "$ARCHES" --parent="$PARENT" ${TAG}-build
$KOJI add-tag --arches "$ARCHES" ${TAG}-testing
$KOJI add-tag --arches "$ARCHES" ${TAG}-qa
$KOJI add-tag --arches "$ARCHES" ${TAG}-stable

if [[ $DISTTAG != "" ]]; then
  $KOJI add-group-pkg ${TAG}-build build buildsys-macros-${DISTTAG}
  $KOJI add-group-pkg ${TAG}-build srpm-build buildsys-macros-${DISTTAG}
fi

if [[ $EPEL -eq 1 ]]; then
  $KOJI add-tag-inheritance --priority 10 ${TAG}-build epel9
fi

$KOJI edit-tag ${TAG}-testing --perm="$PERMS"
$KOJI edit-tag ${TAG}-qa      --perm="$PERMS"
$KOJI edit-tag ${TAG}-stable  --perm="$PERMS"

$KOJI add-tag-inheritance --priority 1 ${TAG}-build ${TAG}-testing
$KOJI add-target ${TAG} ${TAG}-build ${TAG}-testing
```

### Create template for user tags for RHEL 9

```bash
[aiadm43] ~/lxdist-build/bin/tags (master) > cat createtag9el.template
#!/bin/bash

TAG="mynewtag9el"
#ARCHES="x86_64 aarch64"
#PERMS=""
#TESTING=0
#EPEL=0
#DISTTAG=""

### You probably don't need to change anything below this
source createtag9el.sh
```

### Create user tags for RHEL 9

```bash
[aiadm43] ~/lxdist-build/bin/tags (master) > cat openafs9el.sh
#!/bin/bash

TAG="openafs9el"
#ARCHES="x86_64 aarch64"
#PERMS=""
TESTING=1
#EPEL=0
DISTTAG="rh9.cern"

### You probably don't need to change anything below this
source createtag9el.sh
```
