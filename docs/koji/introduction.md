# Koji build system at CERN

Koji is an open source tool, written in Python, used and developed primarily by Red Hat employees. It is a system for building and tracking binary RPMs, Cloud and Docker images.
Koji enables users to create tags, that represent internal repository names. The user submits a source RPM or a git repository, and Koji builds a binary RPM associated with a tag.
Koji supports building for many architectures, such as x86_64, aarch64 and i386, which are the ones being used at CERN.

## Koji components

Koji is comprised of several components:

- `koji-hub` is the center of all Koji operations. It is the only component that has direct access to the PostgreSQL database and is one of the two components that have write access to the shared file system. The other one is kojira.

- `kojid` is the build daemon that runs on each of the build machines. Its primary responsibility is polling for incoming build jobs and handling them accordingly. The machines with kojid installed are called builders.

- `kojira` is a daemon that handles buildroot repos. It checks if any builds were added to buildroot or build tag configuration has changed. If so, it triggers a task to update the buildroot.

- `koji-web` is a web frontend to Koji.

- `koji-client` is a CLI written in Python that allows users and admins to interact with Koji.

[!NOTE]
Koji builders don’t have access to the internet - that’s why it can not build if the package is trying to download external dependencies.

## Koji client configuration

The Koji client is available as part of the Koji package. The upstream package is rebuilt at CERN every time there is a new version, because that way we can include patches needed
to be supported at CERN, and CERN customisations. It is firstly deployed in the testing environment and kept during one week before deploying to production, to ensure the new version didn’t introduce any regressions.
To use the CERN Koji service, each user needs to request access to the Linux Team, which has an automated process to grant build permissions. The synchronization script is scheduled
to run every day within a container in Nomad. It is important to control service’s users, what tags they are responsible for, and Koji’s ACLs to avoid security vulnerabilities.

## Koji concepts: Tags, Packages and Hosts

Koji organizes packages using tags, and all CERN tags in Koji have a corresponding internal repository on http://linuxsoft.cern.ch/internal/repos.
There are three different concepts about packages in Koji: the package itself, a build, and the RPM. The package is the name of a source RPM. A build of a package includes all the architectures and subpackages. An RPM created by a build operation has a specific architecture and subpackage of a build.

For instance, mytag9al−testing tag has the package, myrpm.

- Tag: mytag9al−testing

- Package: myrpm

- Build: myrpm−1.1−29.el9

- RPM: myrpm−1.1−29.el9.x86_64.rpm

An RPM is composed of a name, version, and release (which contains a disttag), the NVR. It is unique in Koji. In the example above, the RPM name is myrpm, version 1.1,
release 29.el9, distag el9, and arch x86_64.
An RPM is built with rpmbuild command and to generate a clean buildroot, Koji uses Mock. To release a new RPM, the release number associated with it needs to be increased in
the .spec file.

In Koji web, there is a Hosts tab, where information about the Koji builders can be found.

- Enabled and Ready concept: A node can accept Koji requests only if it is enabled. A node can be enabled but not ready, and sometimes it means that the kojid process is not running, or the host is overloaded. The host can be enabled or disabled via Koji web button. 

- Tasks have different weights, a decimal number represents them. The capacity is the total weight of the tasks per node, and the load is the total weight of all the tasks running at the moment.

## External Repos 

### List external repos

`$ koji list-external-repos`

### Add an external repo to a tag

1) `$ koji add-external-repo <external_repo_name> --url <external_repo_url>`

2) `$ koji add-external-repo -t <tag>-build <external_repo_name>`

### Fix external repos

The `regen-repo` is for the `-build` tag which is what koji uses for the build root when building rpms.

1) Edit the external repo: `koji edit-external-repo --url <external_repo_url> <external_repo_name>`

2) `koji regen-repo $tag-build`

## Useful Koji commands

### Koji list permissions
`koji list-permissions --user <username>`

### Enable a module in a koji tag

`$koji edit-tag $TAG-build -x mock.module_setup_commands='[["enable", "subversion-devel"]]'`

Or remove a module:

`koji edit-tag --remove-extra mock.module_setup_commands $TAG-build`

### Get Koji tags with permissions

`for i in $(koji list-tags); do grep -q 'Required permission' <(koji taginfo $i) && echo $i; done`


## How to contribute to koji upstream

Documentation: https://docs.pagure.org/pagure/usage/first_steps.html
	
1) Add the pub key in your machine - https://pagure.io/settings#nav-ssh-tab
	
2) Fork the project - https://docs.pagure.org/pagure/usage/forks.html 
	
3) Clone it - `$ git clone ssh://git@pagure.io/forks/<username>/koji.git`

4) Have fun!

### Sync your fork:

Documentation: https://stackoverflow.com/questions/7244321/how-do-i-update-or-sync-a-forked-repository-on-github

1) `git remote add -f master https://pagure.io/koji.git`

2) `git branch -va`

3) `git checkout master`

4) `git merge remotes/master/master`

5) `git add .`

6) `git push origin master`

## How to install koji via puppet:

Example: https://gitlab.cern.ch/ai/it-puppet-module-bagplus/-/blob/qa/code/manifests/features/linuxsupport.pp?ref_type=heads

```
osrepos::kojitag{ 'linuxsupport':
  priority => 5,
}
-> ensure_packages(['koji'])
```

## ACL koji permissions

You can get requests from users asking to have permissions to build to a specific tag, then you need:

1) Add the tag to a koji admin egroup in [lsb hg](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsb/-/blob/qa/data/hostgroup/lsb.yaml?ref_type=heads#L151).

That is enough! But if the user is in a hurry:

2) Run `prod_koji_synchronize_users` nomad job to sync this change.

[!NOTE]
You won't get an email for the change.

3) Check if the permission exist:

`koji list-permissions --user <username>`

The change in this scenario will just be on the koji hubs which will update the policy based on the same hieradata. Puppet needs to run on the hub nodes.

4) Run puppet on koji10/koji11

