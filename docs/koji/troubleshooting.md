
# Debug Koji

The koji logs from web, hub and builder servers can be found in <https://os-lsb.cern.ch/>.

# Creating an issue in Koji upstream

Submit questions to Koji upstream in https://pagure.io/koji/new_issue or send an email to  buildsys@lists.fedoraproject.org .

# Troubleshooting
## Troubleshooting Koji Image builds

You can connect to a running QEMU process while an image is being built in order to debug it.

1. Go to Koji and look for the relevant `createImage` task. Find what builder it's running in.
2. SSH as root into that host and run `ps axf | grep qemu`:

```bash
[koji33] ~ > ps axf | grep qemu
3150593 pts/1    S+     0:00        /usr/libexec/qemu-kvm -name factory-build-ef31730c-269c-448b-8358-09ca4bd6433a -S -machine pc-i440fx-rhel7.0.0,accel=kvm,usb=off,dump-guest-core=off -m 4096 -realtime mlock=off -smp 2,sockets=2,cores=1,threads=1 -uuid edcff249-042a-4902-894a-444fdeaf3aa9 -no-user-config -nodefaults -chardev socket,id=charmonitor,path=/var/lib/libvirt/qemu/domain-87-factory-build-ef3173/monitor.sock,server,nowait -mon chardev=charmonitor,id=monitor,mode=control -rtc base=utc -no-reboot -boot strict=on -kernel /var/builder/builder/tasks/7815/1697815/scratch_images/factory-build-ef31730c-269c-448b-8358-09ca4bd6433a-kernel -initrd /var/builder/builder/tasks/7815/1697815/scratch_images/factory-build-ef31730c-269c-448b-8358-09ca4bd6433a-ramdisk -append method=http://linuxsoft.cern.ch/cern/centos/7.7/os/x86_64/ ks=file:/ks.cfg -device piix3-usb-uhci,id=usb,bus=pci.0,addr=0x1.0x2 -drive file=/var/builder/builder/tasks/7815/1697815/output_image/ef31730c-269c-448b-8358-09ca4bd6433a.body,format=raw,if=none,id=drive-virtio-disk0 -device virtio-blk-pci,scsi=off,bus=pci.0,addr=0x4,drive=drive-virtio-disk0,id=virtio-disk0,bootindex=1 -netdev tap,fd=28,id=hostnet0,vhost=on,vhostfd=30 -device virtio-net-pci,netdev=hostnet0,id=net0,mac=52:54:00:dd:d4:1b,bus=pci.0,addr=0x3 -chardev pty,id=charserial0 -device isa-serial,chardev=charserial0,id=serial0 -chardev socket,id=charserial1,host=127.0.0.1,port=21581,server,nowait -device isa-serial,chardev=charserial1,id=serial1 -vnc 127.0.0.1:0 -vga cirrus -device virtio-balloon-pci,id=balloon0,bus=pci.0,addr=0x5 -msg timestamp=on
```

Look for the `-vnc 127.0.0.1:0` part of the command, to find the display number, in this case `0`. Pro-tip: it will usually be 0 unless there's another qemu process running on the same host, so you can usually assume it's 0 and skip straight to step 3.

3. From aiadm, run a vnc viewer (such as TigerVNC Viewer) and connect to the display number on the koji builder: `vncviewer -via root@koji33 :0`. You can navigate to different screens (anaconda, shell, logs, etc.) with ALT+arrow keys.

## Restart misbehaving Koji builder

* Go to the `Hosts` tab
* Select the host you want to reboot and disable it
* Wait for its current tasks to finish
* Reboot the node

## Testing koji using a non admin user

We created the service account `kojiuser` to enable the linux team to do tests as a non koji user admin.

For instance, this is useful when it is needed to test the output of a koji command that was restricted only to admin users.

```
[kojiuser@mvf-alma9 ~]$ koji -p kojitest list-users --perm=admin
Usage: koji list-users [options]
(Specify the --help global option for a list of other help options)

koji: error: This action requires admin privileges

```

The password of the account is available in `tbag show kojiuser --hg lsb`.