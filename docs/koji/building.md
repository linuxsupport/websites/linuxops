# Building RPMs in Koji

## Overview / Quick start

* Koji is accessible via [https://koji.cern.ch](https://koji.cern.ch)
* The koji client is available via the koji package `yum install koji`
* Before using koji, you require an account in koji - please request an account through Linuxsupport SNOW [here](https://cern.service-now.com/service-portal?id=sc_cat_item&name=Get-help-on-Linux-device&se=linux-desktop)
* If required, configure your koji client as per these [instructions](https://cern.service-now.com/service-portal?id=kb_article&n=KB0005632). Alternatively you may use lxplus or aiadm
* To confirm that you account is ready you may run `koji list-permissions --mine`. Providing everything is configured correctly you should have 'build' permissions
* If you plan to use puppet: read the "Repositories" and "Workflow" section on this page

## Repositories

* All tags in koji have a corresponding repository on http://linuxsoft.cern.ch/internal/repos
* These repositories are synced every 10 mins.
* We do not provide .repo files automatically, an example repo file can be seen here if you follow the [AI workflow](http://linuxsoft.cern.ch/internal/repos/ai6.repo)
* If your machine is puppet managed, consider investigating the 'osrepo' module

## RPM 101

* A rpm is composed of a NAME, VERSION, RELEASE(which contains a DISTTAG)

        ksh-20120801-10.el6_5.4
        NAME=ksh
        VERSION=20120801
        RELEASE=10.el6_5.4
        DISTTAG=el6_5


* A rpm is built with `rpmbuild -ba`. If it doesn't work on your machine it will not work in Koji. However if it works on your machine it may not work on Koji (dependencies you have installed but not documented in the spec file, afs dependency, tag not defined, etc..)
* To generate a clean buildroot koji uses [mock](https://github.com/rpm-software-management/mock/wiki)
* A rpm N V R is unique in koji.
* When you release a new RPM, bump the release number and associate it with a Changelog entry in the .spec file.

## Workflow

As agreed with the AI team, the default workflow is the following:

![workflow](workflow.png)

* We consider your machine runs puppet and uses the provided repositories definition
* The tag used in the example is ai7 but it can be yours.
* **IMPORTANT** If you request a new tag it is your responsibility to use the best practice of deploying your repo file via the ai os-repos puppet module

We will now see how to execute step 1, 3 and 5 from the image above

### Build from a src.rpm

* NOTE: All the build operations can be executed with `--scratch` to test if your package will build correctly, however please be aware that koji is not designed to be CI server
* First add packages to your tag, as an example we are going to use the `ai7` tag
```
koji add-pkg --owner=ai-team ai7-testing mypkg
koji add-pkg --owner=ai-team ai7-qa mypkg
koji add-pkg --owner=ai-team ai7-stable mypkg
```
* This operation is only needed once, when you add a new package
* **Note:** There is no notion of a group in koji, only users. `--owner` can only be set to a valid user name. If you would like to generate builds using a 'team' username, please contact LinuxSupport to create the user for you
* Build you package `koji build ai7 mypkg-1.2-4.el7.src.rpm`
* Your package will build and providing it was successful the rpm will be available in the ai7-**testing** repository

### Build from gitlab.cern.ch

* An example to build via gitlab is `koji build ai7 git+ssh://git@gitlab.cern.ch:7999/ai-config-team/ai-tools.git#8.12-1`
* This command will build from the `ai-config-team/ai-tools repo` from the `8.12-1` branch

* Notes:
 * Your Gitlab project must be configured with visibility set to `Internal` (i.e. all authenticated CERN users can access the project) or `Public` (i.e. all Internet users can access the project without authentication). See Gitlab documentation for more details on visibility levels
 * Alternatively, you may use a project with visibility set to `Private` and explicitly grant access to user `koji support`, but take into account that the source RPM built by Koji will be open to all CERN users anyway

### Promoting a package

* Tag your build to -qa `koji tag-build ai7-qa mypkg-1.2-4.el7`
* Create a CRM ticket following this template with `Proposed date for production` set in a weeks time:

![promotion](promotion.png)

* Your package will be available in -qa in maximum 15 minutes. (a cron job runs every 5 minutes to fetch new packages)
* Wait a week and check if your ticket is not a blocker for anyone else
* Tag it to -stable `koji tag-build ai7-stable mypkg-1.2-4.el7`
* Your package will be available in -stable in maximum 15 minutes. (a cron job runs every 5 minutes to fetch new packages)

## Requesting a new tag

* Complete this [SNOW form](https://cern.service-now.com/service-portal?id=service_element&name=linuxbuild)
* All tag names should be short and will have the distribution major release number in it's final name (8el, 9el, 8al, 9al etc..)
* Please do not forget to fill the Admin-Egroup. Only accounts belonging to that egroup have permissions to build to that koji tag.


# Building RPMs in kojitest environment
You'll have to build a src.rpm manually:

1) Go to a node with the OS that you want to build to, for instance if you want a al9, go to a alma9 node.

2) Install `make` and `rpm-build`

3) Git clone the repo and then run make srpm

4) Check that koji.conf has `kojitest` well defined. If not, take that info from aiadm or lxplus.

5) Run koji build using user/password:

`koji -p kojitest --user koji --authtype password --password `cat /etc/auth/koji_password` build --wait <tag> build/SRPMS/<src.rpm>`

or 

6) Run koji build using kerberos:

`kinit kojici` using the password from `tbag show --hg lsb kojici_password`

`koji -p kojitest build --wait <tag> build/SRPMS/<src.rpm>`

