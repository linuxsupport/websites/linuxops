# Deleting a tag in koji

Eventually, SNow tickets will come our way regarding the need of deleting an obsolete tag in koji (e.g. [RQF1766211](https://cern.service-now.com/service-portal?id=ticket&table=u_request_fulfillment&n=RQF1766211).

To accomplish that mission, start by cloning the [lxdist-build](https://gitlab.cern.ch/linuxsupport/lxdist-build) repo.

## Remove the tag from the repository
In the `bin/tags` directory, find the file which documents creating the tag. Remove that file with git rm <filename> and commit that change with reference to the SNOW ticket asking for the removal.
Push and create a merge request for the team to review

## Update the checkout on lxsoftadm01

Please update the clone of this repo on lxsoftadm01:

```bash
~ > ssh build@lxsoftadm01
[build@lxsoftadm01] cd bin
[build@lxsoftadm01] git pull
```
## Untag packages
If there are any packages using these tags, it is better to untag them before removing the tag from KOJI. To get the full list of tags use:
```bash
koji list-tags $DIST-*
```
where $DIST is to be replaced by the tag pattern to be removed.
For each of these tags, check if there are any corresponding builds, eg
```bash
koji list-tagged $DIST-testing
```
Then untag each of the listed packages before proceeding with eg
```bash
koji untag $DIST-testing <package>
```


## Remove the tag from koji
From lxsoftadm01 remove all related tags from koji, eg
```bash
koji remove-tag $DIST-testing
koji remove-tag $DIST-build
koji remove-tag $DIST-qa
koji remove-tag $DIST-stable

```
where again ```$DIST``` is to be replaced by the tag to be removed.


