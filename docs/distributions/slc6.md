# Scientific Linux CERN 6

!!! danger
    SLC6 is deprecated and you should not run these commands any longer

## Practical info

* Upstream support : EOL-ed on 2020-11-30
* As of 12/2020 CERN OpenStack images for SLC6 have been marked as 'community'. This means they are hidden from the list of images available, however it's still possible to spawn a SLC6 VM if the user knows the UUID of the image. This allows us to essentially deprecate the distribution, however still provide it in 'emergency' situations. An example of how to spawn a SLC6 VM is as follows:
    * `ai-bs -i febdeea7-99d4-46a2-ab06-e9155bbc9e7f` # SLC6 - i686 [2018-08-20]
    * `ai-bs -i 6058e02c-2577-417b-b8b8-478fee326f56` # SLC6 - x86_64 [2018-08-20]



## Building updates

These directories allow an administrator to rebuild packages provided by Red Hat and new/updated CERN packages.

| Incoming |
| -- |
| /mnt/data2/home/build/packages/incoming/slc6/updates |
| /mnt/data2/home/build/packages/incoming/slc6/extras |
| /mnt/data2/home/build/packages/incoming/slc6/cernonly	|
| /mnt/data2/home/build/packages/incoming/slc6/mrg |

!!! Note
    Source or binary packages can be added to incoming.

Build:

```
/mnt/data2/home/build/bin/bsbuild -d "slc6" -r "updates extras cernonly mrg"
/mnt/data2/home/build/bin/bsdownload -s -d "slc6" -r "updates extras cernonly mrg"
```

!!! Note "Signing packages"
    Retrieve the signing passphrase from teigi:

    ```
    tbag show distribution_signing_key_passphrase --hg lxsoft/adm
    ```

!!! note ""
    From time to time we might receive [requests](https://cern.service-now.com/service-portal/view-request.do?n=RQF1573433) asking for proprietary software to be added to `cernonly` repo. To do so you will still need to run `/mnt/data2/home/build/bin/bsdownload -s -d "slc6" -r "updates extras cernonly mrg"` to sign the packages.

If a new kernel package is available (e.g `kernel-2.6.32-754.24.2.el6`), please build the associated modules **as in this example, replacing with your concerning version**:

```
bsbuildmodules -d slc6 -k 2.6.32-754.24.2.el6  # build with mock new modules
bscopymodules -d slc6 -k 2.6.32-754.24.2.el6  # copy modules in the build area
```

!!! Note ""
    You then need to follow the [Release to testing](#release-to-testing) procedure.

## Release to testing

!!! Note ""
    On Thursday morning please [release to production](#Release-to-production), then release to testing

If any packages were built in the above step, move the built packages to testing:
```
/mnt/data2/home/build/bin/bsbuild2test -d slc6 -r "testing"
```

Regenerate testing repositories metadata:
```
/mnt/data2/home/build/bin/bsregenrepos -d slc6 -r "testing mrg-testing"
```

Sync /mnt/data2 -> /mnt/data1:

```
/mnt/data2/home/build/bin/bssync -t "data1 eos" -d slc6 -r "testing mrg-testing"
```

Test the upgrade works as expected on `lxvmslc6t-amd64` and `lxvmslc6t-i386`:
```
yum clean all && yum update --enablerepo=slc6-testing
```

Email `linux-announce-test@cern.ch` with updated packages information. This will as well add the updates to <https://linux.web.cern.ch/updates/slc6/test/latest_updates/>:
```
/mnt/data2/home/build/bin/bsmailusers -d slc6 -r testing -e -s
```

!!! Note ""
    `$EDITOR` must be set to your preferred editor to edit the email

## Release to production

Generate list of repos that have been updated in the last week
```
/mnt/data2/home/build/bin/generaterepolist -d slc6
```
List available updates:
```
/mnt/data2/home/build/bin/bstest2prod -d slc6 -r "$REPOLIST"
```

Promote package. `\*` can be used to promote all updates. A careful review is needed in general:
```
/mnt/data2/home/build/bin/bstest2prod -d slc6 -r "$REPOLIST" [pkgname to release]
```
Note - running this command will move files from testing to what will be production, for this reason we should bsregenrepos on both testing and non-testing repositories

Generate list of repos that have been updated in the last week + including testing repositories

```
/mnt/data2/home/build/bin/generaterepolist -d slc6 -t
```

Regenerate production repositories metadata (=~ 30min):
```
/mnt/data2/home/build/bin/bsregenrepos -d slc6 -r "$REPOLIST"
```

!!! danger "Sync /mnt/data2 -> /mnt/data1:"
    Running `bssync` will actually "de facto" move all packages to production. Reversing this action is not trivial:

    ```
    /mnt/data2/home/build/bin/bssync -t "data1 eos" -d slc6 -r "$REPOLIST"
    ```

Test the upgrade works as expected on `lxvmslc6p-amd64` and `lxvmslc6p-i386`
```
yum clean all && yum update
```

## Update web pages & send email

The below process should be followed:

run

```
/mnt/data2/home/build/bin/bsmailusers -d slc6 -r updates -e -s
```

Confirm 'yes' to send to send the mail. This will also [push an update automatically](https://gitlab.cern.ch/linuxsupport/websites/linux.cern.ch) that will end up showing on on <https://linux.web.cern.ch/updates/slc6/prod/latest_updates/>
