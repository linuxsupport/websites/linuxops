# OpenStack images

## Introduction

Our group is responsible for building and testing new OpenStack glance images for the distributions that we support.

We have a scripted process, utilizing Koji and kickstart files which can be found here: [https://gitlab.cern.ch/linuxsupport/koji-image-build](https://gitlab.cern.ch/linuxsupport/koji-image-build).

This repository takes care of running scheduled pipelines once a month to rebuild our cloud images. It has tests in place which can be also skipped if needed.

## Testing

Testing is carried out by a [separate pipeline](https://gitlab.cern.ch/linuxsupport/testing/image-ci) which is
triggered automatically when images are rebuilt. It can also be run on demand to test the image or the Puppet
and Cloud infrastructure.

To make it easier for you, you can use this form to pre-fill the CI pipeline variables. Select the options
you want, click the button at the bottom and then click "Run pipeline" at the bottom of the Gitlab page
that will open in a new tab.

<!--
  Most of the magic happens in docs/javascripts/testing_form.js
  If you make changes here, you'll have to make changes there too.
-->
<form name="image_ci" class="testing">
  Select at least one option from each of the following categories:
  <div class="row">
    <fieldset>
      <legend>Operating System</legend>
      <div>
        <input type="checkbox" id="TEST_OS8al" name="TEST_OS8al">
        <label for="TEST_OS8al">AlmaLinux 8</label>
      </div>
      <div>
        <input type="checkbox" id="TEST_OS9al" name="TEST_OS9al">
        <label for="TEST_OS9al">AlmaLinux 9</label>
      </div>
      <div>
        <input type="checkbox" id="TEST_OSRH8" name="TEST_OSRH8">
        <label for="TEST_OSRH8">RHEL 8</label>
      </div>
      <div>
        <input type="checkbox" id="TEST_OSRH9" name="TEST_OSRH9">
        <label for="TEST_OSRH9">RHEL 9</label>
      </div>
    </fieldset>

    <fieldset>
      <legend>Instance Type</legend>
      <div>
        <input type="checkbox" id="TEST_VIRTUAL" name="TEST_VIRTUAL">
        <label for="TEST_VIRTUAL">Virtual</label>
      </div>
      <div>
        <input type="checkbox" id="TEST_PHYSICAL" name="TEST_PHYSICAL">
        <label for="TEST_PHYSICAL">Physical</label>
      </div>
    </fieldset>

    <fieldset>
      <legend>Configuration</legend>
      <div>
        <input type="checkbox" id="TEST_UNMANAGED" name="TEST_UNMANAGED">
        <label for="TEST_UNMANAGED">Unmanaged</label>
      </div>
      <div>
        <input type="checkbox" id="TEST_PUPPET" name="TEST_PUPPET">
        <label for="TEST_PUPPET">Puppet-managed</label>
      </div>
    </fieldset>

    <fieldset>
      <legend>Architecture</legend>
      <div>
        <input type="checkbox" id="TEST_X86_64" name="TEST_X86_64">
        <label for="TEST_X86_64">x86_64</label>
      </div>
      <div>
        <input type="checkbox" id="TEST_AARCH64" name="TEST_AARCH64">
        <label for="TEST_AARCH64">aarch64</label>
      </div>
    </fieldset>
  </div>

  <fieldset>
    <legend>Advanced Settings</legend>
    <div>
      <label for="IMAGE">Image UUID <span class="help">(optional, if not set the latest image is tested)</span>:</label>
      <input type="text" id="IMAGE" name="IMAGE" size="36" pattern="^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$">
    </div>

    <div>
      <label for="FLAVOR">Physical machine flavor <span class="help">(optional, if not set all flavors available in the project <code>IT Linux Support - CI Physical</code> are tested)</span>:</label>
      <input type="text" id="FLAVOR" name="FLAVOR" size="36">
    </div>

    <div>
      <input type="checkbox" id="DELETE_FAILURES" name="DELETE_FAILURES" checked>
      <label for="DELETE_FAILURES">Delete machines even if tests failed</label>
    </div>

  </fieldset>

  <center>
    <button type="button" name="run" onclick="runImageTests()" disabled>Run 0 tests</button>
  </center>
</form>
