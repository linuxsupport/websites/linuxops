# Docker images

## Introduction

We currently build and maintain docker images for supported distributions.

| Distribution | Gitlab repo | Dockerhub URL |
|--------------|-------------|---------------|
| cc7-base   | [https://gitlab.cern.ch/linuxsupport/cc7-base]   | [https://hub.docker.com/r/cern/cc7-base]   |
| cs8-base   | [https://gitlab.cern.ch/linuxsupport/cs8-base]   | [https://hub.docker.com/r/cern/cs8-base]   |
| alma8-base | [https://gitlab.cern.ch/linuxsupport/alma8-base] | [https://hub.docker.com/r/cern/alma8-base] |
| alma9-base | [https://gitlab.cern.ch/linuxsupport/alma9-base] | [https://hub.docker.com/r/cern/alma9-base] |


To access [hub.docker.com](https://hub.docker.com/u/cern) we have a service account, that it's only used by the projects that create the docker images. The service account can be found [here](https://gitlab.cern.ch/groups/linuxsupport/-/settings/ci_cd).

The docker images are pushed to our registry and then also to dockerhub, but we only ever use our own copy. The dockerhub one is for externals.

## Building a new image

New images are built and tested by the CI processes inside the corresponding Gitlab repos. This CI is triggered automatically on the 1st of each month. The last step of the CI pipeline, which tags images as latest, is manual.

Below you can find the old manual procedure, which is essentially what the CI is doing.

### Manual procedure (for reference)

1. Build an image via koji, using the image-build command from the README.md file of the gitlab $distribution repository, this will build a .tar.xz file
2. Download the .tar.xz file from koji and import into your local docker installation `docker load < $filename.tar.xz`
3. Test the image `docker run -it $imagename /bin/bash`
4. Providing testing is okay, update the 'latest image' list in README.md of the gitlab $distribution repo

#### Uploading the new image to the CERN gitlab registry

1. Push the image to gitlab
    1. `docker login gitlab-registry.cern.ch`
    2. `docker tag $imageid gitlab-registry.cern.ch/linuxsupport/$distribution:$date_YYYYMMDD`
    3. `docker tag $imageid gitlab-registry.cern.ch/linuxsupport/$distribution:latest`
    4. `docker push gitlab-registry.cern.ch/linuxsupport/$distribution`

#### Uploading the new image to the hub.docker.com registry

1. Push the image to dockerhub
    1. `docker login --username=$username`
    2. `docker tag $imageid cern/$distribution:$date_YYYYMMDD`
    3. `docker push cern/$distribution`
2. Update the description of $distribution on dockerhub, if necessary

Note - we do not by default tag each new image as 'latest' on hub.docker.com, instead we wait for a period of time whilst the image is tagged as 'latest' in the internal gitlab registry to ensure that there are no regressions. This period of time is typically 4 weeks. After this time has past, we can safely tag the last image uploaded on hub.docker.com as 'latest'.
