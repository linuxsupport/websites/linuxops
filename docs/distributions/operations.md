# Operational procedures

Please add here detailed procedures that might be reusable from time to time, so you do not have to figure it out again.

## Import binaries / Add already built RPM

* Build your package for both architecture + debuginfo

* Copy all the RPMS/SRPMS to a directory

* If the build already exists (say you sent it and you can't build it in koji).

```bash
koji call resetBuild devtoolset-2-dyninst-8.0-5dw.el5
``` 

* Create an empty build:

```bash
koji call --python createEmptyBuild "'devtoolset-2-dyninst'" "'8.0'" "'5dw.el5'" None
koji call --python createEmptyBuild "'QConvergeConsoleCLI'" "'2.3.00'" "'11'" None
```

* Import with koji all RPMs at once from your directory, or any RPM you have downloaded and want to add:

```bash
koji import *
```    

* Tag your package or **tell the user to tag it themselves**, according your needs. You might need to add the package to the tag first.

```bash
koji add-pkg mytag QConvergeConsoleCLI
koji tag-build mytag devtoolset-2-dyninst-8.0-5dw.el5
```

## Preparing AIMS PXE targets for HP models with non-working NICs on CC7

This information comes from [LOS-659](https://its.cern.ch/jira/browse/LOS-659)

Ref. [KB0007418: Installing CERN CentOS 7 on HP 800 G6 and G8 models](https://cern.service-now.com/service-portal?id=kb_article&n=KB0007418)

This is the complete procedure after successful testing in HP 800 G6 *and* G8 models.

1. Take a spare physical machine that does not have the mentioned issue. It may be possible to use an Openstack VM, but https://clouddocs.web.cern.ch/details/user_supplied_images.html#boot-from-iso upstream docs do not help here.
2. Perform a minimal installation either through PXE network installation or with a USB key. Ref. https://linux.web.cern.ch/centos7/docs/install/
    1. Take into account that the used machines will need GPN connectivity to install extra packages. Otherwise you will need to transfer all dependencies locally to your machine, which you do not want to do
    2. Once installed, do not allow any updates on the given machine so you keep the default installation kernel (For CC 7.9 : kernel-3.10.0-1160.el7)
3. Install the kmod that adds support for the problematic NIC:

```
yum install https://elrepo.org/linux/elrepo/el7/x86_64/RPMS/kmod-e1000e-3.8.4-3.el7_9.elrepo.x86_64.rpm
```

4. Add some missing packages you will need afterwards to regenerate initramfs with dracut:

```
yum install anaconda-dracut dracut-tools dracut-fips dracut-fips-aesni dracut-caps dracut-config-generic clevis-dracut dracut-config-rescue redhat-upgrade-dracut-plymouth
```

5. Modify dracut default settings and run it :

```
echo 'hostonly="no"' >> /etc/dracut.conf
```

    1. Reference:

```
hostonly="{yes|no}"
           Host-Only mode: Install only what is needed for booting the
           local host instead of a generic host and generate
           host-specific configuration.
```

6. Check which dracut parameters we need to add by inspecting how the default ISO file was done:
    1. Download it

```
curl -O http://linuxsoft.cern.ch/cern/centos/7/os/x86_64/images/boot.iso
```

    1. After mounting it somewhere, inspect the initrd arguments:

```
lsinitrd /mount/ISO/initrd.img | grep Arguments
```

7. Configure dracut to regenerate initramfs with all the required parameters you just got

```
echo $(date) >> .buildstamp
dracut -f --nomdadmconf --nolvmconf --xz --install '/.buildstamp' --no-early-microcode --add 'fips' --add 
'anaconda pollcdrom' --force /tmp/initrd-new.img $(uname -r) 
```

8. Take this initrd.img file somewhere else and upload it to AIMS using the default CC7 `vmlinuz`

```
[root@aims01 ~]# aims2client addimg --name CC79_X86_64_E1000E --description "CERN CENTOS 7 X86_64 with kmod-e1000e-3.8.4-3.el7_9.elrepo updates for problematic NICs" --arch x86_64 --uefi --vmlinuz /aims_share/tftpboot/aims/boot/CC7_X86_64/vmlinuz --initrd /tmp/initrd-new.img
```

9. You can already install a machine with this, but once rebooted you will not have the driver installed, so you have to force its installation, either with a kickstart file or with a DUD, which we can configure as in this menu entry for UEFI machines:

```
    menuentry 'Install CERN CentOS 7.9 with kmod-e1000e elrepo updates' {
        insmod progress
        clear
        echo -n 'loading kernel ...'
        linuxefi (http)/aims/boot/CC79_X86_64_E1000E/vmlinuz ip=dhcp inst.repo=http://linuxsoft.cern.ch/cern/centos/7/os/x86_64/ dd=http://linuxsoft.cern.ch/elrepo/dud/el7/x86_64/dd-e1000e-3.8.4-3.el7_9.elrepo.iso
        clear
        echo -n 'loading initrd ...'
        initrdefi (http)/aims/boot/CC79_X86_64_E1000E/initrd
    }
```

Warning! As with any other UEFI installation, CentOS will be the first boot option after the installation, so in order to reinstall from PXE you will need to either enter the boot menu or use a kickstart that restores the bootorder, just like with [KB0006881](https://cern.service-now.com/service-portal?id=kb_article&n=KB0006881)


# Upgrading the minor version

When a minor version is release, if you are using the default repository <http://linuxsoft.cern.ch/cern/rhel/<8/9>/baseos/x86_64/os>, no manual action are needed. The upgrade is done by the usual distro-sync. In any case, is always good to reboot the machine to upgrade the kernel.
The `/8/` or `/9/` moves every week and, for instance, `/8.8/` is static.
Lock in a specific version, for instance `8.8`, means that you will not get security updates.
