# Debian

We provide limited Debian support as part of the [ATS-IT agreement for Debian support for CERN Accelerator Front-end Systems](https://cernbox.cern.ch/pdf-viewer/eos/project/a/ats-it-tc/ATS-IT%20Projects/Linux%20Future%20-%20Debian%20Support%20for%20Accelerator%20Front-end%20Platform/LINUX%20FUTURE%20-%20Debian%20for%20FECs.pdf?contextRouteName=files-spaces-generic&contextRouteParams.driveAliasAndItem=eos/project/a/ats-it-tc/ATS-IT%20Projects/Linux%20Future%20-%20Debian%20Support%20for%20Accelerator%20Front-end%20Platform).

This support is limited to:

* [mirroring of upstream Debian repositories](https://gitlab.cern.ch/linuxsupport/cronjobs/rsync/-/blob/674e6a91af3249e4de01d13c14e79d1071b7001b/prod.repos.yaml#L149-173)
* [snapshotting of Bookworm mirrors](https://gitlab.cern.ch/linuxsupport/cronjobs/yumsnapshot/-/blob/78464203305e8fa992ef2a0d5166a5ad3741acc0/yumsnapshot/runyumsnapshot.sh#L448-459)
* building two packages:
    * [cern-ca-certs](https://gitlab.cern.ch/linuxsupport/rpms/cern-ca-certs/-/tree/master/debian)
    * [cern-get-keytab](https://gitlab.cern.ch/linuxsupport/rpms/cern-get-keytab/-/tree/master/debian)
* publishing a [repo](https://linuxsoft.cern.ch/cern/debian/minimal/) with those packages

## Workflow

As Debian currently has minimal support, most tasks aren't automated and have been done largly by hand.
The exception to this is the package builds themselves, for which we have some automation integrated in [rpmci](https://gitlab.cern.ch/linuxsupport/rpmci/-/blob/master/debian.yml), for lack of a better place.

For the rest, Alex has a hand-installed Debian VM called `ai-deb02` where he runs the rest of the stuff.
What follows is largely a collection of bash history snippets for reference.

### GPG Key for Debian

In order to sign the packages, we created a new GPG key, as follows:

```bash
[aiadm61] ~ > gpg --full-gen-key
gpg (GnuPG) 2.3.3; Copyright (C) 2021 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Please select what kind of key you want:
   (1) RSA and RSA (default)
   (2) DSA and Elgamal
   (3) DSA (sign only)
   (4) RSA (sign only)
  (14) Existing key from card
Your selection? 1
RSA keys may be between 1024 and 4096 bits long.
What keysize do you want? (3072)
Requested keysize is 3072 bits
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0)
Key does not expire at all
Is this correct? (y/N) y

GnuPG needs to construct a user ID to identify your key.

Real name: Linux Platform Engineering Team
Email address: Linux.Support@cern.ch
Comment:
You selected this USER-ID:
    "Linux Platform Engineering Team <Linux.Support@cern.ch>"

Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? C
Comment: Debian signing key
You selected this USER-ID:
    "Linux Platform Engineering Team (Debian signing key) <Linux.Support@cern.ch>"

Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? O
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
gpg: key 15FB0B8BC542A5D7 marked as ultimately trusted
gpg: revocation certificate stored as '/afs/cern.ch/user/a/airibarr/.gnupg/openpgp-revocs.d/42421B452F2EB5199E0EB16F15FB0B8BC542A5D7.rev'
public and secret key created and signed.

pub   rsa3072 2024-10-18 [SC]
      42421B452F2EB5199E0EB16F15FB0B8BC542A5D7
uid                      Linux Platform Engineering Team (Debian signing key) <Linux.Support@cern.ch>
sub   rsa3072 2024-10-18 [E]
[aiadm61] ~ > gpg --export --armor --output debian.pub 42421B452F2EB5199E0EB16F15FB0B8BC542A5D7
[aiadm61] ~ > gpg --export-secret-key --armor --output debian.priv 42421B452F2EB5199E0EB16F15FB0B8BC542A5D7
[aiadm61] ~ > tbag set --hg lsb debian_gpg_privatekey --file debian.priv
[aiadm61] ~ > tbag set --hg lsb debian_gpg_publickey --file debian.pub
[aiadm61] ~ > tbag set --hg lsb debian_gpg_password
Enter Secret: <gpg key passphrase>
[aiadm61] ~ > rm debian.{priv,pub}
```

The key has to be imported into `ai-deb02`:

```bash
root@ai-deb02:~# gpg --import debian.priv
gpg: key 15FB0B8BC542A5D7: "Linux Platform Engineering Team (Debian signing key) <Linux.Support@cern.ch>" not changed
gpg: key 15FB0B8BC542A5D7: secret key imported
gpg: Total number processed: 1
gpg:              unchanged: 1
gpg:       secret keys read: 1
gpg:   secret keys imported: 1
```

### Initial repository creation

Using aptly, and then rsyncing the files to linuxsoft:

```bash
root@ai-deb02:~# aptly repo create -distribution bookworm -component main minimal

Local repo [minimal] successfully added.
You can run 'aptly repo add minimal ...' to add packages to repository.
root@ai-deb02:~# aptly repo add minimal ~/build_packages/cern-ca-certs/output/*
Loading packages...
[!] Unknown file extension: /root/build_packages/cern-ca-certs/output/cern-ca-certs_20230604.tar.xz
[!] Unknown file extension: /root/build_packages/cern-ca-certs/output/cern-ca-certs_20230604_amd64.changes
[!] Unknown file extension: /root/build_packages/cern-ca-certs/output/cern-ca-certs_20230604_arm64.changes
[+] cern-ca-certs_20230604_source added
[+] cern-ca-certs_20230604_amd64 added
[+] cern-ca-certs_20230604_arm64 added
[!] Some files were skipped due to errors:
  /root/build_packages/cern-ca-certs/output/cern-ca-certs_20230604.tar.xz
  /root/build_packages/cern-ca-certs/output/cern-ca-certs_20230604_amd64.changes
  /root/build_packages/cern-ca-certs/output/cern-ca-certs_20230604_arm64.changes
ERROR: some files failed to be added
root@ai-deb02:~# aptly repo show -with-packages minimal
Name: minimal
Comment:
Default Distribution: bookworm
Default Component: main
Number of packages: 3
Packages:
  cern-ca-certs_20230604_amd64
  cern-ca-certs_20230604_arm64
  cern-ca-certs_20230604_source
root@ai-deb02:~# aptly snapshot create minimal-20241021 from repo minimal

Snapshot minimal-20241021 successfully created.
root@ai-deb02:~# aptly publish snapshot -batch -passphrase="<gpg key passphrase>" minimal-20241021
Loading packages...
Generating metadata files and linking package files...
Finalizing metadata files...
Signing file 'Release' with gpg, please enter your passphrase when prompted:
Clearsigning file 'Release' with gpg, please enter your passphrase when prompted:

Snapshot minimal-20241021 has been successfully published.
Please setup your webserver to serve directory '/root/.aptly/public' with autoindexing.
Now you can add following line to apt sources:
  deb http://your-server/ bookworm main
  deb-src http://your-server/ bookworm main
Don't forget to add your GPG key to apt with apt-key.

You can also use `aptly serve` to publish your repositories over HTTP quickly.
```

Rsync the results to linuxsoft:

```bash
root@ai-deb02:~# kinit airibarr
airibarr@CERN.CH's Password:
root@ai-deb02:~# rsync -avz ~/.aptly/public/ root@lxsoftadm01.cern.ch:/mnt/data1/dist/cern/debian/minimal/
sending incremental file list
./
dists/
dists/bookworm/
dists/bookworm/Contents-amd64.gz
dists/bookworm/Contents-arm64.gz
dists/bookworm/InRelease
dists/bookworm/Release
dists/bookworm/Release.gpg
dists/bookworm/main/
dists/bookworm/main/Contents-amd64.gz
dists/bookworm/main/Contents-arm64.gz
dists/bookworm/main/binary-amd64/
dists/bookworm/main/binary-amd64/Packages
dists/bookworm/main/binary-amd64/Packages.bz2
dists/bookworm/main/binary-amd64/Packages.gz
dists/bookworm/main/binary-amd64/Release
dists/bookworm/main/binary-arm64/
dists/bookworm/main/binary-arm64/Packages
dists/bookworm/main/binary-arm64/Packages.bz2
dists/bookworm/main/binary-arm64/Packages.gz
dists/bookworm/main/binary-arm64/Release
dists/bookworm/main/source/
dists/bookworm/main/source/Release
dists/bookworm/main/source/Sources
dists/bookworm/main/source/Sources.bz2
dists/bookworm/main/source/Sources.gz
pool/
pool/main/
pool/main/c/
pool/main/c/cern-ca-certs/
pool/main/c/cern-ca-certs/cern-ca-certs_20230604.dsc
pool/main/c/cern-ca-certs/cern-ca-certs_20230604.tar.xz
pool/main/c/cern-ca-certs/cern-ca-certs_20230604_amd64.deb
pool/main/c/cern-ca-certs/cern-ca-certs_20230604_arm64.deb

sent 34,249 bytes  received 894 bytes  23,428.67 bytes/sec
total size is 57,730  speedup is 1.64
```

### Adding new packages or versions

This will need to be done whenever new versions of our Debian packages are released.

First, download the artifacts.zip of the jobs `build_deb12_x86_64` and `build_deb12_aarch64` of the tag pipeline of the newly-released package.

Then, unzip the artifacts:

```bash
✔ pcitds29:/tmp> mkdir cgk
✔ pcitds29:/tmp> cd cgk/
✔ pcitds29:/tmp/cgk> unzip ../artifacts1.zip
Archive:  ../artifacts1.zip
   creating: debian/output/
  inflating: debian/output/cern-get-keytab_1.5.16.dsc
  inflating: debian/output/cern-get-keytab_1.5.16.tar.xz
  inflating: debian/output/cern-get-keytab_1.5.16_arm64.buildinfo
  inflating: debian/output/cern-get-keytab_1.5.16_arm64.changes
  inflating: debian/output/cern-get-keytab_1.5.16_arm64.deb
✔ pcitds29:/tmp/cgk> unzip ../artifacts2.zip
Archive:  ../artifacts2.zip
replace debian/output/cern-get-keytab_1.5.16.dsc? [y]es, [n]o, [A]ll, [N]one, [r]ename: A
  inflating: debian/output/cern-get-keytab_1.5.16.dsc
  inflating: debian/output/cern-get-keytab_1.5.16.tar.xz
  inflating: debian/output/cern-get-keytab_1.5.16_amd64.buildinfo
  inflating: debian/output/cern-get-keytab_1.5.16_amd64.changes
  inflating: debian/output/cern-get-keytab_1.5.16_amd64.deb
```

Copy the artifacts to `ai-deb02`

```bash
✔ pcitds29:/tmp/cgk> cd debian/
✔ pcitds29:/tmp/cgk/debian> scp -r output/ airibarr@aiadm.cern.ch:
cern-get-keytab_1.5.16_arm64.buildinfo                                      100% 5251     1.6MB/s   00:00
cern-get-keytab_1.5.16_arm64.changes                                        100% 1657   476.0KB/s   00:00
cern-get-keytab_1.5.16_arm64.deb                                            100%   16KB   2.6MB/s   00:00
cern-get-keytab_1.5.16.dsc                                                  100%  660   223.4KB/s   00:00
cern-get-keytab_1.5.16.tar.xz                                               100%   26KB   2.9MB/s   00:00
cern-get-keytab_1.5.16_amd64.buildinfo                                      100% 5252     1.5MB/s   00:00
cern-get-keytab_1.5.16_amd64.changes                                        100% 1657   555.7KB/s   00:00
cern-get-keytab_1.5.16_amd64.deb                                            100%   16KB   2.6MB/s   00:00
```

```bash
[aiadm61] ~ > scp -r output/ root@ai-deb02:~/build_packages/cern-get-keytab/
Enter passphrase for key '/afs/cern.ch/user/a/airibarr/.ssh/id_rsa':
cern-get-keytab_1.5.16_arm64.buildinfo                                      100% 5251    12.4MB/s   00:00
cern-get-keytab_1.5.16_arm64.changes                                        100% 1657     5.9MB/s   00:00
cern-get-keytab_1.5.16_arm64.deb                                            100%   16KB  22.8MB/s   00:00
cern-get-keytab_1.5.16.dsc                                                  100%  660     2.5MB/s   00:00
cern-get-keytab_1.5.16.tar.xz                                               100%   26KB  57.7MB/s   00:00
cern-get-keytab_1.5.16_amd64.buildinfo                                      100% 5252    17.0MB/s   00:00
cern-get-keytab_1.5.16_amd64.changes                                        100% 1657     5.7MB/s   00:00
cern-get-keytab_1.5.16_amd64.deb                                            100%   16KB  26.3MB/s   00:00
[aiadm61] ~ > rm -rf output/
```

Add them to the repo:

```bash
root@ai-deb02:~/build_packages# aptly repo add minimal ~/build_packages/cern-get-keytab/output/*
Loading packages...
[!] Unknown file extension: /root/build_packages/cern-get-keytab/output/cern-get-keytab_1.5.16.tar.xz
[!] Unknown file extension: /root/build_packages/cern-get-keytab/output/cern-get-keytab_1.5.16_amd64.changes
[!] Unknown file extension: /root/build_packages/cern-get-keytab/output/cern-get-keytab_1.5.16_arm64.changes
[+] cern-get-keytab_1.5.16_source added
[+] cern-get-keytab_1.5.16_amd64 added
[+] cern-get-keytab_1.5.16_arm64 added
[!] Some files were skipped due to errors:
  /root/build_packages/cern-get-keytab/output/cern-get-keytab_1.5.16.tar.xz
  /root/build_packages/cern-get-keytab/output/cern-get-keytab_1.5.16_amd64.changes
  /root/build_packages/cern-get-keytab/output/cern-get-keytab_1.5.16_arm64.changes
ERROR: some files failed to be added
root@ai-deb02:~/build_packages# aptly repo show -with-packages minimal
Name: minimal
Comment:
Default Distribution: bookworm
Default Component: main
Number of packages: 6
Packages:
  cern-ca-certs_20230604_amd64
  cern-ca-certs_20230604_arm64
  cern-ca-certs_20230604_source
  cern-get-keytab_1.5.16_amd64
  cern-get-keytab_1.5.16_arm64
  cern-get-keytab_1.5.16_source
root@ai-deb02:~/build_packages# aptly snapshot create minimal-20241101 from repo minimal

Snapshot minimal-20241101 successfully created.
You can run 'aptly publish snapshot minimal-20241101' to publish snapshot as Debian repository.
root@ai-deb02:~/build_packages# aptly publish list
Published repositories:
  * ./bookworm [amd64, arm64, source] publishes {main: [minimal-20241021]: Snapshot from local repo [minimal]}
root@ai-deb02:~/build_packages# aptly publish drop bookworm
Removing /root/.aptly/public/dists...
Removing /root/.aptly/public/pool...

Published repository has been removed successfully.
root@ai-deb02:~/build_packages# aptly publish snapshot -batch -passphrase="<gpg key passphrase>" minimal-20241101
Loading packages...
Generating metadata files and linking package files...
Finalizing metadata files...
Signing file 'Release' with gpg, please enter your passphrase when prompted:
Clearsigning file 'Release' with gpg, please enter your passphrase when prompted:

Snapshot minimal-20241101 has been successfully published.
Please setup your webserver to serve directory '/root/.aptly/public' with autoindexing.
Now you can add following line to apt sources:
  deb http://your-server/ bookworm main
  deb-src http://your-server/ bookworm main
Don't forget to add your GPG key to apt with apt-key.

You can also use `aptly serve` to publish your repositories over HTTP quickly.
```

Rsync the results to linuxsoft:

```bash
root@ai-deb02:~/build_packages# kinit airibarr
airibarr@CERN.CH's Password:
root@ai-deb02:~/build_packages# rsync -avz ~/.aptly/public/ root@lxsoftadm01.cern.ch:/mnt/data1/dist/cern/debian/minimal/
sending incremental file list
./
dists/
dists/bookworm/
dists/bookworm/Contents-amd64.gz
dists/bookworm/Contents-arm64.gz
dists/bookworm/InRelease
dists/bookworm/Release
dists/bookworm/Release.gpg
dists/bookworm/main/
dists/bookworm/main/Contents-amd64.gz
dists/bookworm/main/Contents-arm64.gz
dists/bookworm/main/binary-amd64/
dists/bookworm/main/binary-amd64/Packages
dists/bookworm/main/binary-amd64/Packages.bz2
dists/bookworm/main/binary-amd64/Packages.gz
dists/bookworm/main/binary-amd64/Release
dists/bookworm/main/binary-arm64/
dists/bookworm/main/binary-arm64/Packages
dists/bookworm/main/binary-arm64/Packages.bz2
dists/bookworm/main/binary-arm64/Packages.gz
dists/bookworm/main/binary-arm64/Release
dists/bookworm/main/source/
dists/bookworm/main/source/Release
dists/bookworm/main/source/Sources
dists/bookworm/main/source/Sources.bz2
dists/bookworm/main/source/Sources.gz
pool/
pool/main/
pool/main/c/
pool/main/c/cern-ca-certs/
pool/main/c/cern-get-keytab/
pool/main/c/cern-get-keytab/cern-get-keytab_1.5.16.dsc
pool/main/c/cern-get-keytab/cern-get-keytab_1.5.16.tar.xz
pool/main/c/cern-get-keytab/cern-get-keytab_1.5.16_amd64.deb
pool/main/c/cern-get-keytab/cern-get-keytab_1.5.16_arm64.deb

sent 171,344 bytes  received 796 bytes  344,280.00 bytes/sec
total size is 236,838  speedup is 1.38
```
