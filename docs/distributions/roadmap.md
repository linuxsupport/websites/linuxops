# Distribution Roadmap

<script src="https://cdnjs.cloudflare.com/ajax/libs/rasterizehtml/1.3.0/rasterizeHTML.allinone.js"></script>
<script type="text/javascript">
// Timeline scope
const timeline = ['2024.1', '2035.2'];

// Data to visualize
const data = [
    {"row_id": "lhc",
    "items": [
        {"name": "LS2", "start": "2019.1", "end": "2022.1", "style": "ls"},
        {"name": "Run 3", "start": "2022.2", "end": "2026.1", "style": "run"},
        {"name": "LS3", "start": "2026.2", "end": "2030.1", "style": "ls"},
        {"name": "Run 4", "start": "2030.2", "end": "2032.2", "style": "run"},
        {"name": "LS4", "start": "2033.1", "end": "2035.1", "style": "ls"},
        {"name": "Run 5", "start": "2035.2", "end": "2039.1", "style": "run"},
        {"name": "LS5", "start": "2039.1", "end": "2039.2", "style": "ls"},
        {"name": "Run 6", "start": "2040.1", "end": "2041.2", "style": "run"},
    ]},
    {"row_id": "fermi",
    "items": [
        {"name": "Run", "start": "2023.1", "end": "2026.2", "style": "run"},
        {"name": "LS", "start": "2027.1", "end": "2028.2", "style": "ls"},
        {"name": "Run", "start": "2029.1", "end": "2032.1", "style": "run"},
    ]},
    {"row_id": "cc7",
    "items": [
        {"name": "CC7", "start": "2014.2", "end": "2024.1", "style": "cc7"},
        {"name": "ELS7 (RHEL7 EUS)", "start": "2024.2", "end": "2026.1", "style": "cc7"},
        //{"name": "Possible RHEL7 EUS", "start": "2027.1", "end": "2028.1", "style": "cc7-potential"},
    ]},
    {"row_id": "cs8",
    "items": [
        {"name": "CS8", "start": "2019.2", "end": "2024.1", "style": "cs8"},
    ]},
    {"row_id": "el8",
    "items": [
        {"name": "EL8", "start": "2019.2", "end": "2029.1", "style": "el8"},
    ]},
    {"row_id": "cs9",
    "items": [
        {"name": "CS9", "start": "2021.2", "end": "2027.1", "style": "cs9"},
    ]},
    {"row_id": "el9",
    "items": [
        {"name": "EL9", "start": "2022.2", "end": "2032.1", "style": "el9"},
    ]},
    {"row_id": "cs10",
    "items": [
        {"name": "CS10 ?", "start": "2025.1", "end": "2029.2", "style": "cs10"},
    ]},
    {"row_id": "el10",
    "items": [
        {"name": "EL10 ?", "start": "2025.2", "end": "2035.1", "style": "el10"},
    ]},
    {"row_id": "el11",
    "items": [
        {"name": "EL11 ?", "start": "2028.2", "end": "2038.1", "style": "el11"},
    ]},
    {"row_id": "deb12",
    "items": [
        {"name": "Debian 12", "start": "2023.2", "end": "2026.1", "style": "deb12"},
        {"name": "Debian 12 LTS", "start": "2026.2", "end": "2028.1", "style": "deb12"},
        {"name": "Debian 12 ELTS", "start": "2028.2", "end": "2033.1", "style": "deb12"},
    ]},
    {"row_id": "deb13",
    "items": [
        {"name": "Debian 13 ?", "start": "2025.2", "end": "2028.1", "style": "deb13"},
        {"name": "Debian 13 LTS ?", "start": "2028.2", "end": "2030.1", "style": "deb13"},
        {"name": "Debian 13 ELTS ?", "start": "2030.2", "end": "2035.1", "style": "deb13"},
    ]},
];

// Canvas dimensions for the timeline export
const CANVAS_WIDTH = 1224;
const CANVAS_HEIGHT = 768;
</script>

<style>
/* Yes, this needs to be here and not somewhere in stylesheets/. We need it to export the image correctly. */
table.roadmap {
  border-spacing: revert;
}
table.roadmap th {
  border: 1px solid #999;
  padding-top: 0.2rem;
  padding-bottom: 0.2rem;
  font-weight: revert;
}
table.roadmap td {
  padding: 0.5rem;
  text-align: center;
  font-size: 17px;
  font-weight: bold;
  color: white;
}
table.roadmap td.title {
  background-color: lightblue;
  color: black;
}
.controls {
  padding-bottom: 20px;
  color: grey;
}
button.controls {
  border: none;
  color: black;
  background: lightgrey;
  padding: 10px 15px;
  text-align: center;
  display: inline-block;
  font-size: 1.2em;
  margin: 10px;
  border-radius: 10px;
  transition-duration: 0.4s;
}
.current_date {
  background-color: #4900fc;
  color: rgb(255, 255, 255);
}
.run {
  background-color: #ccffcc;
  color: black !important;
}
.ls {
  background-color: #8db4e2;
}
.cc7 {
  background-color: purple;
}
.cc7-potential {
  background-color: #80008038;
}
.cs8 {
  background-color: darkgreen;
}
.el8 {
  background-color: blue;
}
.el9 {
  background-color: darkblue;
}
.cs9 {
  background-color: yellow;
  color: black !important;
}
.cs10 {
  background-color: rgba(0, 255, 221, 0.658);
  color: black !important;
}
.el10 {
  background-color: rgba(0, 98, 85, 0.658);
  color: black !important;
}
.el11 {
  background-color: rgba(21, 191, 32, 0.84);
  color: black !important;
}
.deb12 {
    background-color: red;
    color: black !important;
}
.deb13 {
    background-color: #f76969;
    color: black !important;
}
</style>

<form class="controls">
    Show:
    <input type="checkbox" id="chk1-lhc" checked>
    <label for="chk1-lhc">LHC Schedule</label>
    <input type="checkbox" id="chk1-fermi" checked>
    <label for="chk1-fermi">Fermilab Schedule</label>
    <input type="checkbox" id="chk1-cc7" checked>
    <label for="chk1-cc7">CC7</label>
    <input type="checkbox" id="chk1-cs8">
    <label for="chk1-cs8">CS8</label>
    <input type="checkbox" id="chk1-el8" checked>
    <label for="chk1-el8">EL8</label>
    <input type="checkbox" id="chk1-cs9">
    <label for="chk1-cs9">CS9</label>
    <input type="checkbox" id="chk1-el9" checked>
    <label for="chk1-el9">EL9</label>
    <input type="checkbox" id="chk1-cs10" checked>
    <label for="chk1-cs10">CS10</label>
    <input type="checkbox" id="chk1-el10" checked>
    <label for="chk1-el10">EL10</label>
    <input type="checkbox" id="chk1-el11" checked>
    <label for="chk1-el11">EL11</label>
    <input type="checkbox" id="chk1-deb12" checked>
    <label for="chk1-deb12">Deb12</label>
    <input type="checkbox" id="chk1-deb13" checked>
    <label for="chk1-deb13">Deb13</label>
</form>

<table class="roadmap" id="distribution"></table>

<button type="button" class="controls" onclick="exportSVG('distribution')">Export</button>
