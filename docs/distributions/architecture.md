# lxdist Architecture

## Script symlinks

All scripts are stored in git [https://gitlab.cern.ch/linuxsupport/lxdist-build](https://gitlab.cern.ch/linuxsupport/lxdist-build) which is initialised in the home directory of the build user (/mnt/data2/home/lxdist-build)

The following symlinks exist, linked to the above git repository.

| Path | Symlinked to
| -- | -- |
| /mnt/data2/home/build/bin | /mnt/data2/home/build/lxdist-build/bin |
| /mnt/data2/bin | /mnt/data2/home/build/lxdist-build/lxsoftbin |
| /mnt/data2/home/packages/configs | /mnt/data2/home/build/lxdist-build/packages/configs |
| /mnt/data2/home/modules/configs| /mnt/data2/home/build/lxdist-build/modules/configs |
| /mnt/data2/home/modules/configs| /mnt/data2/home/build/lxdist-build/modules/configs |


## File systems

| Mount path | Purpose
| -- | -- |
| /mnt/data2 | Staging area (cephfs) |
| /mnt/data1 | Production area (cephfs) |
| /mnt/data3 | Production area (flash cephfs) |
| /eos/project	| Archive area |

## lxsoft update hosts

these hosts are used for testing purposes

| fqdn | dist/arch |
| -- | -- |
| lxvmcc7 | CERN CentOS 7 x86_64 |
| lxvmcs8 | CentOS Stream 8 x86_64 |
| lxvmrh7 | Red Hat Enterprise Linux 7 x86_64 |
| lxvmrh8 | Red Hat Enterprise Linux 8 x86_64 |
| lxvmrh9 | Red Hat Enterprise Linux 9 x86_64 |
| lxvmal8 | AlmaLinux 8 x86_84 |
| lxvmal9 | AlmaLinux 9 x86_64 |

* These VMs can be rebuilt at any time, which is scripted via AIMS. Essentially these VMs have an iPXE image defined as their boot image so to rebuild we set the AIMS pxeon flag and issue an 'openstack server rebuild' command to the VM in question.

* This process is simplified via the `rebuild-lxdist-testing-vm.sh` script which can be found in [https://gitlab.cern.ch/linuxsupport/lxdist-vm-rebuild](https://gitlab.cern.ch/linuxsupport/lxdist-vm-rebuild)

Note - you can rebuild ALL testing hosts with the following command `rebuild-lxdist-testing-vm.sh -s all -p`
