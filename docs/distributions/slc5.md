# Scientific Linux CERN 5

!!! danger
    SLC5 is deprecated and you should not be running this commands

## Practical info

* Upstream support : EOL-ed on 2019-04-01
* As of 10/2019 CERN OpenStack images for SLC5 have been marked as 'community'. This means they are hidden from the list of images available, however it's still possible to spawn a SLC5 VM if the user knows the UUID of the image. This allows us to essentially deprecate the distribution, however still provide it in 'emergency' situations. An example of how to spawn a SLC5 VM is as follows:
    * `ai-bs -i 254c5631-bd91-49ca-a19d-f5d72d9af165` # SLC5 - i386 [2018-03-16]
    * `ai-bs -i fbdd1097-8cb9-46ca-903f-7caf099f34f5` # SLC5 - x86_64 [2018-03-16]


## Building updates

These directories allow an administrator to rebuild packages provided by Red Hat and new CERN packages.

| Incoming |
| -- |
| /mnt/data2/home/build/packages/incoming/slc5/updates |
| /mnt/data2/home/build/packages/incoming/slc5/extras |
| /mnt/data2/home/build/packages/incoming/slc5/cernonly	|
| /mnt/data2/home/build/packages/incoming/slc5/mrg |

> Notes:
> * Source or binary package can be added to incoming. Only commercial packages are generally added.

|Build|
| -- |
|/mnt/data2/home/build/bin/bsbuild -d "slc5" -r "updates extras cernonly mrg"|
|/mnt/data2/home/build/bin/bsdownload -s -d "slc5" -r "updates extras cernonly mrg"|

> Notes:
>* Grab the signing pass phrase in tbag :
```
tbag show distribution_signing_key_passphrase --hg lxsoft/adm
```

|If New kernel package : e.g kernel-**2.6.XXX.elX**, please build associated modules| |
| -- | -- |
| bsbuildmodules -d slc5 -k **2.6.XXX.elX** | build with mock new modules |
| bscopymodules -d slc5 -k **2.6.XXX.elX** | copy modules in the build area|

> Notes:
> * You need then to follow "Release to testing" procedure.

|Release to testing||
| -- | -- |
| /mnt/data2/home/build/bin/bsbuild2test -d slc5 -r "testing" | Move all built packages to testing|
| /mnt/data2/home/build/bin/bsregenrepos -d slc5 -r "testing mrg-testing" | Regenerate testing repositories metadata |
| /mnt/data2/home/build/bin/bssync -t "data1 eos" -d slc5 -r "testing mrg-testing" |sync /mnt/data2 -> /mnt/data1 |
| /mnt/data2/home/build/bin/bsmailusers -d slc5 -r testing -e -s -w | Email linux-announce-test@cern.ch with updated packages|

> Notes:
> * $EDITOR must be set to your preferred editor to edit the email

|Release to production||
| -- | -- |
| /mnt/data2/home/build/bin/bstest2prod -d slc5 -r "updates mrg" | List available updates |
| /mnt/data2/home/build/bin/bstest2prod -d slc5 -r "updates mrg" [pkgname to release] | Promote package. \\* can be used to promote all updates. A careful review is needed in general. |
|/mnt/data2/home/build/bin/bsregenrepos -d slc5 -r "testing mrg-testing cernonly extras updates mrg"| Regenerate production repositories metadata (=~ 30min) |
|/mnt/data2/home/build/bin/bssync -t "data1 eos" -d slc5 -r "testing mrg-testing cernonly extras updates mrg"	| sync /mnt/data2 -> /mnt/data1|
| /mnt/data2/home/build/bin/bsmailusers -d slc5 -r updates -e -s -w | Email linux-announce@cern.ch with updated packages|

|Update web pages||
| -- | -- |
| vi /afs/cern.ch/project/linux/wwwtest/freshnews.shtml	| Edit the web pages, section can be copied from previous week. Previous sent Email subject is used as a title. Dates need to be updated. (need better automation) |
| /afs/cern.ch/project/linux/adm/bin/wwwtest2prod | sync web page to production site.|
