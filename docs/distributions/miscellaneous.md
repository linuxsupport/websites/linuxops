# Miscellaneous

## flash-plugin

We no longer maintain a custom package that contains the NPAPI firefox flash plugin.

The below notes are provided simply for historical information.

You can check the latest version from Adobe on this page [https://get.adobe.com/flashplayer/about/](https://get.adobe.com/flashplayer/about/) or alternatively:

$ `curl -s https://get.adobe.com/flashplayer/about/  |grep -A3 "Linux" | tail -1 | cut -d\> -f2 | cut -d\< -f1`

The process to rebuild the package is documented inside the git repository found here [https://gitlab.cern.ch/linuxsupport/flash-plugin](https://gitlab.cern.ch/linuxsupport/flash-plugin)

## nss

We should NOT need to rebuild the nss package, as newer versions of thunderbird/firefox refer to the system ca bundle without requiring explicit reference to certificates in the nss database.

If we ever do need to, you can check out [the nss repo](https://gitlab.cern.ch/linuxsupport/rpms/nss) for an automated method to create the src rpm

The manual / legacy process is:

* Download the most current CERN src rpm: `yumdownloader --source nss`
* Download and install the most current upstream src rpm: `rpm -ivh http://vault.centos.org/7.6.1810/cr/Source/SPackages/nss-xyz.el7.src.rpm`
* extract the CERN RPM contents to a temporary directory `mkdir nss-temp && mv nss*src.rpm nss-temp && cd nss-temp && rpm2cpio nss*src.rpm | cpio -idmv`
* copy nss-temp/certdata.cern.txt to ~/rpmbuild/SOURCES/
* Diff the CERN and upstream spec files, but essentially the differences should be adding 'Source100' and the certdata.txt block (search for 'CERN') to `~/rpmbuild/SPECS/nss.spec`
* Build the new src rpm: `rpmbuild -bs --define "el7.cern" ~/rpmbuild/SPECS/nss.spec --nodeps`
* Copy the src to incoming `cp /mnt/data2/home/build/rpmbuild/SRPMS/nss-3.44.0-4.el7.cern.src.rpm ~/packages/incoming/cc7/updates`
