## How to test a different version of a rpm
- Git clone the project in the machine were you want to do the tests
- Install make and rpm-build
- Delete the installed package from cern repos (witouth remove deps): `rpm -e --nodeps <package>`
- Create the new rpm (dont forget to change the changelog): `make rpm` 
- Install the rpm  `rpm -ivh build/RPMS/noarch/<rpm>`

## How to check if a CVE has been fixed
A better way to check if a CVE has been fixed is to use `rpm -q --changelog <package>` and read/grep the content.

## Repositories

- List all repositories: `yum repolist all`
- Add a repository: `yum-config-manager --add-repo=<repopath>`
- Enable a repository: `yum-config-manager --enable <reponame>`
- List all packages of a repopath: `yum --repofrompath=test,<repopath> --repo=test list available`

## What to do when there is a security vulnerability
1) Check if the fix is release upstream for Alma Linux and RHEL to each distribution.
    1.1) RHEL releases packages when there is an errata in https://access.redhat.com/security/cve/CVE-<cve_number>
    1.2) Almalinux release packages in <https://repo.almalinux.org/almalinux/9/BaseOS/x86_64/os/Packages/> or watch <https://errata.almalinux.org>
2) Run reposync rhel nomad job
3) Run rsync almalinux nomad job
4) Confirm the packages exist on `/mnt/data1/dist/cdn.redhat.com/` and `/mnt/data1/dist/almalinux`
5) Reply to the email thread stating that packages have been released by redhat and they will be available in testing tomorrow
6) When tomorrow arrives, confirm that the snapshot script does release them to testing
7) Again email people saying that the packages are available in testing
8) Wait to see if security want them promoted to production earlier
9) If so, follow the manually-updating-symlinks documentation.

