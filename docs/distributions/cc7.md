# CERN CentOS 7

## Practical info

!!! danger ""
    Upstream support: until 2024-06-30.
    ELS support (only for ATS): until [2026-06-30](https://edms.cern.ch/ui/#!master/navigator/document?D:101163581:101163581:subDocs).
    Upstream ELS support: until [2028-07-01](https://www.redhat.com/en/blog/announcing-4-years-extended-life-cycle-support-els-red-hat-enterprise-linux-7).

## Automation

As of December 2020, the daily release process is completely automated through the nomad job [centos7_release](https://gitlab.cern.ch/linuxsupport/cronjobs/centos7_release). This job utilises the `bsauto` script which is essentially a wrapper around the 'bs' scripts.

Apart from the obvious, the script also supports:

* Holiday logic (does not run on weekends, Christmas Shutdown or CERN public holidays)
* Building CERN rpms (if they are placed in the necessary `incoming` directories)
* Automatic rebuild of the openafs kmod for main and centosplus kernels
* Ability to pause the release by populating a `/mnt/data2/home/build/bin/.freeze` file
* Ability to exclude packages from promotion to production by populating the `/mnt/data2/home/build/bin/exclude_list` file
* Ability to exclude packages from being synced from upstream by populating the `/mnt/data2/home/build/bin/exclude_list_upstream` file
* Administrator email notifications in the event of a failure, no new packages, or if only src/debug packages were released

!!! danger ""
    Nobody should need to follow the below instructions for daily testing/production updates, however the information is still provided if at all needed.

Note - if required `bsauto` can be executed interactively

```
[build@lxsoftadm01] bsauto -h
usage: /mnt/data2/home/build/bin/bsauto -t -p

This script automates the daily release process
Only cc7 is supported

OPTIONS:

  -t        execute testing release process
  -p        execute production release process
  -d        execute dryrun only
  -s        ask before executing each step
  -h        display this help
```

## Pushing upstream updates

As we do not rebuild upstream packages we just have to sync it from our [local mirror](https://linuxsoft.cern.ch/centos/)

Build:

```
/mnt/data2/home/build/bin/bscentos2test -d "cc7" -r "all"
```
bscentos2test will inform you of the repositories that should be updated with bsregenrepos

Sometimes src rpms take some time to be published, therefore this step might be delayed until a few days later. **Since you cannot exclude a package from the procedure, you will have to exclude its repo until the procedure can take this repo into account.**

If a new kernel package is available (e.g `kernel-3.10.0-957.27.2.el7` or `kernel-3.10.0-1062.4.3.el7.centos.plus`), please build the associated modules:

```bash
# Build new modules with mock
bsbuildmodules -d cc7 -k 3.10.0-${kversion}.el7
# or for plus packages
bsbuildmodules -d cc7 -k 3.10.0-${kversion}.el7.centos.plus
# Copy modules in the build area
bscopymodules -d cc7 -k 3.10.0-${kversion}.el7
# or for plus packages
bscopymodules -d cc7 -k 3.10.0-${kversion}.el7.centos.plus

# The script will prompt you to sign the modules, using the common secret:
# tbag show distribution_signing_key_passphrase --hg lxsoft/adm
bsdownload -a -s to sign the modules

# New kernel module is in the cern repo
bsbuild2test -d cc7 -r "cern cernonly rt centosplus rhcommon"
```

!!! note ""
    You then need then to follow the [Release to testing](#release-to-testing) procedure as modules are added to the `cern` repository.

## Building internal updates

These directories allow an administrator to rebuild packages provided by Red Hat and new CERN packages.

| Incoming |
| -- |
| /mnt/data2/home/build/packages/incoming/cc7/cern |
| /mnt/data2/home/build/packages/incoming/cc7/cernonly |
| /mnt/data2/home/build/packages/incoming/cc7/rt	|
| /mnt/data2/home/build/packages/incoming/cc7/centosplus |
| /mnt/data2/home/build/packages/incoming/cc7/rhcommon |
| /mnt/data2/home/build/packages/incoming/cc7/cr |

!!! note ""
    Source or binary packages can be added to incoming, however it's always preferable to populate this directory with source RPMs to ensure that the build process through koji is followed.

Build:

```
/mnt/data2/home/build/bin/bsbuild -d "cc7" -r "all"
/mnt/data2/home/build/bin/bsdownload -s -d "cc7" -r "all"
```

!!! note "Signing packages"
    In order to sign packages, retrieve the signing passphrase from teigi:

    ```
    tbag show distribution_signing_key_passphrase --hg lxsoft/adm
    ```

!!! note ""
    From time to time we might receive [requests](https://cern.service-now.com/service-portal?id=ticket&table=u_request_fulfillment&n=RQF1573433) asking for proprietary software to be added to `cernonly` repo. To do so you will still need to run `/mnt/data2/home/build/bin/bsdownload -s -d "cc7" -r "all"` to sign the packages. Remember to use `REPOLIST="cernonly-testing"` for the next steps.

If any packages were built in the above step, move the built packages to testing:
```
/mnt/data2/home/build/bin/bsbuild2test -d cc7 -r "cern cernonly rt centosplus rhcommon"
```

## Release to testing

!!! Note ""
    On Thursday morning please [release to production](#Release-to-production), then release to testing

**Note - check the output of the previous commands for yellow text which will inform you which repositories should be worked on ($REPOLIST in the below steps)**

Ex. `REPOLIST="cern-testing cernonly"`

Regenerate testing repositories metadata:
```
/mnt/data2/home/build/bin/bsregenrepos -d cc7 -r "$REPOLIST"
```

sync /mnt/data2 -> /mnt/data1:
```
/mnt/data2/home/build/bin/bssync -t "data1 eos" -d cc7 -r "$REPOLIST"
```

!!! Note ""
    If by mistake a package got to testing when it should not, please follow [this troubleshooting section](https://linuxops.web.cern.ch/distributions/troubleshooting/#Revert-publishing-a-test-rpm)

Send an email to `linux-announce-test@cern.ch` with updated packages information. This will as well add the updates to <https://linux.web.cern.ch/updates/cc7/test/latest_updates/>:

```
/mnt/data2/home/build/bin/bsmailusers -d cc7 -r testing -e -s
```

!!! Note ""
    `$EDITOR` must be set to your preferred editor to edit the email

!!! note "No packages in the mail's draft?"
    If you thought there were some packages to be updated but you don't see any in the draft, it might very well be that these were `debugingo` or `src.rpm`s. As you can see in [this line](https://gitlab.cern.ch/linuxsupport/lxdist-build/blob/master/bin/bsmailusers#L197) we do not send updates for those.

## Release to production

Generate list of repos that have been updated in the last week
```
/mnt/data2/home/build/bin/generaterepolist -d cc7
```

List available updates:
```
/mnt/data2/home/build/bin/bstest2prod -d cc7 -r "$REPOLIST"
```

Promote package. `\*` can be used to promote all updates. A careful review is needed in general:
```
/mnt/data2/home/build/bin/bstest2prod -d cc7 -r "$REPOLIST" [pkgname to release]
```
Notes:

* If any packages are excluded (as defined via EXCLUDE_LIST in bstest2prod) a reference to the exclude list will be displayed in yellow. Be sure to check that a package is not being accidentally excluded as the EXCLUDE_LIST works through 'grep'

* Running this command will move files from testing to what will be production, for this reason we should bsregenrepos on both testing and non-testing repositories

Generate list of repos that have been updated in the last week + including testing repositories.
```
/mnt/data2/home/build/bin/generaterepolist -d cc7 -t
```
Regenerate production repositories metadata (=~ 15min):
```
/mnt/data2/home/build/bin/bsregenrepos -d cc7 -r "$REPOLIST"
```


!!! danger "Sync /mnt/data2 -> /mnt/data1:"
    Running `bssync` will actually "de facto" move all packages to production. Reversing this action is not trivial:

    ```
    /mnt/data2/home/build/bin/bssync -t "data1 eos" -d cc7 -r "$REPOLIST"
    ```

!!! Note ""
    You may want to run a first iteration with `data1` only, and sync to `eos` later (once the email has been sent), as it takes considerably longer

## Update web pages & send email

The below process should be followed:

run
```
/mnt/data2/home/build/bin/bsmailusers -d cc7 -r updates -e -s
```

Confirm 'yes' to send to send the mail. This will also [push an update automatically](https://gitlab.cern.ch/linuxsupport/websites/linux.cern.ch) that will end up showing on on <https://linux.web.cern.ch/updates/cc7/prod/latest_updates/>

## 'Minor' (7.x -> 7.x+1) version release process.

### Introduction

This documentation describes the steps needed to promote an upstream point release

CentOS typically releases the new version ~40 days after the RHEL release. The process CentOS follows is to first release new binary packages into the continuous release (CR) repository. This allows the community to test the upgrade process before they are promoted to GA and binary ISO images are built and published.

The CERN process is similar to the standard release process, making the upstream packages in CR available for CC7. The CERN CR repository is enabled by default on all 'QA' nodes, thus we have a fleet of machines that will upgrade/distrosync to the new packages.

### Release to testing (CR)

Once CR is available, the steps to facilitate the new release are:

* Set these variables accordingly
```
RELEASE="9"
YEARMONTH="2009"
```

* Create `/mnt/data{1,2}/dist/cern/centos/7.$release.$yearmonth/{os,cr,cr-testing}` directories
```
mkdir /mnt/data{1,2}/dist/cern/centos/7.$RELEASE.$YEARMONTH/{os,cr,cr-testing} -p
```
* Create `/mnt/data2/dist/cern/centos/7.$release.$yearmonth/cr-testing/Debug/{i386,x86_64}/Packages` directories
```
mkdir /mnt/data2/dist/cern/centos/7.$RELEASE.$YEARMONTH/cr-testing/Debug/{i386,x86_64}/Packages -p
```
* Symlink `/mnt/data{1,2}/dist/cern/centos/7.$release.$yearmonth to /mnt/data{1,2}/dist/cern/centos/7.$release`
```
ln -s /mnt/data2/dist/cern/centos/7.$RELEASE.$YEARMONTH/ /mnt/data2/dist/cern/centos/7.$RELEASE
ln -s /mnt/data1/dist/cern/centos/7.$RELEASE.$YEARMONTH/ /mnt/data1/dist/cern/centos/7.$RELEASE
```
* Symlink `/mnt/data{1,2)/dist/cern/centos/7.1.1503/{centosplus,centosplus-testing,cern,cern-testing,cernonly,cernonly-testing,cloud,cloud-testing,extras,extras-testing,fasttrack,fasttrack-testing,rhcommon,rhcommon-testing,rt,rt-testing,sclo,sclo-testing,storage,storage-testing,updates,updates-testing,virt,virt-testing} to /mnt/data{1,2}/dist/cern/centos/7.$release.$yearmonth/`:
```
for directory in data1 data2
do
  cd /mnt/$directory/dist/cern/centos/7.$RELEASE.$YEARMONTH/
  for i in centosplus centosplus-testing cern cern-testing cernonly cernonly-testing cloud cloud-testing extras extras-testing fasttrack fasttrack-testing rhcommon rhcommon-testing rt rt-testing sclo sclo-testing storage storage-testing updates updates-testing virt virt-testing;
  do
    ln -s ../7.1.1503/$i $i;
  done
done
```
* Download cr from 'upstream': `bscentos2test -d "cc7" -r cr`
* Rebuild the kernel: `bsbuildmodules`:
```
bsbuildmodules -d cc7 -k 3.10.0-$KERNELVERSION.el7  # you'll find the exact version in the output of the `bscentos2test` command.
bscopymodules -d cc7 -k 3.10.0-$KERNELVERSION.el7
bsdownload -a -s
```
* Create missing directories `/mnt/data2/dist/cern/centos/7.$release.$yearmonth/{cr/Source,cr/x86_64}/SPackages`
```
mkdir /mnt/data2/dist/cern/centos/7.$RELEASE.$YEARMONTH/{cr/Source,cr/x86_64}/SPackages -p
```
* Move CERN built packages: `bsbuild2test -d cc7 -r "cr cern"`
* Create cr-testing repodata: `bsregenrepos -d cc7 -r "cr-testing cern-testing"`
* Sync to data1/eos: `bssync -t "data1 eos" -d cc7 -r "cr-testing cern-testing"`
* Perform testing on a VM with `yum --enablerepo={cr,cern}-testing update`
```
yum --enablerepo={cr,cern}-testing clean all
yum --enablerepo={cr,cern}-testing update
```
* Promote cr-testing to cr `bstest2prod -d cc7 -r cr \*`
* Regen repodata `bsregenrepos -d cc7 -r "cr cr-testing"`
* Sync to data1/eos `bssync -d cc7 -r "cr cr-testing" -t "data1 eos"`. Note you may have to run this command more than once due to hardcoded limits in the bssync script regarding deletions.
* Test the upgrade process on several VMs:
```
yum --enablerepo=cr --enablerepo={updates,extras,cern}-testing --enablerepo=epel-testing clean all
yum --enablerepo=cr --enablerepo={updates,extras,cern}-testing --enablerepo=epel-testing update
```
* Update the webpage at [http://linux.web.cern.ch/linux/news.shtml](https://linux.web.cern.ch/#latest-news)
* Add an entry to the ITSSB (the following can be used an example [OTG0052241](https://cern.service-now.com/service-portal?id=outage&n=OTG0052241))
* Send email template to users informing that CentOS7.$release is available for testing. The recipient should be `linux-users@cern.ch` and the following recipients should be included in BCC `linux-announce@cern.ch`, `linux-announce-test@cern.ch`, `ai-admins@cern.ch`

```
From: Ben Morrice
Sent: 03 September 2019 14:58
To: linux-users (Forum for Linux related information exchange at CERN.)
Subject: CERN CentOS 7.7 testing available.

Dear Linux users.

We are pleased to announce the updated CERN CentOS Linux (CC7) TEST version:

             *******************************
             CC 7.7 is available for testing
             *******************************
                as of Tuesday 03.09.2019.

For information about CC7, including installation instructions please visit:

         http://cern.ch/linux/centos7/

Please note that install media is not yet available.
We will send an additional email when ready.

* Updating CERN CentOS 7.6 to 7.7

CC7 system can be updated by running:

# yum --enablerepo=cr \
  --enablerepo={updates,extras,cern}-testing \
  --enablerepo=epel-testing \
  clean all

# yum --enablerepo=cr \
  --enablerepo={updates,extras,cern}-testing \
  --enablerepo=epel-testing \
  update

* Agile infrastructure

As a reminder, all puppet configured machines within the
QA environment will receive automatically those updates.

--
Ben Morrice for Linux.Support@cern.ch
```

### Release TEST boot media, TEST VM

This stage involves working on a new tree from upstream (QA). We add CERN specific packages to an anacondacern repository which is referred to via custom anaconda additions on a boot.iso which is also authored by us.

You can check whether the ostree has been already made public:

```
PREVIOUS_RELEASE="8"
RELEASE="9"
YEARMONTH="2009"
rsync -4 --list-only rsync://qa.centos.org/c5beta-qa/CentOS/7.$RELEASE.$YEARMONTH/
```

Once it is there (showing some contents):

* `mkdir -p /mnt/data2/dist/cern/centos/7.$RELEASE/os/x86_64/`
* `cp -a /mnt/data2/dist/cern/centos/7.$PREVIOUS_RELEASE/os/x86_64/CERN /mnt/data2/dist/cern/centos/7.$RELEASE/os/x86_64/`
* `cp /mnt/data2/dist/cern/centos/7.$PREVIOUS_RELEASE/os/x86_64/RPM-GPG-KEY-cern /mnt/data2/dist/cern/centos/7.$RELEASE/os/x86_64/`
    * Note: the scripts contained in the CERN directory can also be found in git here: <https://gitlab.cern.ch/linuxsupport/lxdist-build/tree/master/newrelease>
* Edit `/mnt/data2/dist/cern/centos/7.$RELEASE/os/x86_64/CERN/build/syncfromcentosqa.sh` to the correct $RELEASE and execute the script to download a local tree to data2

* Build a new `centos-release` package for the new released version:
    * Install the previous src rpm (You can find it `/mnt/data1/dist/cern/centos/7/cern/Source/SPackages/`)
        ```
        rpm -i /mnt/data1/dist/cern/centos/7/cern/Source/SPackages/centos-release-7-$PREVIOUS_RELEASE.$YEARMONTH.0.el7.cern.src.rpm
        ```
    * Extract the upstream rpm and compare it's contents with the tarball from the cern src rpm (`usr/lib/systemd` may change slightly, e.g. `90-default.preset` or `85-display-manager.preset`)
        ```
        rpm2cpio centos-release-7-$RELEASE.$YEARMONTH.0.el7.centos.x86_64.rpm | cpio -idmv
        ```
    * Rebuild the source tar if anything changed, and thus needs to be added
    * Edit `~/rpmbuild/SPECS/centos-release-cern.spec` and update the release numbers, the changelog:
    e.g.
    * Build the new package:
    ```
    rpmbuild -ba centos-release-cern.spec
    ```
    * Upload it to koji as well:
    ```
    koji build cc7-cern centos79release/centos-release-7-$RELEASE.$YEARMONTH.0.el7.cern.src.rpm
    ```
    * Sign centos-release with `rpm --define '_gpg_name 1D1E034B' --define '_signature gpg' --addsign centos-release7-$release.0.el7.cern.x86_64.rpm`
    ```
    # Get the signing passphrase from:
    tbag show distribution_signing_key_passphrase --hg lxsoft/adm
    ```
    * Move centos-release to `/mnt/data2/dist/cern/centos/7.$RELEASE/os/x86_64/CERN/Packages`
* Update CERN/Packages to newer versions if needed
    * You can use the `/mnt/data2/dist/cern/centos/7.$RELEASE/os/x86_64/CERN/build/generate_list_CERN_rpms` to display the packages that should be updated
    * Note: We only keep one version in the cernanaconda repository, delete all older versions

* Ensure comps.xml is up to date (`cp /mnt/data1/dist/cern/centos/7/cern/x86_64/comps.xml /mnt/data2/dist/cern/centos/7.$RELEASE/os/x86_64/CERN/build/CERN-comps.xml`)
* Generate repodata for cernanaconda by editing and executing the `/mnt/data2/dist/cern/centos/7.$RELEASE/os/x86_64/CERN/build/makerepo.sh`. Note - if a 'repodata' directory existed from a previous copy, remove it first to start clean.
* Check that generated content is correct - edit and then run `/mnt/data2/dist/cern/centos/7.$RELEASE/os/x86_64/CERN/build/checks.sh`
* rsync `/mnt/data2/dist/cern/centos/7.$RELEASE/os/x86_64/` to an accessible location on `/mnt/data1/dist/tmp/` (this correlates to [http://linuxsoft.cern.ch/tmp/](http://linuxsoft.cern.ch/tmp/)):

* Generate the CERN version of boot.iso:
    * Download the upstream boot.iso, mount to a temporary location and copy the contents to `/mnt/data2/dist/cern/centos/7.$RELEASE/os/x86_64/CERN/build/boot.iso.org`:
    ```
    mkdir /mnt/iso79
    mount -o loop /mnt/data2/dist/cern/centos/7.$RELEASE.$YEARMONTH/os/x86_64/images/boot.iso /mnt/iso79/
    cp /mnt/iso79 /mnt/data2/dist/cern/centos/7.$RELEASE.$YEARMONTH/os/x86_64/CERN/build/boot.iso.org
    ```
    * `unmount` temporary boot.iso mount:
    ```
    umount /mnt/iso79
    ```
    * `unsquash` /mnt/data2/dist/cern/centos/7.$RELEASE/os/x86_64/CERN/build/boot.iso.org/LiveOS/squashfs.img and then mount rootfs.img to a temporary location
    ```
    unsquashfs /mnt/data2/dist/cern/centos/7.$RELEASE.$YEARMONTH/os/x86_64/CERN/build/boot.iso.org/LiveOS/squashfs.img
    mkdir /tmp/iso97rootfs
mount /mnt/data2/dist/cern/centos/7.$RELEASE.$YEARMONTH/os/x86_64/CERN/build/boot.iso.org/LiveOS/squashfs-root/LiveOS/rootfs.img /tmp/iso97rootfs
    ```
    * If required, rebase the files in /mnt/data2/dist/cern/centos/7.$RELEASE/os/x86_64/CERN/build/updates.img/run/install/ off the versions contained in rootfs.img:
    ```
    # For an example of what was done from 7.8 to 7.9:
    diff -u /mnt/data2/dist/cern/centos/7.9/os/x86_64/CERN/build/updates.img/run/install/updates/pyanaconda/packaging/yumpayload.py.{78,79}
    ```
    * `unmount` temporary roofs.img mount
    ```
    umount /mnt/iso79rootfs
    ```
    * Edit and execute `/mnt/data2/dist/cern/centos/7.$RELEASE/os/x86_64/CERN/build/makeimages.sh`
* Test boot.iso
    * `dd` the boot.iso file to a USB stick and test that it can be booted (physically, on a desktop, for instance). You could also test the ISO on any virtualization software such as VirtualBox.
    * Boot a VM with the iso and go through the complete installation process using the path you populated before as the install media location ([http://linuxsoft.cern.ch/tmp/](http://linuxsoft.cern.ch/tmp/))
    ```
    # You'll need to upload it to OpenStack:
    openstack image create --file boot.iso --disk-format raw C7$RELEASE$YEARMONTH
    ```
    * Confirm that there are no errors during installation and that the CERN addon is loaded on system boot
* Remove the temporary location you created at /mnt/data1/dist/tmp
* Edit and run `/mnt/data2/dist/cern/centos/7.$RELEASE/os/x86_64/CERN/build/sync.sh` to populate /mnt/data1/dist/cern/centos/7.$RELEASE/os
     NOTE: Make sure the sync command contains `x86_64`, as opposed to `SOURCES`, which is done at a later step.
* Create a TEST glance image through [koji-image-build](https://gitlab.cern.ch/linuxsupport/koji-image-build)
    * You will need to update the cc7-base.ks and buildimage.sh with reference to the new $RELEASE
    * Run the [monthly scheduled pipeline](https://gitlab.cern.ch/linuxsupport/koji-image-build/-/pipeline_schedules) of koji-image-build, which will build and test (using [image-ci](https://gitlab.cern.ch/linuxsupport/testing/image-ci/)) new images for CC7 and C8 (which you can ignore). If the tests go well, the new images will be uploaded as TEST.
* Add an `.htaccess` file in /mnt/data1/dist/cern/centos/7.$RELEASE to forbid consuming until CentOS QA content is released publicly:
    ```
    cat /mnt/data1/dist/cern/centos/7.$RELEASE/.htaccess
    Deny from all
    ```
* Edit and execute `/mnt/data2/dist/cern/centos/7.$RELEASE/os/x86_64/CERN/build/aims2.sh` to configure a target on aimstest and a TEST target on aims
* Test the PXE install process on both `aimstest` and `aims`
  * Upload the image to AIMS with `aims2client`, accessible on `aiadm`. i.e. `aims2client addimg ...` and `aims2client --testserver addimg ...`
  * On the `"IT Linux Support - Test VMs"` create a VM with `landb-os=LINUX` as metadata and https://gitlab.cern.ch/linuxsupport/aims2-ipxe as image. You will already find uploaded images though so reuse one of them.
  * Prepare the host to test the image as in the following example:
    * `aims2client addhost yourtestvm --pxe --name CC79_X86_64 --kopts "ip=dhcp repo=http://linuxsoft.cern.ch/tmp/79/"`. Adjust the repo to the path from previous steps
    * Open a VNC console on the web browser by going to the "Console" tab on the given test VM
    * Go through the manual installation configuration. You will need to select "Reclaim space" on the partitioning configuration.
    * If at any point you need to re-run the installation, rebuild the same VM with the same `iPXE-YYYYMMDD-aimsprod` image as before, then reboot and start again, it only takes a minute.
* Update [AIMS hostgroup Hiera data](https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/-/blob/qa/data/hostgroup/aims.yaml) with the new release image.
* Update the webpage [https://gitlab.cern.ch/linuxsupport/websites/linux.cern.ch](https://gitlab.cern.ch/linuxsupport/websites/linux.cern.ch)
* Send email template to users informing that CentOS7.$RELEASE is available for testing. The recipient should be 'linux-users@cern.ch' and the following recipients should be included in BCC 'linux-announce@cern.ch', 'linux-announce-test@cern.ch', 'ai-admins@cern.ch'
    * You may use `/mnt/data2/dist/cern/centos/7.$RELEASE/os/x86_64/CERN/build/release-email-test.sh` to generate the text
    * Note: this script does NOT send the email

### Release to production

* Remove `/mnt/data1/dist/cern/centos/7.$RELEASE/.htaccess`
* Change the symlink `/mnt/data2/dist/cern/centos/7` to point to `7.$RELEASE`
```
ln -sfn /mnt/data2/dist/cern/centos/7.$RELEASE.$YEARMONTH/ /mnt/data2/dist/cern/centos/7
```
* Update the `CENTOS_VERSION` string in `~/bin/config.sh`
* Add centos-release, afs (both rpm and src rpm) to cern repo (bypassing cern-testing)
* Perform any clean up in 'extras' or 'updates' (any deprecated package removal from upstream)
* rsync sources from vault:
```
mkdir -p /mnt/data2/dist/cern/centos/7.$RELEASE/os/Source/SPackages
cd /mnt/data2/dist/cern/centos/7.$RELEASE/os
ln -s Source Sources
cd Source
rsync -avP rsync://mirror.nsc.liu.se/centos-store/7.$RELEASE/os/Source/ /mnt/data2/dist/cern/centos/7.$RELEASE/os/Source/
```
* Edit and run `/mnt/data2/dist/cern/centos/7.$RELEASE/os/x86_64/CERN/build/sync.sh` to populate `/mnt/data1/dist/cern/centos/7.$RELEASE/os/Source`
* rsync `/mnt/data2/dist/cern/centos/7.$release/os/x86_64/Packages` to `/mnt/data2/dist/cern/centos/7/updates/x86_64/Packages/` (this is needed for koji dependencies)
* regen repo for cern, cr, cr-testing, updates (and extras if touched)
```
/mnt/data2/home/build/bin/bsregenrepos -d cc7 -r "cern cr cr-testing updates"
```
* sync to data1 `/mnt/data2/home/build/bin/bssync -t "data1" -d cc7 -r "cern cr cr-testing updates"`
* Promote the TEST VM to Production using the [koji-image-build monthly scheduled pipeline](https://gitlab.cern.ch/linuxsupport/koji-image-build/-/pipeline_schedules) you created previously
* Change the symlink `/mnt/data1/dist/cern/centos/7` to point to `7.$release`:
```
ln -sfn /mnt/data1/dist/cern/centos/7.$RELEASE.$YEARMONTH/ /mnt/data1/dist/cern/centos/7
```
* Remove the `$RELEASE TEST` target on aims, add $RELEASE target on aims
* Update [AIMS hostgroup Hiera data](https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/-/blob/qa/data/hostgroup/aims.yaml), removing reference to TEST and promoting $release as default (you can use `/mnt/data2/dist/cern/centos/7.$release/os/x86_64/CERN/build/aims2.sh`). You might want to update the `BE/CO` dedicated menu as well.
* Promote AIMS hostgroup `qa` branch to `master`
* Test the PXE installation works by using the default CC7 entry and seeing $RELEASE on the installed host
* Create Docker image and upload it to gitlab registry and dockerhub, which is done in: `https://gitlab.cern.ch/linuxsupport/cc7-base/`
* Update the webpage [https://gitlab.cern.ch/linuxsupport/websites/linux.cern.ch](https://gitlab.cern.ch/linuxsupport/websites/linux.cern.ch)
* Add an entry to the ITSSB (the following can be used an example [OTG0052544](https://cern.service-now.com/service-portal?id=outage&n=OTG0052544)
* Send email template to users informing that CentOS7.$release is available in production. The recipient should be 'linux-users@cern.ch' and the following recipients should be included in BCC 'linux-announce@cern.ch', 'linux-announce-test@cern.ch', 'ai-admins@cern.ch'
    * You may use `/mnt/data2/dist/cern/centos/7.$release/os/x86_64/CERN/build/release-email-prod.sh` to generate the text
    * Note: this script does NOT send the email
* Create `7.$RELEASE` under `/eos/project/l/linux/cern/centos/`
* Sync to EOS:
```
/mnt/data2/home/build/bin/bssync -t "eos" -d cc7 -r "all"
```
This takes a long time. There might be quota issues, so several runs might be needed.
