# Almalinux and Red Hat Enterprise Linux

These distributions are managed with the new "snapshot" model. This general information
applies to all of them, with specific changes for each one being listed separately.

## End-of-Life dates

| Distribution | End of Support | Notes |
|--------------|----------------|-------|
| CentOS Stream 8 | 2024-03-31 | "Public" date stated in [OTG0074647](https://cern.service-now.com/service-portal?id=outage&n=OTG0074647) is 2023-09-30, but we [agreed with Alice](https://indico.cern.ch/event/1247021/) to keep it alive until 2024-03-31. Upstream EoS is 2024-05-31. |
| CentOS Stream 9 | 2023-06-30 | [OTG0074647](https://cern.service-now.com/service-portal?id=outage&n=OTG0074647). Upstream EoS is 2027-05-31. |
| AlmaLinux 8 | 2029-05-31 | |
| AlmaLinux 9 | 2032-05-31 | |
| RHEL 8 | 2029-05-31 | End of Full Support 2024-05-31, End of Maintenance 2029-05-31, ELS 2032-05-31. [source](https://access.redhat.com/support/policy/updates/errata#Life_Cycle_Dates) |
| RHEL 9 | 2032-05-31 | End of Full Support 2027-05-31, End of Maintenance 2032-05-31, ELS 2035-05-31. [source](https://access.redhat.com/support/policy/updates/errata#Life_Cycle_Dates) |


## Snapshot Creation

Snapshot creation is handled by various Nomad jobs ([CS8](https://gitlab.cern.ch/linuxsupport/cronjobs/stream8_snapshots/), [AlmaLinux](https://gitlab.cern.ch/linuxsupport/cronjobs/alma_snapshots/), [RHEL](https://gitlab.cern.ch/linuxsupport/cronjobs/rhel_snapshots/)).

These jobs run automatically every day at 8am. They create daily snapshot directories
in `{centos,alma,rhel}/*-snapshots/` and maintain the symlinks from `{8,9,s8}` (ie. prod),
`{8,9,s8}-testing`, etc. to the appropriate daily snapshot. The `-testing` symlinks
point to that day's snapshot (see note), and the production (ie. non -testing)
ones point to the previous Wednesday's snapshot. Production symlinks are changed on
Wednesdays and ensure packages spend at least 7 days and at most 13 days in testing.

!!! note ""
    Symlinks are not modified on weekends or between December 16th and January 6th.
    Snapshots will still be created, though.


### Stopping modifications to symlinks

Changes to symlinks can be manually stopped by creating `.freeze.*` files in
`/mnt/data1/dist/cern/{alma,rhel}/`. You can create the file `.freeze.*all` to stop
updating all symlinks, or create specific ones for each symlink (ie. `.freeze.9`,
`.freeze.alma9-testing`, etc.).

A `.freeze.*all` may be created automatically in certain conditions, such as a
validation failure, detection of a "dangerous" RPM (see below), etc. When this
happens, an email will be sent to `lxsoft-admin` explaining the cause. You will
have to take manual action to resolve the situation and remove the `.freeze.*all`
so the process can continue.

| Distribution | Freeze file name                            |
|--------------|---------------------------------------------|
| ALMA8        | `/mnt/data1/dist/cern/alma/.freeze.8all`    |
| ALMA9        | `/mnt/data1/dist/cern/alma/.freeze.9all`    |
| RHEL8        | `/mnt/data1/dist/cern/rhel/.freeze.8all`    |
| RHEL9        | `/mnt/data1/dist/cern/rhel/.freeze.9all`    |


### *-release packages: the "dangerous" RPMs

The "dangerous" RPMs are generally `*-release` packages, the list is defined in `config.sh` in the Nomad job.
These packages include repo files that point to upstream repos, so we must rebuild them to point to our repos.
This is not done for all `*-release` RPMs, but only for a subset of them for which we create snapshots.

We store our new packages in <https://gitlab.cern.ch/linuxsupport/rpms/releases> ([`almalinux-release`](https://gitlab.cern.ch/linuxsupport/rpms/releases/almalinux-release), [`redhat-release`](https://gitlab.cern.ch/linuxsupport/rpms/releases/redhat-release), [`epel-release`](https://gitlab.cern.ch/linuxsupport/rpms/releases/epel-release)).
Add them there if at some point you need to add a new rebuilt package.

As a general rule, when creating a new one, you will need to change the repo file(s) to use
the corresponding URL from CERN and add the GPG keys. You will also need to add a high epoch
to make sure CERN's RPM has priority. Use `almalinux-release` as an example. The source code of
the upstream packages can be found in the [AlmaLinux's git repos](https://git.almalinux.org/explore/repos).

Most of the time, you will simply have to update one of the existing packages. Take a look at the
source RPM for the new upstream version (which you can find on our mirror) and compare it with our
version. Copy the changes from the upstream package into ours, adapting repository paths, etc.
You can check previous commits to see examples.

You can also use [autopatch script](https://gitlab.cern.ch/morrice/autopatch) that will suggest the changes that you need to apply. 

Once the new version of a release package is built and tagged as stable, it will be picked up
the next time the Nomad job runs. If you were proactive enough and did this the day you got
a notification in [~lxsoft-alerts](https://mattermost.web.cern.ch/it-dep/channels/lxsoft-alerts),
then you're awesome and can go take a break. If instead you dropped the ball and waited until you
received the *"Dangerous packages detected: automation stopped"* email to `lxsoft-admins`, then
you should feel shame and bring croissants for everyone. You will also have to delete the
`.freeze.*all` file and rerun the Nomad job. This time, the job will find your new RPM in the
CERN repo and continue the process.

An example of how to apply this procedure for the case of "dangerous" `almalinux-release-8.9-1.el8.aarch64.rpm` and `almalinux-release-8.9-1.el8.x86_64.rpm` packages detected, would include the following steps:

1. Check the content of  [`almalinux-release` for the `a8` branch](https://gitlab.cern.ch/linuxsupport/rpms/releases/almalinux-release/-/tree/a8) and compare it with that of [AlmaLinux's git repo for `almalinux-release`](https://git.almalinux.org/rpms/almalinux-release)
2. In this case, the GPG Key ([`RPM-GPG-KEY-AlmaLinux`](https://git.almalinux.org/rpms/almalinux-release/src/branch/a8/RPM-GPG-KEY-AlmaLinux)) has not changed and is identical to the already existing one ([`src/RPM-GPG-KEY-AlmaLinux`](https://gitlab.cern.ch/linuxsupport/rpms/releases/almalinux-release/-/blob/a8/src/RPM-GPG-KEY-AlmaLinux))
3. Only a few changes should be made to [`almalinux-release.spec`](https://gitlab.cern.ch/linuxsupport/rpms/releases/almalinux-release/-/blob/a8/almalinux-release.spec) to align with the content of the [corresponding `almalinux-release.spec` in the `almalinux-release`](https://git.almalinux.org/rpms/almalinux-release/src/branch/a8/almalinux-release.spec)
4. Create a Merge Request (MR) with the corresponding changes. Use a new branch `release8.9-1` since the concerned release is `8.9-1`, as listed in the name of the "dangerous" packages.
```
% git clone https://gitlab.cern.ch/linuxsupport/rpms/releases/almalinux-release
% cd almalinux-release
% git checkout a8
% git checkout -b release8.9-1
% git add almalinux-release.spec  # with the modifications mentioned above
% git commit -m "release8.9-1: .spec changed. GPG-KEY has not changed"
% git push origin release8.9-1
```
The last command (`git push origin release8.9-1`) will provide a URL that can be used to create the corresponding MR. Ask other members of the Linux Team to review and merge that MR.

5. Create a new tag `v8.9-1` at [`https://gitlab.cern.ch/linuxsupport/rpms/releases/almalinux-release/-/tags/`](https://gitlab.cern.ch/linuxsupport/rpms/releases/almalinux-release/-/tags/) so that the production pipeline runs, and wait for it to conclude.
6. Tag the build to `qa` at the corresponding job listed at [`https://gitlab.cern.ch/linuxsupport/rpms/releases/almalinux-release/-/jobs`](https://gitlab.cern.ch/linuxsupport/rpms/releases/almalinux-release/-/jobs), by clicking on `Run job` (leave empty the `Key` and `Value` fields in the form).
7. Tag the build to `stable` at the corresponding job listed at [`https://gitlab.cern.ch/linuxsupport/rpms/releases/almalinux-release/-/jobs`](https://gitlab.cern.ch/linuxsupport/rpms/releases/almalinux-release/-/jobs), by clicking on `Run job` (leave empty the `Key` and `Value` fields in the form).
8. If the `.freeze.*all` file was created because the snapshot was halted due to a lack of proactive action, delete it. In this instance, the file to be deleted is `lxsoftadm:/mnt/data1/dist/cern/alma/.freeze.8all`.
9. Re-launch the snapshot, in this case for `alma8`, by clicking on `Force launch` at [`https://lxsoftadm.cern.ch:4646/ui/jobs/prod_alma8_snapshots@default?desc=true&sort=submitTime`](https://lxsoftadm.cern.ch:4646/ui/jobs/prod_alma8_snapshots@default?desc=true&sort=submitTime)

### Highlight packages
The highlight just means that it will be presented more prominently in the subject of the email. 
The only thing special about kernels is that there is machinery to automatically build the kmod-openafs module.
- [linux loves afs](https://mattermost.web.cern.ch/it-dep/channels/linux-afs) boot will send a message about the kmod
- The test email will mention the kernel and kmod

### How to check if a CVE has been fixed
A better way to check if a CVE has been fixed is to use `rpm -q --changelog <package>` and read/grep the content.

### Seeing differences between snapshots

You can see all the packages that changed between two dates using the `snapshotdiff`
command, which is provided by [lxdist-build](https://gitlab.cern.ch/linuxsupport/lxdist-build/-/tree/master/bin):

```bash
[build@lxsoftadm01] snapshotdiff -d s8 -s 20200701 -e 20200709
Calculating diff between 20200701 and 20200709...

  apache-sshd                                        2.5.1-1.el8
  apache-sshd-javadoc                                2.5.1-1.el8
  bind                                               9.11.13-5.el8_2
...
  python-novaclient-doc                              15.1.1-1.el8
  python-octaviaclient-doc                           1.10.1-1.el8
  python3-bind                                       9.11.13-5.el8_2

Changes listed: 75
Total number of changes: 176
```

### Manually updating symlinks

!!! danger ""
    This should only be done when you have a **VERY** good reason to do so, such as an
    emergency security update. Updating symlinks will reduce the time some packages spend
    in QA. If you don't understand the implications of this, **DO NOT DO IT**.

You can also manually point symlinks to a more up-to-date snapshot. The symlink
will not be moved backwards in time, but it will move forward again as necessary.

For instance, on Wednesday the 8th, the production symlinks would point to Wednesday
the 1st. On Thursday the 9th an emergency security patch is released and you manually
modify the production symlinks to point to that Thursday's snapshot. The links will
continue pointing to that day even on Wednesday the 15th, where they should
been moved to the 8th but won't be because they're never moved backwards in time.
On Wednesday the 22nd they would be moved to Wednesday the 15th as usual.

Before doing this, it is a good idea to check what packages have changed
between the different snapshots using the `snapshotdiff` command.

The recommended method of updating the links is to ssh into an lxsoftadm machine
and run:

```bash
# cd /mnt/data1/dist/cern/centos
# echo "20200709" > .forcemove.s8
```

Once that file is created, rerun the cronjob in Nomad. It will see
this file, set the production symlinks to that date, and then delete the `.forcemove.*`
file. The job will also send the usual emails and update the web site.

| Distribution | Force-move file name                        |
|--------------|---------------------------------------------|
| CS8          | `/mnt/data1/dist/cern/centos/.forcemove.s8` |
| ALMA8        | `/mnt/data1/dist/cern/alma/.forcemove.8`    |
| ALMA9        | `/mnt/data1/dist/cern/alma/.forcemove.9`    |
| RHEL8        | `/mnt/data1/dist/cern/rhel/.forcemove.8`    |
| RHEL9        | `/mnt/data1/dist/cern/rhel/.forcemove.9`    |

### Scheduling update of symlinks

!!! danger ""
    The same caveat [as above](#manually-updating-symlinks) applies.

You can schedule the manual updating of symlinks for a later date by creating a special
forcemove with a date, with the following format: `.forcemove.YYYYMMDD.*`. The contents of the
file would be the same [as the manual method](#manually-updating-symlinks). This feature
is useful, for example, when you're asked to release a specific package on a specific
date.

For example, today is 2023-07-12 and there's a new kernel in testing that fixes
a critical CVE. You've been asked to make sure that the new kernel is released to production on
Monday the 17th, rather than waiting for its full testing time in QA. You could wait until the 19th
and then create a `.forcemove.9` file, but then you'd have to work on Sunday, which sucks.
With this feature, you could create a `.forcemove.20230717.9` file on the 12th and forget about
this until then. Once the snapshot runs on the 17th, the forcemove file will be taken into account,
the symlink will be updated and the new kernel will be released to production. You'll look like
a hero and you don't actually have to touch a computer over the weekend!

Note that a `.forcemove.*` file will still take precedence over a `.forcemove.YYYYMMDD.*` for that
date. That is to say, if on the 20th there's a `.forcemove.20230720.*` *and* a `.forcemove.*`, the
`.forcemove.*` will be applied and `.forcemove.20230720.*` will be deleted.
