# Services

## Official mirrors (unmodified / open to the outside)

Official mirrors and provided for everybody that can access linuxsoft.cern.ch.

Check the [rsync](https://gitlab.cern.ch/linuxsupport/cronjobs/rsync) repository

This repository is responsible for the mirroring of:

* centos / centos-debuginfo / centos-vault
* stream
* epel
* fedora
* elrepo
* repoforge

## Internal mirrors

These mirrors are for internal use and not advertised to the outside. They are accessible via [http://linuxsoft.cern.ch/mirror/](http://linuxsoft.cern.ch/mirror/) 
We do not have obligation to maintain them online for a third party.
However they may be available to external users and labs (xbatch, other institute, etc...).
Some of them are enabled by default in the CERN distributions.

Check the [reposync](https://gitlab.cern.ch/linuxsupport/cronjobs/reposync) repository for the definition of all /mirror repositories

Note - we also use the reposync repo to synchronise content from RedHat, which can be found at [http://linuxsoft.cern.ch/cdn.redhat.com](http://linuxsoft.cern.ch/cdn.redhat.com) 
