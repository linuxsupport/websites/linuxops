# What is your job ?

The Linux team is in charge of providing a supported Linux distribution for CERN, and the WLCG (Worldwide LHC Computing Grid).

Starting with RHEL 7, CERN decided to join the CentOS community and help them rather than duplicating the work for rebuilding the same sources from Red Hat. CC7 (CERN CentOS 7) is just a simple respin of CentOS 7 with additional CERN repositories and staged updates.

Alma Linux 8/9, CS8 (CentOS Stream 8) follow upstream as closely as possible, with automated daily snapshots and minimal rebuilds.

Our main method of communication as a team is via the Mattermost channel [~lxsoft-admins](https://mattermost.web.cern.ch/it-dep/channels/lxsoft-admins). As a member of the team, **you are expected to follow that channel closely**. There's not a lot of small talk there, generally we're discussing how to solve a particular issue. Even if you're not involved in that specific issue, reading the messages helps us all learn from one another and stay up-to-date with the state of the services we run.

## Meetings

You need to attend the incredibly useful [Ticket Review Meeting](https://indico.cern.ch/category/15435/) which happens every Thu at 9:30 AM.

## Distributions

The main tasks as a release manager are :

- Keep an eye on the automation for [CC7](https://gitlab.cern.ch/linuxsupport/cronjobs/centos7_release), [CS8](https://gitlab.cern.ch/linuxsupport/cronjobs/stream8_snapshots) and [AlmaLinux](https://gitlab.cern.ch/linuxsupport/cronjobs/alma_snapshots). Every day, several emails will be sent to `lxsoft-admins` notifying you of the progress, and the full logs can also be seen in [Kibana](https://es-linux.cern.ch/?security_tenant=internal).
    - Should the automation fail or stop for any reason (new packages that need to be rebuilt, etc.), your job is to make sure the situation is corrected and the automation can continue (either that day, by rerunning the cronjob, or the next).
    - For CC7, production updates should happen on Thursday morning, and for CS8 and Alma8/9 they should happen on Wednesday morning.
    - Manually, these updates may be stopped or done earlier depending on issues in the upstream packages or urgent security fixes. Always check with the rest of the team before doing so.
- Stay up-to-date with the latest Red Hat and CentOS updates. Notifications for particularly interesting packages [will be sent](https://gitlab.cern.ch/linuxsupport/cronjobs/centos_rss) to the Mattermost channel [~lxsoft-alerts](https://mattermost.web.cern.ch/it-dep/channels/lxsoft-alerts), but from time to time there will be other interesting updates that are not [on the list](https://gitlab.cern.ch/linuxsupport/cronjobs/centos_rss/-/blob/master/centos_rss/prod.packages.yml).
    - Notifications about ["dangerous packages"](https://linuxops.web.cern.ch/distributions/cs8/#-release-packages-the-dangerous-rpms) need to be acted upon as soon as possible. Not doing so incurrs a debt of croissants for the rest of the team.
    - This also includes keeping track of new Red Hat point releases and [publishing them](../support/redhat.md).
- Once a month on the first of the month, new [Openstack](https://gitlab.cern.ch/linuxsupport/koji-image-build) and Docker images ([CC7](https://gitlab.cern.ch/linuxsupport/cc7-base), [CS8](https://gitlab.cern.ch/linuxsupport/cs8-base), [Alma9](https://gitlab.cern.ch/linuxsupport/alma9-base), [Alma8](https://gitlab.cern.ch/linuxsupport/alma8-base)) will be created a Gitlab scheduled jobs. Make sure the process works and all the tests pass. Promote the resulting images to production.
- All alerts need to be investigated and understood/fixed in a timely manner by the rota person. The rota person should also notify the rest of the team of the root cause and solution for the alerts.

## Services

The main tasks as a service manager are :

- Maintain and improve main distribution server (linuxsoft.cern.ch).
- Maintain and improve installation infrastructure servers (aims.cern.ch).
- Maintain and improve RPM build service (koji.cern.ch).
- Maintain and improve our [testing tools](https://gitlab.cern.ch/linuxsupport/testing).
- Maintain and improve [internal tools](https://gitlab.cern.ch/linuxsupport) used for integration with CERN IT.
    - This includes the [cronjobs](https://gitlab.cern.ch/linuxsupport/cronjobs) that power all our automation.

## User Support

The main support tasks are :

- Pay attention to any incoming tickets for both `Linux Software Building 3th Line Support` and `Linux Support 3th Line Support`. Try to fix them as long as you can handle them. Otherwise, talk to other experts to find a solution together.
- Manage second line support, their job is to execute procedure you provided.
- Train first line and second line support. (It has not been done in few years, due to workload)
- Write documentation for users. Main linux website and SNOW knowledge base articles are used.
- Help IT service managers with recommendation about what software can be used. Mainly done with puppet team and shared puppet modules.

## 2nd Level

The 2nd Level team will be sending tickets your way. You might want to check out their instructions as there may be things
we could add there to reduce the number of tickets we get:

[https://it-dep-cd-docs.web.cern.ch/trm/docfor2ndlevel.html](https://it-dep-cd-docs.web.cern.ch/trm/docfor2ndlevel.html)
