# Service Alarms

On a given machine, you can see which collectd alarms are in failure state like this:

```bash
$ collectdctl listval state=failure
```

### Systemctl to Manage Systemd Services and Units:
```bash
systemctl status
systemctl --state=failed
journalctl -xe -u <service_name>
```

## certificate_validity

`lxsoft` and `lsb` (Koji) have Sectigo certificates, which are trusted by all clients. These
certificates have a validity of 1 year and they need to be requested to IT-PW / Andreas Wagner.

For reference, the last request for Koji certificates was [RQF2840607](https://cern.service-now.com/service-portal?id=ticket&table=u_request_fulfillment&n=RQF2840607) and for lxsoft certificates was [RQF3013533](https://cern.service-now.com/service-portal?id=ticket&n=RQF3013533).

> **_NOTE:_** The lxsoft certificate is valid until 22 Feb 2026. 

After the certificate is issued, it can be updated using the corresponding gitlab repo of each hostgroup. e.g.:

   * For lxsoft [here](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxsoft). Example update for [lxsoft](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxsoft/-/commit/ada0d196be941ec1a344493171e2a436ebdfcb1d).
   * For koji [here](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsb). Example update for [lsb](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsb/-/commit/8afd6c9792cf34888f015aea3681dd5bd1d3a21b).

After that, run puppet and restart collectd on each lxsoft/koji web node.


## freeze

| Distribution | Freeze alarm    |
|--------------|-----------------|
| CS8          | freeze-stream8  |
| ALMA8        | freeze-alma8    |
| ALMA9        | freeze-alma9    |
| RHEL8        | freeze-rhel8    |
| RHEL9        | freeze-rhel9    |

These alarms indicates that the respective snapshot process can't update the symlinks because
there's a `.freeze.*all` file. You should also have received an email explaining why the
process was stopped and what you need to do to resolve it.

## notupdated

| Distribution | notupdated alarm    | Snapshot Nomad jobs                                              | ES dashboard                                                                                                       |
|--------------|---------------------|------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
| CS8          | notupdated-stream8  | https://gitlab.cern.ch/linuxsupport/cronjobs/stream8_snapshots/  | https://es-linux.cern.ch/kibana/app/dashboards?security_tenant=internal#/view/6b8102c0-51cb-11eb-932f-51687e53f66a |
| ALMA8        | notupdated-alma8    | https://gitlab.cern.ch/linuxsupport/cronjobs/alma_snapshots/     | https://es-linux.cern.ch/dashboards/goto/1bb9a3e765f7e99aa77fcbaf6f1994e5?security_tenant=internal                 |
| ALMA9        | notupdated-alma9    | https://gitlab.cern.ch/linuxsupport/cronjobs/alma_snapshots/     | https://es-linux.cern.ch/dashboards/goto/18a216d237182fc2b3529a17468dfa39?security_tenant=internal                 |
| RHEL8        | notupdated-rhel8    | https://gitlab.cern.ch/linuxsupport/cronjobs/rhel_snapshots/     | https://es-linux.cern.ch/dashboards/goto/9bff1a346435c88c080cc492fe34a276?security_tenant=internal                 |
| RHEL9        | notupdated-rhel9    | https://gitlab.cern.ch/linuxsupport/cronjobs/rhel_snapshots/     | https://es-linux.cern.ch/dashboards/goto/78ef38cf73cb832b6fa7f15c756869d7?security_tenant=internal                 |

These alarms are triggered when the `.{8/9}-latest` (or `.s8-latest`) symlink hasn't been updated in over 30 hours.
This symlink is always supposed to point to the latest snapshot and is supposed to be updated
every day. If it hasn't been updated, chances are something is wrong with the snapshot Nomad job.
Check the logs in the ES dashboard.


## oldtmp

!!! danger ""
    Before deleting _anything_, make sure you know what you're doing. If in doubt, double-check with the rest of the team.

These alarms indicate that there are old `.tmp.*` directories in `/mnt/data1/dist/cern/{dist}/{version}-snapshots/`.
Those directories are created when the snapshot is running, but they are renamed at the end of the process.
If there are directories left over, it means something interrupted that day's snapshot and needs to be investigated.
If the snapshots are currently failing, don't delete today's `.tmp.*` snapshot, and **never** delete the `.*-latest` symlink
(or any symlink, for that matter). The latest symlink should always exist and point to something.

| Distribution | old alarm       | dist   | version |
|--------------|-----------------|--------|---------|
| CS8          | oldtmp-stream8  | centos | s8      |
| ALMA8        | oldtmp-alma8    | alma   | 8       |
| ALMA9        | oldtmp-alma9    | alma   | 9       |
| RHEL8        | oldtmp-rhel8    | rhel   | 8       |
| RHEL9        | oldtmp-rhel9    | rhel   | 9       |

## repo_wrong

This alarm indicates an issue with the named repo, as indicated by `/usr/bin/repoquery --repofrompath=<repoid>,<repopath> -qa`.
Hopefully it will be resolved by rerunning the mirroring script.

## nomad_health-cluster

This alarm indicates that something might be wrong with the [Nomad cluster quorum](../nomad/troubleshooting.md).

## nomad_health-afs

This alarm indicates an issue with the AFS mount on a Nomad node. The Nomad client should now be ineligible for new jobs.
Fix the cause of the AFS mount error and the node should mark itself as eligible again.

## nomad_health-data1

This alarm indicates an issue with the data1 CephFS mount on a Nomad node. The Nomad client should now be ineligible for new jobs.
Fix the cause of the mount error and the node should mark itself as eligible again.

## nomad_health-data2

This alarm indicates an issue with the data2 CephFS mount on a Nomad node. The Nomad client should now be ineligible for new jobs.
Fix the cause of the mount error and the node should mark itself as eligible again.

## no_contact linuxsoft-probes

These probes are configured in [monit-remote-probes](https://gitlab.cern.ch/monitoring/monit-remote-probes/-/blob/master/linuxsoft/probes.yaml),
they were created in [LOS-904](https://its.cern.ch/jira/browse/LOS-904). The [dashboard](https://monit-grafana.cern.ch/d/w57luRvnz/remote-probes-overview?orgId=1&var-producer=linuxsoft&var-module=All&var-instance=All) will show you the current status.
At the moment (Apr. 2022) there's no good way to get logs of failures to help with the debugging. You can find logs of the
most recent probes at [http://monit-remote.cern.ch/blackbox](http://monit-remote.cern.ch/blackbox), but they're not kept around for long. Another thing you can do
is manually trigger a probe by visiting [http://monit-remote.cern.ch/blackbox/probe?module=linuxsoft/http_ipv4_koji&target=http%3A%2F%2Fkoji22.cern.ch&debug=true](http://monit-remote.cern.ch/blackbox/probe?module=linuxsoft/http_ipv4_koji&target=http%3A%2F%2Fkoji22.cern.ch&debug=true). You can change the module and target parameters of that URL to trigger different probes.
These magical URLs were given to us by Borja Garrido and they may stop working at any time as they're not an "official" service interface.
