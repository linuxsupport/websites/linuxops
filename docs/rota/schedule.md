# Linux Team Rota

The Linux Team Rota is now managed with [Op-Webtools Planning](https://op-webtools.web.cern.ch/planning/#/system/it:cd:cli:linux/group/4651).

Go there to see when you're on rota. While there, you can also [configure](https://op-webtools.web.cern.ch/preferences)
email alerts and get a link to a calendar you can import into your favorite calendar app. [Shiftie](https://gitlab.cern.ch/linuxsupport/cronjobs/rota_alerter),
our helpful rota bot, will also send a message to [~lxsoft-admins](https://mattermost.web.cern.ch/it-dep/channels/lxsoft-admins) to notify
us when a new rota starts.

If you wish to switch a rota week with another team member, please coordinate with that person and make the modification directly in Op-Tools Planning.

The rota starts on Friday at 17:30 and finishes on the next Friday at the same hour.

## Previous schedules

### 2023 Schedule

| Who?     | When?                         |
| -------- | ----------------------------- |
| Alex     | 31 Dec to 06 Jan              |
| Ben      | 07 Jan to 13 Jan              |
| Georgios | 14 Jan to 20 Jan              |
| Marta    | 21 Jan to 27 Jan              |
| Manuel   | 28 Jan to 03 Feb              |
| Marta    | 04 Feb to 10 Feb              |
| Ben      | 11 Feb to 17 Feb              |
| Georgios | 18 Feb to 24 Feb              |
| Marta    | 25 Feb to 03 Mar              |
| Manuel   | 04 Mar to 10 Mar              |
| Ben      | 11 Mar to 17 Mar              |
| Alex     | 18 Mar to 24 Mar              |
| Georgios | 25 Mar to 31 Mar              |
| Marta    | 01 Apr to 07 Apr              |
| Manuel   | 08 Apr to 14 Apr              |
| Alex     | 15 Apr to 21 Apr              |
| Manuel   | 22 Apr to 28 Apr              |
| Georgios | 29 Apr to 05 May              |
| Marta    | 06 May to 12 May              |
| Manuel   | 13 May to 19 May              |
| Georgios | 20 May to 26 May              |
| Ben      | 27 May to 02 Jun              |
| Alex     | 03 Jun to 09 Jun              |
| Ben      | 10 Jun to 16 Jun              |
| Manuel   | 17 Jun to 23 Jun              |
| Alex     | 24 Jun to 30 Jun              |
| Marta    | 01 Jul to 07 Jul              |
| Georgios | 08 Jul to 14 Jul              |
| Marta    | 15 Jul to 21 Jul              |
| Manuel   | 22 Jul to 28 Jul              |
| Alex     | 29 Jul to 04 Aug              |
| Marta    | 05 Aug to 11 Aug              |
| Georgios | 12 Aug to 17 Aug (18 Aug Ben) |
| Ben      | 19 Aug to 25 Aug              |
| Alex     | 26 Aug to 01 Sep              |
| Manuel   | 02 Sep to 08 Sep              |
| Ben      | 09 Sep to 15 Sep              |
| Marta    | 16 Sep to 22 Sep              |
| Manuel   | 23 Sep to 29 Sep              |
| Alex     | 30 Sep to 06 Oct              |
| Ben      | 07 Oct to 13 Oct              |
| Marta    | 14 Oct to 20 Oct              |
| Manuel   | 21 Oct to 27 Oct              |
| Alex     | 28 Oct to 03 Nov              |
| Ben      | 04 Nov to 10 Nov              |
| Marta    | 11 Nov to 17 Nov              |
| Manuel   | 18 Nov to 24 Nov              |
| Alex     | 25 Nov to 01 Dec              |
| Ben      | 02 Dec to 08 Dec              |
| Marta    | 09 Dec to 15 Dec              |
| Manuel   | 16 Dec to 22 Dec              |
| Alex     | 23 Dec to 29 Dec              |
| Ben      | 30 Dec to 05 Jan              |


### 2022 Schedule

| Who?                                  | When?                              |
| ------------------------------------- | ---------------------------------- |
| Georgios                              | 30 Apr to 06 May                   |
| Alex                                  | 07 May to 13 May                   |
| Ben (14-18), Georgios (19), Alex (20) | 14 May to 20 May                   |
| Daniel                                | 21 May to 27 May                   |
| Alex                                  | 28 May to 03 Jun                   |
| Georgios                              | 04 Jun to 10 Jun                   |
| Daniel                                | 11 Jun to 17 Jun                   |
| Manuel                                | 18 Jun to 24 Jun                   |
| Ben                                   | 25 Jun to 01 Jul                   |
| Daniel                                | 02 Jul to 07 Jul (08 Jul Georgios) |
| Manuel                                | 09 Jul to 15 Jul                   |
| Alex                                  | 16 Jul to 22 Jul                   |
| Manuel                                | 23 Jul to 29 Jul                   |
| Georgios                              | 30 Jul to 04 Aug (05 Aug Ben)      |
| Ben                                   | 06 Aug to 12 Aug                   |
| Ben                                   | 13 Aug to 19 Aug                   |
| Alex                                  | 20 Aug to 26 Aug                   |
| Georgios                              | 27 Aug to 02 Sep                   |
| Manuel                                | 03 Sep to 09 Sep                   |
| Manuel                                | 10 Sep to 16 Sep                   |
| Alex                                  | 17 Sep to 23 Sep                   |
| Georgios                              | 24 Sep to 30 Sep                   |
| Alex                                  | 01 Oct to 07 Oct                   |
| Marta                                 | 08 Oct to 14 Oct                   |
| Ben                                   | 15 Oct to 21 Oct                   |
| Marta                                 | 22 Oct to 28 Oct                   |
| Ben                                   | 29 Oct to 04 Nov                   |
| Georgios                              | 05 Nov to 11 Nov                   |
| Alex                                  | 12 Nov to 18 Nov                   |
| Georgios                              | 19 Nov to 25 Nov                   |
| Marta                                 | 26 Nov to 02 Dec                   |
| Marta                                 | 03 Dec to 08 Dec                   |
| Manuel                                | 09 Dec to 16 Dec                   |
| Manuel                                | 17 Dec to 23 Dec                   |
| Ben                                   | 24 Dec to 30 Dec                   |
| Alex                                  | 31 Dec to 06 Jan                   |
