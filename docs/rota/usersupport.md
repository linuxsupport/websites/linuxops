## Access CERN Linux user's machines

The `cern-linuxsupport-access` is a tool that can be used to enable/disable root access to the machine by CERN Linux Support personnel.

```bash
$ yum install cern-linuxsupport-access
$ cern-linuxsupport-access enable
```

## How to create a CERN account on a private Linux machine

First check if the user account is already defined in the machine:

```bash
less /etc/passwd
getent passwd <username>
```

If it is not:
1) Check if `locmap` is installed and enabled.
2) Follow the [article](https://cern.service-now.com/service-portal?id=kb_article&n=KB0000270) to give access to the user with his/her username: `useraddcern <username>`

## How to have the home directory for the <username> user to be /afs and not /home
This can be changed by running: `useraddcern -u -d <username>`

## The /afs access
 If the user has afs configured via locmap in the machine, then locmap module deploys the `cern-aklog-systemd-user` which contains a systemd user script to periodically refresh both the kerberos and afs tokens for the user. It's the same configuration as lxplus.
 The user can check `systemctl --user list-timers |grep aklog`.
 If afs is well configured via locmap and the user is authenticating with kerberos or password, the user should not need to kinit. 
 If they use ssh keys, they will still need to kinit.

## Error using the ssh keys

If a user reports a error trying to ssh to a machine with the username using the ssh keys and the error is `"Could not open authorized keys '/afs/cern.ch/user/.ssh/authorized_keys2': No such file or directory"`, the issue is that `.ssh` resides on `/afs` and sshd cannot read the authorized_keys directory as `.ssh` is owned by the user.

The fix is to move the `authorized_keys` file to a location in public and then use a symlink from:
1) Access `lxplus`
2) `cd public/`
3) `mkdir ssh`
4) `mv ~/.ssh/authorized_keys .`
5) `ln -s /afs/cern.ch/user/<username>/public/authorized_keys /afs/cern.ch/user/<username>/.ssh/authorized_keys`
6) `ls -la ~/.ssh`

To confirm from the logfile, access the machine as root and `grep "Accepted publickey for <username>" /var/log/secure | tail -1`.

## Generating a user keytab 

To generate keytab files you should use `cern-get-keytab`. More info in the [article](https://cern.service-now.com/service-portal?id=kb_article&n=KB0003405) :

```bash
cern-get-keytab -u -k <keytabfile> -l <account>
kdestroy
kinit -k -t <keytabfile> <account>@CERN.CH
klist
```

## What to do when kvno is different from krb5.keytab

Check the data as root:

```
[root@<machine_name> ~]# kvno <machine_name>
[root@<machine_name> ~]# klist -k /etc/krb5.keytab
```

Follow the steps:
1) `mv /etc/krb5.keytab /etc/krb5.keytab.$(date +%Y%m%d)` or similar rather than deleting the keytab
2) Remove `/etc/krb5.keytab`
3) Run `cern-get-keytab`

More information in <https://linux.web.cern.ch/docs/kerberos-access/#server-side-configuration-and-troubleshooting>.

## IPv6
- How to check if exists a ipv6 dns entry: `dig +short AAAA <hostname>.cern.ch`
- How to check if ipv6 is enabled (run in the machine): `ping6 google.com -c1`

## ssh tunneling via lxplus
To access a host from the outside, the answer is ssh tunneling via lxplus. 
More information in the [security recommendations](https://security.web.cern.ch/recommendations/en/ssh_tunneling.shtml)

## The afs token
To obtain tokens for authentication to AFS there is the command `aklog`.
The `aklog.service` runs on user login, and `aklog.timer` runs periodically.
