# AIMS2

AIMS2 is the Linux Automated Installations Management Systems service provided for CERN by Linux Support.

AIMS2 allows the user to perform remote PXE (Preboot eXecution Environment) installations, minimising human intervention. Multiple systems can be installed in parallel and the configuration can be easily tailored for specific settings. The system is based, and extends, the Kickstart software from the RedHat distribution.

## Documentation

### AIMS2 Resources

Check [our resources page](./resources.md) documentation to know what this service uses and how to retrieve access to them or contact whoever is responsible for them.

### AIMS2 Client

For a more comprehensive guide to the features available with aims2 please refer to the [aims2client](./aims2client.md) documentation.

### Installation servers

For more information on how an `aims2` server is configured and how the service works please refer to the [aims2server](./aims2server.md) documentation. This documentation is mainly intended for those within Linux Support.

### Ancient references

Although this is outdated, on Wenesday 1st October 2008, a presentation titled ["Introduction to AIMS2"](https://twiki.cern.ch/twiki/pub/LinuxSupport/Aims2/aims2.ppt) was presented.

If for whatever reason you still need more old references, refer to <https://twiki.cern.ch/twiki/bin/view/LinuxSupport/Aims2how2>. More than probably it is not relevant anymore and our current docs are more than enough to clarify how the service works.
