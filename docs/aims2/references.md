# Useful references

This is a compilation of all the interesting resources that can help you better understand of justify the way this service is built. They are separated by subject.

## Bootloader references

Maybe relevant for the bootloader mentions in <https://linuxops.web.cern.ch/aims2/aims2diagrams/>

* <http://www.dolda2000.com/~fredrik/doc/grub2>: GRUB 2 on a lower level, a nice explanation about how GRUB2 works
* <https://fecos.docs.cern.ch/fec/bootloader/#build-a-self-contained-grub-image>: A secondary reference on how to build the GRUB2 bootloader
* <http://blog.fpmurphy.com/2010/03/grub2-efi-support.html>: Why we patch GRUB2
* <https://www.mail-archive.com/grub-devel@gnu.org/msg20203.html>: Why GRUB2's sleep patch is not upstream
* <https://lists.gnu.org/archive/html/grub-devel/2020-04/msg00107.html>: Why it may not be easy to make it upstream as it conflicts with this one
* <https://www.centlinux.com/2018/10/automate-pxe-client-installations-with-kickstart.html>: Simplified similar version on how our setup works
* <https://www.gnu.org/software/grub/manual/grub/html_node/Installing-GRUB-using-grub_002dinstall.html>: Installing GRUB2. Useful for [koji-image-build's](https://gitlab.cern.ch/linuxsupport/koji-image-build/) Kickstart files peculiarities
* <https://netboot.xyz/>: External service we use for installing unsupported (by CERN) distributions through PXE. Currently suffering from [LOS-820](https://its.cern.ch/jira/browse/LOS-820)
    * Documented by Manuel on <https://linux-community.web.cern.ch/t/installing-other-linux-distributions-from-within-the-cern-network/95>
    * Watch out, back in the days security blocked all `*.xyz` domains making this unusable. We hope this does not happen again.
* <https://gist.github.com/vkanevska/fd624f708cde7d7c172a576b10bc6966>: Custom CentOS 7 bootable ISO / Support UEFI & Secure boot
* <https://superuser.com/questions/1165557/how-grub2-works-on-a-mbr-partitioned-disk-and-gpt-partitioned-disk>: How grub2 works on a MBR partitioned disk and GPT partitioned disk?

## DHCP / `dnsmasq` references

Useful for <https://linuxops.web.cern.ch/aims2/aims2workflows/> and  <https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/-/blob/qa/code/templates/dnsmasq_old_patched.conf.epp>

* <https://wiki.fogproject.org/wiki/index.php?title=ProxyDHCP_with_dnsmasq>: Explanation on how our `dnsmasq` component works and interacts with central DHCP server
* <https://serverfault.com/questions/829068/trouble-with-dnsmasq-dhcp-proxy-pxe-for-uefi-clients>: DNSMASQ specific arch numbers and configuration for UEFI to work
* <https://wiki.archlinux.org/title/dnsmasq>: `dnsmasq` matching rules docs
* <https://toreanderson.github.io/2015/11/16/ipv6-network-boot-with-uefi-and-ipxe.html>: references for the future if you need to investigate DHCP + IPv6 + PXE
* <https://www.mail-archive.com/dnsmasq-discuss@lists.thekelleys.org.uk/msg15651.html>: Juarez being ignored on the `dnsmasq` mailing list about DHCPv6 proxy features

## Useful references for AIMS Perl code

Relevant for <https://linuxops.web.cern.ch/aims2/aims2server/> and <https://gitlab.cern.ch/linuxsupport/rpms/aims2>

* <https://www.perl.com/pub/2001/01/soap.html/>: Perl SOAP examples that you can extrapolate for AIMS code

## iPXE references

Useful for <https://gitlab.cern.ch/linuxsupport/aims2-ipxe>

* <http://kimizhang.com/create-pxe-boot-image-for-openstack/>: iPXE image building reference
* <https://doc.rogerwhittaker.org.uk/ipxe-installation-and-EFI/>: iPXE reference. It may be useful if one day you decide iPXE is the way to go
* <https://gist.github.com/AdrianKoshka/5b6f8b6803092d8b108cda2f8034539a>: creating a bootable iPXE image

## Other references

* <https://vessokolev.blogspot.com/2015/03/how-to-install-centos-7-on-hp-proliant.html?m=1>: Driver Disk info. Relevant for <https://pagure.io/koji/pull-request/3217>
* <https://www.golinuxcloud.com/kickstart-clearpart-not-working-rhel-8/>: Solved: Kickstart clearpart not working in RHEL CentOS 7 8
* <https://forums.centos.org/viewtopic.php?f=13&t=69141&start=10>: ISO Missing e1000e Driver to Support Intel 8086:15bb, relevant for [Add LOS-659 image workaround for HP 800 G6 and G8 models commit](https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/-/commit/3457833712a95f76073bdceb92bcba7f7f438e39)
* <https://community.theforeman.org/t/update-driver-included-in-discovery-image/21330/7>: Update driver included in discovery image. Relevant for [LOS-659](https://its.cern.ch/jira/browse/LOS-659), when desktops do not work with our images due to missing driver compatibility.
* <https://blog.devopstom.com/hacking-initrdgz-ubuntu-netboot-installer/>: Also refs for altering image content to work un unsupported hardware. Same as previous links.
* <https://bugs.launchpad.net/ubuntu/+source/linux/+bug/1924889>: i915 graphics driver not loading for Rocket Lake i7-11700K [8086:4c8a]. Same as other links.
