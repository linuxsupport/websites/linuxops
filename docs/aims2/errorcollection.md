# GRUBsetta Stone

## AIMS2 error compilation

This section serves as a log of strange things we saw AIMS2 doing, for the record in case it even helps again.

We welcome any contribution to this page. Ironic and Procurement guys are also contributors to this list.

## UEFI world

<table>
<tbody>
<tr>
<td colspan="4"><b>(NO LONGER RELEVANT AFTER REDESIGN) DXE--CSM Initialization.. too little memory. Aborted. Press any key to exit.</b></td>
</tr>
<tr>
<td><i>Where it appears</i></td>
<td><i>Suspected reason</i></td>
<td><i>Reference</i></td>
<td><i>Resolution</i></td>
</tr>
<tr>
<td>Loading image with GRUB2</td>
<td>RPM update going wrong</td>
<td>-</td>
<td>Reinstall aims2-loaders and <code>service aims2sync restart</code></td>
</tr>
  
<tr>
<td colspan="4"><b>loading initrd ...error: you need to load the kernel first. Press any key to continue...alloc magic is broken at 0x4b9bd7e0: 0. Aborted. Press any key to exit.</b></td>
</tr>
<tr>
<td><i>Where it appears</i></td>
<td><i>Suspected reason</i></td>
<td><i>Reference</i></td>
<td><i>Resolution</i></td>
</tr>
<tr>
<td>Loading image with GRUB2</td>
<td>Image is not on disk</td>
<td>AIMS2 xinetd logs show error 404</td>
<td>This should not happen anymore after the redesign</td>
</tr>

<tr>
<td colspan="4"><b>loading initrd ...error: connection timeout.</b></td>
</tr>
<tr>
<td><i>Where it appears</i></td>
<td><i>Suspected reason</i></td>
<td><i>Reference</i></td>
<td><i>Resolution</i></td>
</tr>
<tr>
<td>Loading image with GRUB2</td>
<td>Slow download speeds</td>
<td>Check download speed before client error</td>
<td>Reduce the amount of clients contacting AIMS2 or check network</td>
</tr>
 
<tr>
<td colspan="4"><b>loading initrd ...error: out of memory</b></td>
</tr>
<tr>
<td><i>Where it appears</i></td>
<td><i>Suspected reason</i></td>
<td><i>Reference</i></td>
<td><i>Resolution</i></td>
</tr>
<tr>
<td>Loading image with GRUB2</td>
<td>Image too big. Hitting 4G memory limits</td>
<td>-</td>
<td>Reduce image size</td>
</tr>

<tr>
<td colspan="4"><b>loading initrd ...error: cannot allocate memory</b></td>
</tr>
<tr>
<td><i>Where it appears</i></td>
<td><i>Suspected reason</i></td>
<td><i>Reference</i></td>
<td><i>Resolution</i></td>
</tr>
<tr>
<td>Loading image with GRUB2</td>
<td>Broken BIOS</td>
<td>-</td>
<td>Reinstall BIOS firmware and reload the BIOS config</td>
</tr>

<tr>
<td colspan="4"><b>loading initrd ...error: couldn't find suitable memory target</b></td>
</tr>
<tr>
<td><i>Where it appears</i></td>
<td><i>Suspected reason</i></td>
<td><i>Reference</i></td>
<td><i>Resolution</i></td>
</tr>
<tr>
<td>Loading image with GRUB2</td>
<td>Unknown</td>
<td>-</td>
<td>Reduce image size</td>
</tr>

<tr>
<td colspan="4"><b>Reboot and Select proper Boot device. Please press the &lt;DEL&gt; key into BIOS setup menu...</b></td>
</tr>
<tr>
<td><i>Where it appears</i></td>
<td><i>Suspected reason</i></td>
<td><i>Reference</i></td>
<td><i>Resolution</i></td>
</tr>
<tr>
<td>-</td>
<td>Unknown</td>
<td>-</td>
<td>Unknown</td>
</tr>

<tr>
<td colspan="4"><b>DXE--CSM Initialization..error: no such partition. Entering rescue mode...</b></td>
</tr>
<tr>
<td><i>Where it appears</i></td>
<td><i>Suspected reason</i></td>
<td><i>Reference</i></td>
<td><i>Resolution</i></td>
</tr>
<tr>
<td>-</td>
<td>Unknown</td>
<td>-</td>
<td>Unknown</td>
</tr>

<tr>
<td colspan="4"><b>PXE-E21: Remote Boot cancelled</b></td>
</tr>
<tr>
<td><i>Where it appears</i></td>
<td><i>Suspected reason</i></td>
<td><i>Reference</i></td>
<td><i>Resolution</i></td>
</tr>
<tr>
<td>HNS2600KP models</td>
<td>Old PXE client</td>
<td>dnsmasq code: pxe_uefi_workaround @ rfc2131.c</td>
<td>Use BIOS mode</td>
</tr>

</tbody>
</table>
<p>&nbsp;</p>


## BIOS world

<table>
<tbody>

<tr>
<td colspan="4"><b>Loading kernel reboots the node</b></td>
</tr>
<tr>
<td><i>Where it appears</i></td>
<td><i>Confirmed reason</i></td>
<td><i>Reference</i></td>
<td><i>Resolution</i></td>
</tr>
<tr>
<td>HNS2600KP models</td>
<td>Advanced -> Power & Performance": Enable CPU HWPM=HWPM NATIVE MODE. This setting must be disabled -> `Enable CPU HWPM=Disabled`</td>
<td>Continuous restarts, no installation starts</td>
<td>Reset BIOS settings, then reinstall</td>
</tr>

</tbody>
</table>
<p>&nbsp;</p>
