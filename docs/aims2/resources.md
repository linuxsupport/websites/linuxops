# Related resources

## Useful Mattermost channel

Ask Arne Wiebalck to add you to <https://mattermost.web.cern.ch/it-dep/channels/ironic-internal>. This is the meeting point for Ironic/Procurement/AIMS2 conversations and helps speeding up certain tests or debugging.

## Databases

**Production**

* Databases: `dbod-aims.cern.ch:6601`
* User: `aims`
* Retrieve credentials with: `tbag show --hg "aims" aims_psqldb_pwd`
* Owner: `aims-admins`

**Test**

* Databases: `dbod-aimstest.cern.ch:6616`
* User: `aims`
* Retrieve credentials with: `tbag show --hg "aims" aims_psqldbtest_pwd`
* Owner: `aims-admins`

**Please be aware when connecting directly to the database. It is case sensitive and might have issues with passwords that contain `@` character.**

If you change the Database password, please do the following:

* Update the service account password and update the Teigi stored value: `tbag set --hg aims aims_psqldb_pwd --binary <new_password>` or `tbag set --hg aims aims_psqldbtest_pwd --binary <new_password>`
* Force a Puppet run on each of the AIMS hosts

Please check [the account management for Oracle accounts](https://cern.service-now.com/service-portal?id=kb_article&n=KB0000829) for further information.

## Storage

* CephFS shares:
  * `aims.cern.ch`
    * `AIMS Service` tenant
    * `aims_share` CephFS share
      * `aims_id` share-id
      * `flax`/`Meyrin CephFS` cluster
      * `remote_path`: `'/volumes/_nogroup/cebaadff-7d3d-4a1f-88d0-6eb773325901'`
      * `access_key`: `tbag show --hg aims flax.aims_id.secret`
      * Size 500GB
      * [Metrics](https://filer-carbon.cern.ch/grafana/d/000000111/cephfs-detail?from=now-90d&orgId=1&refresh=1m&to=now&viewPanel=15&var-cluster=flax&var-share=cebaadff-7d3d-4a1f-88d0-6eb773325901)
  * `aimstest.cern.ch`
    * `AIMS Service` tenant
    * `aims_share_test` CephFS share
      * `aims_id` share-id
      * `dwight`/`Geneva CephFS Testing` cluster
      * `remote_path`: `'/volumes/_nogroup/4272339e-0efb-4d0f-b17d-43d4d6ec600b'`
      * `access_key`: `tbag show --hg aims dwight.aims_id.secret`
      * Size 200GB
      * [Metrics](https://filer-carbon.cern.ch/grafana/d/000000111/cephfs-detail?from=now-90d&orgId=1&refresh=1m&to=now&viewPanel=15&var-cluster=dwight&var-share=4272339e-0efb-4d0f-b17d-43d4d6ec600b)

!!! note ""
    `aims.cern.ch`, i.e. production, has backups configured by our CephFS colleagues. See [RQF1823187](https://cern.service-now.com/service-portal?id=ticket&table=u_request_fulfillment&n=RQF1823187). If you need any assistance, contact them or open a Service Now ticket.


## Service accounts

* User: `aims`
* Retrieve credentials with: `tbag show --hg "aims" aims_password`
* Owner: Alex Iribarren
* Purpose: connecting to LANDB and LDAP

**Password changing procedure:**

* Announce with an OTG that the service will be degraded for 5-10min. No new issued installations will work.
* Update the service account password and update the Teigi stored value: `tbag set --hg aims aims_password --binary <new_password>`
* Force a Puppet run on each of the AIMS hosts

---

* User: `linux_private`
* Retrieve credentials with: `tbag show espassword --hg aims`
* Owner: Ulrich Schwickerath
* Purpose: Sending logs to our ES instance

## Egroups

* `aims2-upload`: Users with permissions to upload images to AIMS servers
* `aims-admins`: AIMS administrators
* `aims2-cc-admins`: Administrators for hosts on buildings 513, 613, 9994 (SafeHost), 9918 (Wigner), 773 (Network Hub) or 6045 (LHCb containers)

**Note these egroups can only be replaced for AIMS if updating `CONF` table on the database. Keys correspond to `EGROUP_UPLOAD`, `EGROUP_AIMSSUPPORT` and `EGROUP_SYSADMINS`.**

## LB Alias

* `aims.cern.ch`: used for prod instances.
* `aimstest.cern.ch`: used for test instances.
* `aimsdev.cern.ch`: used for dev instances and debugging. Might be empty at any given point. It helps isolating specific nodes.

## Related GitLab projects

* AIMS2 hostgroup: <https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims>
  * As of July 2021, these are the existing nodes for the hostgroup:
    * **prod**: `aims01`, `aims02`, `aims03`
    * **test**: `aimstest01`, `aimstest02`, `aimstest03`
* AIMS2 applications: <https://gitlab.cern.ch/linuxsupport/rpms/aims2>
* AIMS2 GRUB2: <https://gitlab.cern.ch/linuxsupport/aims/grub2>
* iPXE Openstack images to test PXE with VMs: <https://gitlab.cern.ch/linuxsupport/aims2-ipxe>
* Other AIMS2 related components: <https://gitlab.cern.ch/linuxsupport/aims>
