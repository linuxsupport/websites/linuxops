# Adding a new image to the base pxe boot menu

The example described below is adding a new Fedora image, as this may happen more frequently than adding a CentOS image - though the process will remain largely the same. As of today we do not do add more images than RHEL/CentOS, but this can help other use cases.

1. Ensure that the distribution (fedora for example) is mirrored. You may need to update `/mnt/data2/bin/new-lxsoft-sync-fedora` (please refer to [Architecture](../../distributions/architecture))
2. Ensure that the rsync process for your distribution is complete (check [http://linuxsoft.cern.ch/fedora/linux/releases/](http://linuxsoft.cern.ch/fedora/linux/releases/))
3. Download a copy of the vmlinuz and initrd for the new release ```wget http://linuxsoft.cern.ch/fedora/linux/releases/30/Server/x86_64/os/isolinux/vmlinuz && wget http://linuxsoft.cern.ch/fedora/linux/releases/30/Server/x86_64/os/isolinux/initrd.img```
4. Add a new image to AIMS2 ```aims2client addimg --name FEDORA30_X86_64 --arch x86_64 --vmlinuz vmlinuz --initrd initrd.img --description '[UNSUPPORTED] FEDORA 30 FOR X86_64 ARCH' --uefi```
5. Update the AIMS2 loaders and menu configurations in [AIMS hostgroup Hiera data](https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/-/blob/qa/data/hostgroup/aims.yaml).
6. Test `qa` branch, which should correspond to <http://aimstest.cern.ch>
7. Test your menu item by booting a VM which is configured to boot from the network. You can ensure that your VM boots from the network by setting the VM metadata 'CERN::cern-waitdns' to 'false' and 'LanDB::landb-os' to 'PXELINTEST'.
- Note: As of August 2019, OpenStack projects that are configured with neutron do not support the passing of next-server and filename DHCP options. (please refer to [iPXE cheatsheet](../../cheatsheets/ipxe) for more debugging info)
8. Providing everything is okay, promote `qa` branch to `master` branch.

# Testing a new image

If you want to test a new image you can follow this example (adapt when needed):

* `aims2client addimg --name C82_X86_64_TEST --arch x86_64 --vmlinuz vmlinuz --initrd initrd.img --description "CENTOS 8.2 X86_64 TEST" --uefi`
* Using [ipxe](/cheatsheets/ipxe), create a new VM with `LanDB::Os='LINUX'` (aims) or `LanDB::Os='PXELINTEST'` (aimstest)
* Wait for it to appear on LANDB
* Download or write a sample Kickstart file such as <http://linux.web.cern.ch/linux/centos8/default.ks>
* Add the host to AIMS (You might need to adapt your options, take [AIMS hostgroup Hiera data](https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/-/blob/qa/data/hostgroup/aims.yaml) as an example for kernel options:
```
aims2client addhost djuarezg-c82-m  --kickstart default.ks --kopts "ip=dhcp inst.repo=http://linuxsoft.cern.ch/cern/centos/8.2-testing/BaseOS/x86_64/os inst.addrepo=CERN,http://linuxsoft.cern.ch/cern/centos/8.2-testing/CERN/x86_64/ inst.addrepo=locmap,http://linuxsoft.cern.ch/internal/repos/potd8-stable/x86_64/os/ ks=http://linux.web.cern.ch/linux/centos8/default.ks" --pxe --name C82_X86_64_TEST
```
* Wait for the host config to sync, eventually your VM will PXE boot using your KS file and options
