# Sync AIMS and AIMSTEST

## Using DBeaver and `rsync`

In this section we are assuming you have an already configured DBeaver instance. You can have this locally or use [CERN Terminal Servers](https://remotedesktop.web.cern.ch/remotedesktop/).

For AIMS and AIMSTEST credentials check `tbag showkeys --hg aims` or `cat /etc/aims2.conf` from within an AIMS prod or test node accordingly:

```
# psql admin test credentials
tbag show aims_psqldbtest_pwd_admin --hg aims
# psql admin prod credentials
tbag show aims_psqldb_pwd_admin --hg aims
```


1. As per <https://dbeaver.com/docs/wiki/Data-transfer/> select all the tables in the prod schema and export them.
    * Basically, select all prod tables, right-click, export data, Database type of export, next, next, next, start
        * There may be some conflicts from key collisions. Delete them by hand if you want, or directly empty table data from test instance prior to migration
    * You now directly use the Database export and make it point to the test instance. Check the "ON CONFLICT DO UPDATE SET" setting.
1. Rsync the boot directories to sync all the PXE images: `[root@aims01 ~]# rsync -arv /aims_share/tftpboot/aims/boot/ root@aimstest01:/aims_share/tftpboot/aims/boot`

## Other methods

Feel free to add your own way to do so, maybe you have a simpler way using just commands.
