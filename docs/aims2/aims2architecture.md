# AIMS2 architecture

All AIMS instances are under the control of the [corresponding hostgroup](https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims) which also takes care of putting the corresponding Teigi secrets in place.

Uploaded images are stored on a CephFS share to be shared across all nodes, backed up by our Storage colleagues. Ref. <https://linuxops.web.cern.ch/aims2/aims2/#storage>

AIMS2 servers have the following main components:

* An Apache server, which has a module corresponding to the AIMS2 server, therefore you can find related logs under `/etc/httpd/logs/`
* AIMS2 server, which logs in `/var/log/messages`. This is the main component of the whole system
  * It deals with all requests from the clients
* `dnsmasq` component which serves as differentiation for received requests from the client machines. Depending on its architecture, the server provides a different bootloader.
  * For unregistered machines, the differentiation is done on [the DHCP configuration](https://gitlab.cern.ch/network/cfmgr/blob/master/cfmgr_modules/hcp/shcp.pm) due to the clients not sending any DHCP option to AIMS servers.
* `httpd` keeps coherence between the database data and the local file system. It acts upon request.
  * It stores image blobs in the mounted CephFS share `/aims_share/tftpboot/aims/boot/`
  * It stores the specific `pxelinux` or `grub` configurations for clients under `/aims_share/tftpboot/aims/config/`
* `aims2client` is the tool used to interact with the system, for new hosts, images or configurations
* `aims2config` is the tool used to interact with the Oracle DB configuration, such as the Authorization Egroups used by the server to validate certain operations.

All nodes within a hostgroup/LB alias are equally balanced to spread the load. The CephFS share helps keeping all content in sync across all nodes and make it scalable.
