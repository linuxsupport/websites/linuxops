# AIMS2 server

AIMS2 servers have two main roles. The first is to service client requests, handling new registrations, permissions checks and interaction with the database. The second role is to service TFTP requests. The following sections should outline how an AIMS2 server is configured and how (should you need to) deployed.

## Server logic

When the user adding a new image is authorised, the image blob is stored on a CephFS share. `httpd` takes care storing it on disk under its corresponding directory according to the server TFTP structure; it also builds the corresponding configuration files for the registered interfaces. All boot images, bootloaders and PXE configurations are shared between all hostgroup nodes thanks to CephFS shares.

## TFTP Structure

The following structure shows how an AIMS2 server is configured (Root path `/aims_share/tftboot`).

```bash
[root@aims01 tftpboot]# ls -R | grep ":$" | sed -e 's/:$//' -e 's/[^-][^\/]*\//--/g' -e 's/^/   /' -e 's/-/|/'
   .
   |-dnsmasq
   |-aims
   |---boot
   |-------CS8_X86_64
   |-------initrd
   |-------vmlinuz
   |-----CC7_X86_64
   |-------initrd
   |-------vmlinuz
   |-----HWREG_AUTOINSTALL
   (...)
   |---config
   |-----arm64
   |-------arm64-efi
   |-----bios
   |-----uefi
   |-------themes
   |---------not
   |-------x86_64-efi
   |---loader
   |-----arm64
   |-----bios
   |-------aims
   |-----netbootxyz
   |-----uefi
   |-aimshttpupload
   |-hwreg
   |---loader
   |-----bios
   |-----uefi
   |-------x86_64-efi
   |---pxelinux.cfg
```

Please note that `aims/config/<TYPE>` and `aims/boot/` are dynamic directories, that is, their contents is maintained by `aims2server`.

## SOAP Interface

To connect to the SOAP interface, you need to use the following parameters in your connection method.

URI: `urn:/aims2server`

PROXY: `http://SERVERNAME/aims2server/aims2server.cgi`

SERVERNAME must match that of the defined Kerberos principals.

## Modules

`aims2server` modules are installed in `/usr/share/perl5/aims2server/`

For example,

```perl
use aims2server::ldap;
use aims2server::db;
```

## Methods

 Methods that the client are allowed to execute are documented in soap.pm and server.pm

```perl
   # These are our callable methods. Anything else will croak below.
      for my $Method qw(AddHost RemoveHost GetHostByName AddImage RemoveImage GetImageByName ListAllImages EnablePXE DisablePXE HostHistory GetKickstartFile UpdateKickstartFile Permission)

use subs qw(new SetUser AddImage RemoveImage GetImageByName AddHost RemoveHost GetHostByName EnablePXE DisablePXE GetKickstartFile UpdateKickstartFile RemoveKickstartFile Permission);
```

## Authentication

Kerberos authentication setup is done through the hostgroup.

## Database

`aims2server` database supported is provided by DBoD.

The database configuration is set at `/etc/aims2.conf`, through the [AIMS2 hostgroup](https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims).

Check [our resources page](./resources.md) to get the access details and credentials.

## Server-side configuration

Most AIMS2 server configuration comes from the `CONF` table in the database (which has a simple schema of KEY/VALUE/DESCRIPTION), this includes the various credentials used for accessing other databases (such as CDB or LDAP or LANDB). Use interactive access, look for and/or update CDB_CONN/CDB_USER/CDB_PASS etc.

You can use the `aims2config` tool from within an AIMS host to see, update or create new entries into the table.

## Cleanup Daemon

To maintain the server clean of inconsistent PXE configurations that do not match the bootmode a node has in the database each server has a daemon running. This daemon maintains a persistent connection to the database.

* Starting the daemon: `service aims2cleanup start`

* Stopping the daemon: `service aims2cleanup stop`

During normal operation the daemon will maintain a connection to the database. If the connection fails for any reason, the daemon will disconnect from the database (if not already forced) and go to sleep for a period of `$DELAY` seconds. As this happens it will also send out a message to administrators that there is a problem. After `$DELAY`, the daemon will try to re-connect. The daemon will retry this until it can re-connect, each time sleeping for `$DELAY+RAND`. On re-connection, the daemon notifies server administrators that things are again OK.

### Daemon Logging

The daemon will log output to `/var/log/aims/aims2cleanup.log`

### Old PXE configurations cleanup

Since we do not want to keep the PXE configurations files of a node forever (as this means it will always try to install whatever the configuration file says), we have a cleanup mechanism [here](https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/-/blob/af8603c4275cc8288b76c2377a568013cf42a145/code/manifests/aims.pp#L324-340). This runs a daily cleanup using [aims2oldhostsremoval](https://gitlab.cern.ch/linuxsupport/rpms/aims2/-/blob/master/src/aims2oldhostsremoval) script to remove no longer relevant content.

The reason why we run the script from a cron on the Puppet configuration is to avoid it running on all nodes at the same time. Only one node running is required as all of them share the filesystem.

