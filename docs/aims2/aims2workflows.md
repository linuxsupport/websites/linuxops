# AIMS2 PXE-DHCP workflows

As of today (April 2022) we use two specific workflows depending on whether we are dealing with a registered machine (present on LANDB) or an unregistered machine (no LANDB registry).

## Use case for unregistered machines

For machines not yet present on LANDB, it is the DHCP server who decides which bootfile/bootloader to use.

Take this ISC DHCP configuration extract as an example:

```
# Installation services require a divided pool for unknown devices. Only secondaries are allowed
(...)
if substring (option vendor-class-identifier, 0, 9) = "PXEClient" {
    option vendor-class-identifier "PXEClient";
    vendor-option-space LINUX;
    option LINUX.discovery-control 11;
    next-server aims.cern.ch;

    if option client-architecture = 00:00 { # Intel x86PC - in use now.
        option LINUX.pxelinux-magic F1:00:74:7E;
        option LINUX.pxelinux-pathprefix "hwreg/loader/bios/";
        option LINUX.pxelinux-reboottime 50;
        filename "hwreg/loader/bios/lpxelinux.0";
    }
    if substring (option vendor-class-identifier, 0, 20) = "PXEClient:Arch:00007" { # UEFI-64-1
        option LINUX.pxelinux-magic F1:00:74:7E;
        option LINUX.pxelinux-pathprefix "hwreg/loader/x86_64-efi/";
        option LINUX.pxelinux-reboottime 50;
        filename "hwreg/loader/x86_64-efi/bootx64.efi.0";
    }
    if substring (option vendor-class-identifier, 0, 20) = "PXEClient:Arch:00011" { # AARCH64_EFI
        option LINUX.pxelinux-magic F1:00:74:7E;
        option LINUX.pxelinux-pathprefix "hwreg/loader/arm64-efi/";
        option LINUX.pxelinux-reboottime 50;
        filename "hwreg/loader/arm64-efi/bootarm64.efi.0";
    }
}
```

As the configuration shows, DHCP will decide which file to use depending on the vendor class, i.e. which architecture/bootmode the client is using.

### Workflow

The workflow implies the following:

* Client contacts DHCP server
* DHCP tells the client which filename to download from AIMS server
* Client downloads the bootloader
* Bootloader uses a default configuration file to [boot an auto-registration image](https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/-/blob/8604dd20edf50d6a80833a340c8009c6a00a59d6/code/templates/aimsloaders/hwreg/x86_64-efi/grub.cfg.erb#L47). In practice, this is [`OPENSTACK-IRONIC-IPA-X86_64`](https://gitlab.cern.ch/cloud-infrastructure/ironic-python-agent-builder) booted with parameters pointing to the Ironic server to contact for registration.
* The Ironic image takes over from this point.

The future KEA DHCP replacement also allows to do the same decision making.

This basically covers points (1) and (2) in the following overview diagram:

![Autoregistration](../assets/images/autoregistration.png)

## Use case for registered machines

Machines already present on LANDB have multiple properties. Among these, the Operative System, which will help deciding where to go to continue with the booting process.

Take these extracts from ISC DHCP configuration as an example:

```
# LINUX
if substring (option vendor-class-identifier, 0, 9) = "PXEClient" {
    option vendor-class-identifier "PXEClient";
    # This seems to be mandatory when using dnsmasq DHCP-proxy mode
    option dhcp-server-identifier aims.cern.ch;
    next-server aims.cern.ch;
}
(...)
# PXELINTEST
if substring (option vendor-class-identifier, 0, 9) = "PXEClient" {
    option vendor-class-identifier "PXEClient";
    # This seems to be mandatory when using dnsmasq DHCP-proxy mode
    option dhcp-server-identifier aimstest.cern.ch;
    next-server aimstest.cern.ch;
}
```

Depending on the OS the machine has on LANDB, it will go through one group or the other, pointing to the next server required for the PXE process, i.e. `aims.cern.ch` for `LINUX` or `aimstest.cern.ch` for `PXELINTEST`.

To ease debugging on our side and agility in changes, the decision making for these cases is in AIMS side. For this matter, we use `dnsmasq` in proxy-DHCP mode.

`dnsmasq` will just take these vendor class options and make clients use one filename/bootloader or other, depending on them. Just as the DHCP does.

On <https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/-/blob/qa/code/templates/dnsmasq.conf.epp> you will find the matching we are currently using.


### Workflow

The workflow implies the following:

* Client contacts DHCP server
* DHCP tells the client which `next-server` to use to continue the PXE boot process
* AIMS server uses `dnsmasq` to serve the client the bootloader it requires according to `vendor-class`
* Bootloader uses a default configuration file or a customised one for the client and allow booting the desired image

## Considerations

* Any unregistered node will always go through the first use case
  * For this case, we have to agree with DHCP service managers on which configuration, filenames and paths to use.
* We could standardise everything at some point if we agree on a way to debug PXE calls

## Extra references

* [Nice explanation on how the dnsmasq Proxy-DHCP mode works](https://wiki.fogproject.org/wiki/index.php?title=ProxyDHCP_with_dnsmasq)

# Future improvements

* [LOS-900: Having a test environment for DHCP](https://its.cern.ch/jira/browse/LOS-900)
    * This would be great as it would avoid depending on anyone to test change by change and agreeing on dates and times to do so.
    * Otherwise sorry for the next souls that need to test changes on these workflows.
