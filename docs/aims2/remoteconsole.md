# Remote console access tips and tricks

This section servers as a recommendation of practices that we know do the job of accessing remote consoles for debugging, testing and all. If you find yourself a better way of doing so, please put them in here for future reference.

## Retrieving console credentials for machines

Before getting access to a console you first need the BMC credentials to access the machine. These can come from two different places.

Of course these details can be extracted through the corresponding APIs.

### Judy credentials

![Judy BMC credentials](../assets/images/judy_credentials.png)

### Openstack credentials

![Openstack BMC credentials](../assets/images/openstack_credentials.png)

## Serial Over LAN

According to attempts on December 2022, this is the only way that works.

After getting the credentials for the machine of interest from Judy (Console Username and console Password), you can run the following command from aiadm:

```bash
[djuarezg@aiadm10 ~]$ ipmitool -I lanplus -H techlab-arm64-thunderx-01-ipmi -U <console_username> -P <console_password> sol activate
```

Then you are prompted to login. Type "root".

The password for this login is defined by the "tellme" number. According to [Root passwords](https://configdocs.web.cern.ch/tellme/index.html) documented by Configuration Management you can get the password by running this command from aiadm:
```bash
$ tellme show root --host techlab-arm64-thunderx-01
```
Use the response/password and you will be connected to the node (in this case: techlab-arm64-thunderx-01).

If you need to reboot the machine from remote console, you go to: Judy -> Properties [Remote console : Console] -> Power -> Power Cycle System -> Apply Changes

While you are connected through SOL you can see the machine rebooting and you will be able to intervene on the boot process (e.g. go to Rescue Boot) if needed.

The server configuration URL will show you different tabs and configuration options. It is impossible to tell you where the needed option will be as each vendor has its own WebUI.

If you encounter any IPMI misbehaviour use [`ipmiproxy`](https://gitlab.cern.ch/hw/ipmiproxy) from `aiadm` instead, a tool maintained by our procurement colleagues.

Of course when using SOL you will not be able to see any graphical interface, like AIMS menus, graphical OS installer etc. SOL can help when any other visual console methods fail.

## vKVM or any similar Java Web based applet

If you are not on the CERN network, the IPMI URL will not be accessible. If you are using an SSH proxy, make sure also the console traffic is proxied.

For this matter, the easier way is to use [sshuttle](https://codimd.web.cern.ch/vjC8BHbTS7etHwJve-K2Uw) to redirect all traffic.

You will usually be faced with security warnings due to certificates and insecure applets. If acknowledging all risks and ignoring the security warnings is still not enough, try configuring your local Java policies accordingly.

### CERN Terminal servers

As a backup plan, use [CERN Terminal Servers](https://espace.cern.ch/winservices-help/Terminal%20Services/Introduction/Pages/Login%20into%20Terminal%20Services.aspx) which will provide you with a Windows server for you to use and configure. In here, you can add security exceptions and configure Java Security by searching for `Configure Java` on the Windows explorer. <!-- codespell:ignore espace -->

Once in, using your remote desktop application of preference (Remote Desktop Viewer on CentOS 7 by default), connecting to <cernts.cern.ch> using RDP protocol, you can repeat the process and get the console URL from either Judy/Foreman or Openstack.


## HTML5 console

If you are lucky enough, your server will have a console over HTML5, which so far we have found to always work.

**Go for this one if you can.**

## Other tips and tricks

* Some consoles allow you to run key macros (for example Alt+Tab) through the upper menus. You may need it to switch between the different logs while Anaconda runs or fails.
* On CERN TS you can install your own applications, so choose your preferred web browser instead of using IE/Edge/Whatever MicroSoft puts in by default.
* While on site you will be on CERN network so SSH proxies or `sshuttle` will not be needed.
* The provided BMC credentials allow you to operate the machine with IPMI, such as powering on, off, power cycle, hard reset, **and even altering the boot order**

