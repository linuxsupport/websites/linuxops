# Legacy workaround

After [OTG0069753](https://cern.service-now.com/service-portal?id=outage&n=OTG0069753), legacy workaround does no longer exist. Nonetheless if you still need to save some old out of warranty nodes and be able to make them PXE boot here is what you need to do. **We hope you never have to do this**:

* Mark each node for this temporary intervention: `sudo roger update --appstate intervention --message "Temporary intervention RQFXXXXX" --all_alarms false`
* Stop Puppet temporarily to avoid your changes to be overridden: `puppet agent --disable`
* Identify the node's MAC addresses. If you know which one it is going to use for PXE booting, use that one only, if not, **do this for each of them**.
    * You can get the MAC addresses by checking on LANDB or by checking them if it is registered on AIMS with `aims2client showhost TESTNODE --full`
* Create a `dnsmasq` conf file that matches this MAC address
    * For a MAC address `FA-16-3E-C2-6A-EC` you will create `/aims_share/dnsmasq/01-fa-16-3e-c2-6a-ec.lgcy`. Note it always starts with `01-`.
    * Corresponding file content will be `dhcp-mac=x86PC,fa:16:3e:c2:6a:ec`
* **Restart** `dnsmasq` service on all the AIMS hosts otherwise it will not take this configuration into account.
* Create the required PXE conf file for SYSLINUX manually. For the previous example:

```
$ cat /aims_share/tftpboot/aims/config/bios/01-fa-16-3e-c2-6a-ec
default linux
 label linux
 kernel http://aims.cern.ch/aims/boot/CC79_X86_64/vmlinuz
 append initrd=http://aims.cern.ch/aims/boot/CC79_X86_64/initrd ip=dhcp repo=http://linuxsoft.cern.ch/cern/centos/7.9/os/x86_64/  inst.ks=http://aims.cern.ch/aims/ks
 ipappend 2
```

* Confirm with the user once it has been installed then remove the configuration files you added and restart the `dnsmasq` service once again.
* Remember to run Puppet again: `puppet agent --enable`
* Run the alarming again: `sudo roger update --appstate production --all_alarms true`

!!! warning
    If the machine is still unable to PXE boot as it should, then it may be because it is that old that it cannot even use HTTP to download the required files. In that case you will have to do the following adaptations to the previous instructions

* Mark each node for this temporary intervention: `sudo roger update --appstate intervention --message "Temporary intervention RQFXXXXX" --all_alarms false`
* Stop Puppet temporarily to avoid your changes to be overridden: `puppet agent --disable`
* The `dnsmasq` conf file (`/aims_share/dnsmasq/01-fa-16-3e-c2-6a-ec.lgcy`) that matches the MAC address will have to have this content instead:
    * `dhcp-mac=x86PClgcy,fa:16:3e:c2:6a:ec`
    * This will make it use an older SYSLINUX version
* Add this line to each and every of AIMS nodes `/etc/dnsmasq.conf`:
    * `pxe-service=tag:x86PClgcy,x86PC, "netboot x86PClgcy", /aims/loader/bios/pxelinux`. Note the bootloader file is different
* **Restart** `dnsmasq` service on all the AIMS hosts otherwise it will not take this configuration into account.
* Create the required PXE conf file for SYSLINUX manually. Note the different protocol, no HTTP, it will use TFTP. For the previous example:

```
$ cat /aims_share/tftpboot/aims/config/bios/01-fa-16-3e-c2-6a-ec
default linux
 label linux
 kernel /aims/boot/CC79_X86_64/vmlinuz
 append initrd=/aims/boot/CC79_X86_64/initrd ip=dhcp repo=http://linuxsoft.cern.ch/cern/centos/7.9/os/x86_64/  inst.ks=http://aims.cern.ch/aims/ks
 ipappend 2
```

* Confirm with the user once it has been installed then remove the configuration files you added and restart the `dnsmasq` service once again.
* Remember to run Puppet again: `puppet agent --enable`
* Run the alarming again: `sudo roger update --appstate production --all_alarms true`
