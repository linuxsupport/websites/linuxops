# Replace AIMS2 servers

The setup is self-contained under the [aims hostgroup](https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims) the replacement should be straight-forward.

In case you need to replace any of the servers, the procedure is as simple as it follows:

* Check which instance is active at this moment. If the one you are going to replace it the master, change the status on Roger so the slaves take its place.

```bash
sudo roger update --appstate intervention --message "Replacing node" --all_alarms false
```

* Wait for the Load Balanced alias to remove the desired node

* Delete the previous instance
  * Make sure you do not have anything worth keeping from these instances such as any test or temporary file

```bash
# From aiadm
eval $(ai-rc --same-project-as aims_instance)
ai-kill aims_instance
# Wait for the DNS to refresh before recreating the new instance

# For test instances we don't need to specify the availability zone
ai-bs --landb-mainuser aims-admins --landb-responsible aims-admins   --nova-flavor m2.2xlarge --cs8 -g aims --foreman-environment your_environment   aims_instance.cern.ch

# For production instances we specify availability zones to split nodes. One node at least must be in 'AIMS service critical' tenant, gva-critical AVZ.
ai-bs --landb-mainuser aims-admins --landb-responsible aims-admins   --nova-flavor m2.2xlarge --cs8 -g aims --foreman-environment production   aims_instance.cern.ch --nova-availabilityzone cern-geneva-b
```

* Enable Roger state once again, **otherwise the node will not appear in the alias!!!**

```bash
sudo roger update --appstate production --all_alarms true
```

* If needed for testing PXE with VMs, you may need to update AIMS iPXE image on <https://gitlab.cern.ch/linuxsupport/aims2-ipxe> to point to new nodes if you changed them (and therefore their IPs changed) and upload this new image to Openstack.
