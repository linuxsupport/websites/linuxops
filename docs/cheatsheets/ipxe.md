# iPXE

## Introduction

At CERN, the DHCP next-server and filename options presented to a client are controlled via the labdb-os field.

- The field 'LINUX' will present to the client the AIMS2 production next-server address
- The field 'PXELINTEST' will present to the client the AIMS2 test next-server address

## Overriding DHCP options

With iPXE it's possible to override (or set if missing) the next-server and filename DHCP options. This could be useful if these options are not being set when they should be. As of August 2019, CERN OpenStack neutron cells are not setting these options correctly. As a workaround one can:

1. Enter the iPXE boot menu when prompted with Ctl+B
2. run the interactive configuration tool `config net0`
3. set next-server to the IP address of aimstest01
4. set filename to be '/aims/loader/bios/lpxelinux.0'
5. Save the config with 'Ctl+x'
6. autoboot

## Creating an iPXE glance image (automatically)

Please refer to [https://gitlab.cern.ch/linuxsupport/aims2-ipxe](https://gitlab.cern.ch/linuxsupport/aims2-ipxe)
The script in that location will create an iPXE image with hardcoded dhcp options 'next-server' and 'filename', thus ensuring that it will boot regardless of the underlying OpenStack networking technology used and negating the requirement to set the LanDB:os metadata for the instance.

## Creating an iPXE glance image (manually)

The following steps can be taken to create a new bootable iPXE glance image

1. `dd if=/dev/zero of=pxeboot.img bs=1M count=4`
2. `mkdir /mnt/image`
3. `mkdosfs pxeboot.img`
4. `losetup /dev/loop0 pxeboot.img`
5. `mount /dev/loop0 /mnt/image`
6. `setenforce 0` (if it was enforcing)
7. `syslinux --install /dev/loop0`
8. `setenforce 1` (if it was changed in step 6)
9. `wget http://boot.ipxe.org/ipxe.iso`
10. `mount -o loop ipxe.iso /media`
11. `cp /media/ipxe.krn /mnt/image/`
12. populate the syslinux.cfg as per
```
cat > /mnt/image/syslinux.cfg <<EOF
DEFAULT ipxe
LABEL ipxe
KERNEL ipxe.krn
EOF
```
Finally, clean up `umount /media/ && umount /mnt/image`
