# Kerberos

This page details basic checks that should be performed when debugging a 'Kerberos issue'.

## DNS resolution

Kerberos is extremely sensitive to sane hostname resolution on both the Active Directory / FreeIPA (Server) side as well as the client.
Server side resolution should not be a problem at CERN as this is managed via landb, however one should check that the host correctly responds on both forward and reverse lookups.

For example, let's say we have a host 'kerberosclient.cern.ch' that has a 'Kerberos issue' - where do we start by checking what is going on?

1. Can I resolve kerberosclient.cern.ch from another machine on the cern network? ```dig +short kerberosclient.cern.ch```
2. Is the reverse DNS entry for kerberosclient.cern.ch correct when running on another machine on the cern network? ```dig +short -x $IPADDRESS-from-above-command```
3. Is the hostname set correctly on the client machine? ```hostname``` or ```hostnamectl``` should return the fully qualified domain name of the client (kerberosclient.cern.ch in this example)
4. Is DNS forward resolution working correctly on the client machine? ```[client ~]$ dig +short kerberosclient.cern.ch```
5. Is DNS reverse resolution working correctly on the client machine? ```[client ~]$ dig +short -x $IPADDRESS-from-above-command```

This is overly basic, but it cannot be stressed enough that correct DNS resolution is critical to the functionality of Kerberos.

## Client-side debugging

The `KRB5_TRACE` environment variable can be set to make `kinit` write debug outputs to a file. It may ever be set to `/dev/stderr` to write to the standard error stream. These outputs are featuring realm and KDC deduction process, credential selection, choice of encryption algorithm and authentication request results.

## Time configuration

Kerberos also requires that the client has a time set close to the time configured on the active directory servers. The default clock skew threshold is 5 minutes. Ensure that the client machine has a up to date time set.

## Host keytab generation at cern

Host keytabs are generated at CERN via the helper utility /usr/sbin/cern-get-keytab. This is a perl script that when called via root, it requests via windows service a keytab for the host (providing reverse DNS is configured correctly).

## krb5.conf

When debugging a kerberos issue, make sure that the local client configuration is sound. For a puppet/locmap managed machine, this is taken care of of. If not, make sure that that the machine doesn't have anything weird /etc/krb5.conf (for example, setting the default rdns to False)
For non puppet/locmap machines, a correct krb5.conf can always be found here [http://linux.web.cern.ch/linux/docs/krb5.conf](http://linux.web.cern.ch/linux/docs/krb5.conf)

## Checking host principles

From a host that has a working Kerberos installation, you can run ```kvno $SHORTNAME```. If the host exists, you will be presented with the 'key version number' of the record from the directory side. If you are working on a host that has issues, you may want to confirm that the kvno is the same between the version the directory knows about versus what is present on the host. The following command can be used (as root) to query the system keytab to see the current kvno as well as last update time: ```klist -kt /etc/krb5.keytab```

## cern-get-keytab

If you get the error message ```Error checking computer in AD, object not found, not in authorized OU or invalid user trying to join domain```, ensure that the LanDB-OS is set to ```Linux```

This shouldn't be a problem for most cases as VMs with CC7 set this via the glance image metadata, but should be something checked.

It is also worth mentioning that the lxkerbwin service seems to cache the response from LanDB, thus an update to LanDB will take several minutes to see different behaviour via the client (cern-get-keytab)
